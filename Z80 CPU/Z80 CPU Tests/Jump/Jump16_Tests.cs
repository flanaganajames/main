﻿using BitClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;

namespace Z80_CPU_Tests.Tests.SixteenBit.Jump
{
    [TestClass]
    public class Jump16_Tests
    {
        private TrueMemorySimulator memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new TrueMemorySimulator(1500);
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void Given_PCIs0h_When_JP0100h_Then_PCIs0100h()
        {
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            memory[0] = JumpCommand.JP_nn;
            memory[1] = hex_01; 
            memory[2] = ZByte.Empty;
            memory[3] = LoadCommand.LD_aHL_n;
            memory[4] = hex_01;
            
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(JumpCommand.JP_nn, memory._internal[0]);
        }

        [TestMethod]
        public void Given_PCIs0480h_When_JR_05h_Then_PCIs0480h()
        {
            var hex_04 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);
            var hex_85 = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            var hex_05 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            var hex_80 = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);

            memory[0] = JumpCommand.JP_nn;
            memory[1] = hex_80;
            memory[2] = hex_04;

            memory[1152] = JumpCommand.JR_e;
            memory[1153] = hex_05;

            memory[1157] = LoadCommand.LD_aHL_n;
            memory[1158] = hex_05;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(hex_05, memory._internal[0]);
        }

        [TestMethod]
        public void Given_CarryFlagClear_When_JR_C_05h_Then_PCIs02h()
        {            
            var hex_05 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            
            memory[0] = JumpCommand.JR_C_e;
            memory[1] = hex_05;
            memory[2] = LoadCommand.LD_aHL_n;
            memory[3] = hex_05;

            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(hex_05, memory._internal[0]);
        }

        [TestMethod]
        public void Given_CarryFlagSetAndPC04h_When_JR_C_05h_Then_PCIs09h()
        {
            var hex_05 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            var hex_FF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            memory[0] = LoadCommand.LD_A_n;
            memory[1] = hex_FF;
            memory[2] = EightBitArithmeticCommand.ADD_A_N;
            memory[3] = hex_FF;        

            memory[4] = JumpCommand.JR_C_e;
            memory[5] = hex_05;
            memory[6] = LoadCommand.LD_aHL_n;
            memory[7] = hex_05;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(LoadCommand.LD_A_n, memory._internal[0]);
        }

        [TestMethod]
        public void Given_CarryFlagClear_When_JR_NC_05h_Then_PCIs07h()
        {
            var hex_05 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            memory[0] = JumpCommand.JR_NC_e;
            memory[1] = hex_05;
            memory[2] = LoadCommand.LD_aHL_n;
            memory[3] = hex_05;

            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(JumpCommand.JR_NC_e, memory._internal[0]);
        }

        [TestMethod]
        public void Given_CarryFlagSetAndPC04h_When_JR_NC_05h_Then_PCIs06h()
        {
            var hex_05 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            var hex_FF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            memory[0] = LoadCommand.LD_A_n;
            memory[1] = hex_FF;
            memory[2] = EightBitArithmeticCommand.ADD_A_N;
            memory[3] = hex_FF;

            memory[4] = JumpCommand.JR_NC_e;
            memory[5] = hex_05;
            memory[6] = LoadCommand.LD_aHL_n;
            memory[7] = hex_05;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(hex_05, memory._internal[0]);
        }

        [TestMethod]
        public void Given_ZeroFlagClear_When_JR_Z_05h_Then_PCIs02h()
        {
            var hex_05 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            memory[0] = JumpCommand.JR_Z_e;
            memory[1] = hex_05;
            memory[2] = LoadCommand.LD_aHL_n;
            memory[3] = hex_05;

            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(hex_05, memory._internal[0]);
        }

        [TestMethod]
        public void Given_ZeroFlagSetAndPC03h_When_JR_Z_05h_Then_PCIs08h()
        {
            var hex_05 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            var hex_FF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            memory[0] = LoadCommand.ConstantIntoB;
            memory[1] = hex_FF;
            memory[2] = EightBitArithmeticCommand.INC_B;            

            memory[3] = JumpCommand.JR_Z_e;
            memory[4] = hex_05;
            memory[5] = LoadCommand.LD_aHL_n;
            memory[6] = hex_05;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(LoadCommand.ConstantIntoB, memory._internal[0]);
        }

        [TestMethod]
        public void Given_ZeroFlagClear_When_JR_NZ_05h_Then_PCIs05h()
        {
            var hex_05 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            memory[0] = JumpCommand.JR_NZ_e;
            memory[1] = hex_05;
            memory[2] = LoadCommand.LD_aHL_n;
            memory[3] = hex_05;

            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(JumpCommand.JR_NZ_e, memory._internal[0]);
        }

        [TestMethod]
        public void Given_ZeroFlagSetAndPC03h_When_JR_Z_05h_Then_PCIs05h()
        {
            var hex_05 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            var hex_FF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            memory[0] = LoadCommand.ConstantIntoB;
            memory[1] = hex_FF;
            memory[2] = EightBitArithmeticCommand.INC_B;

            memory[3] = JumpCommand.JR_NZ_e;
            memory[4] = hex_05;
            memory[5] = LoadCommand.LD_aHL_n;
            memory[6] = hex_05;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(hex_05, memory._internal[0]);
        }

        [TestMethod]
        public void Given_PCIs0hAndHLis0100h_When_JP_aHL_Then_PCIs0100h()
        {
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            memory[0] = LoadCommand.ConstantIntoH;
            memory[1] = hex_01;
            memory[2] = JumpCommand.JP_aHL;         
            memory[3] = LoadCommand.LD_aNN_A;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(LoadCommand.ConstantIntoH, memory._internal[0]);
        }

        [TestMethod]
        public void Given_PCIs0hAndIXis0100h_When_JP_aIX_Then_PCIs0100h()
        {
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            memory[0] = LoadCommand.NNintoIX_First;
            memory[1] = LoadCommand.NNintoIX_Second;
            memory[2] = hex_01;
            memory[3] = ZByte.Empty;
            memory[4] = JumpCommand.JP_aIX;
            memory[5] = JumpCommand.JP_aIXY;
            memory[6] = LoadCommand.LD_aNN_A;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(LoadCommand.NNintoIX_First, memory._internal[0]);
        }


        [TestMethod]
        public void Given_PCIs0hAndIYis0100h_When_JP_aIY_Then_PCIs0100h()
        {
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            memory[0] = LoadCommand.NNintoIY_First;
            memory[1] = LoadCommand.NNintoIY_Second;
            memory[2] = hex_01;
            memory[3] = ZByte.Empty;
            memory[4] = JumpCommand.JP_aIY;
            memory[5] = JumpCommand.JP_aIXY;
            memory[6] = LoadCommand.LD_aNN_A;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(LoadCommand.NNintoIY_First, memory._internal[0]);
        }

    }
}

