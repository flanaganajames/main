﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using BitClasses;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.SixteenBit.Load
{
    [TestClass]
    public class Load16_Into_StackPointer_Tests
    {
        private ListOfActionsMemory memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new ListOfActionsMemory();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void LoadHLIntoSP()
        {
            var lowOrderValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highOrderValue = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            var address = new SixteenBitAddress(new ZByte(), new ZByte());
            var secondAddress = new SixteenBitAddress(new ZByte(), secondLowAddressByte);

            memory.ReadValues.Add(address, lowOrderValue);
            memory.ReadValues.Add(secondAddress, highOrderValue);

            cpu.PerformInstruction(LoadCommand.NNintoHL, lowOrderValue, highOrderValue);
            cpu.PerformInstruction(LoadCommand.HLIntoSP);
            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.SPintoaNN_second, new ZByte(), new ZByte());

            Assert.AreEqual(lowOrderValue, memory.Saved[0].Item2);
            Assert.AreEqual(highOrderValue, memory.Saved[1].Item2);
        }


        [TestMethod]
        public void LoadIXIntoSP()
        {
            var lowOrderValue = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highOrderValue = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            var address = new SixteenBitAddress(new ZByte(), new ZByte());
            var secondAddress = new SixteenBitAddress(new ZByte(), secondLowAddressByte);

            memory.ReadValues.Add(address, lowOrderValue);
            memory.ReadValues.Add(secondAddress, highOrderValue);

            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, lowOrderValue, highOrderValue);
            cpu.PerformInstruction(LoadCommand.IXIntoSP_First, LoadCommand.IXIntoSP_Second);
            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.SPintoaNN_second, new ZByte(), new ZByte());

            Assert.AreEqual(lowOrderValue, memory.Saved[0].Item2);
            Assert.AreEqual(highOrderValue, memory.Saved[1].Item2);
        }


        [TestMethod]
        public void LoadIYIntoSP()
        {
            var lowOrderValue = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highOrderValue = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One);

            var secondLowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            var address = new SixteenBitAddress(new ZByte(), new ZByte());
            var secondAddress = new SixteenBitAddress(new ZByte(), secondLowAddressByte);

            memory.ReadValues.Add(address, lowOrderValue);
            memory.ReadValues.Add(secondAddress, highOrderValue);

            cpu.PerformInstruction(LoadCommand.NNintoIY_First, LoadCommand.NNintoIY_Second, lowOrderValue, highOrderValue);
            cpu.PerformInstruction(LoadCommand.IYIntoSP_First, LoadCommand.IYIntoSP_Second);
            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.SPintoaNN_second, new ZByte(), new ZByte());

            Assert.AreEqual(lowOrderValue, memory.Saved[0].Item2);
            Assert.AreEqual(highOrderValue, memory.Saved[1].Item2);
        }
    }
}
