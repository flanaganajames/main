﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using BitClasses;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.SixteenBit.Load
{
    [TestClass]
    public class Pop16_Tests
    {
        private ListOfActionsMemory memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new ListOfActionsMemory();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void POPQQ_BC3355hIntoSP1000h()
        {
            //Populate SP - 1000h
            var lowOrderSPValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var highOrderSPValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, lowOrderSPValue, highOrderSPValue);
            cpu.PerformInstruction(LoadCommand.IXIntoSP_First, LoadCommand.IXIntoSP_Second);

            var finalStackPointerLowByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

            var Address1000h = new SixteenBitAddress(new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero), new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero));
            var Address1001h = new SixteenBitAddress(new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero), new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One));

            //Populate Memory - 3355h
            var lowOrderMemoryValue = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            var highOrderMemoryValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

            memory.ReadValues.Add(Address1000h, new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One));
            memory.ReadValues.Add(Address1001h, new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One));

            cpu.PerformInstruction(LoadCommand.PopBC);
            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.BCintoaNN_second, new ZByte(), new ZByte());
            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.SPintoaNN_second, new ZByte(), new ZByte());

            Assert.AreEqual(lowOrderMemoryValue, memory.Saved[0].Item2);
            Assert.AreEqual(highOrderMemoryValue, memory.Saved[1].Item2);

            Assert.AreEqual(finalStackPointerLowByte, memory.Saved[2].Item2);
            Assert.AreEqual(highOrderSPValue, memory.Saved[3].Item2);
        }

        [TestMethod]
        public void POPIX_IX3355hIntoSP1000h()
        {
            //Populate SP - 1000h
            var lowOrderSPValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var highOrderSPValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, lowOrderSPValue, highOrderSPValue);
            cpu.PerformInstruction(LoadCommand.IXIntoSP_First, LoadCommand.IXIntoSP_Second);

            var finalStackPointerLowByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

            var Address1000h = new SixteenBitAddress(new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero), new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero));
            var Address1001h = new SixteenBitAddress(new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero), new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One));

            //Populate Memory - 3355h
            var lowOrderMemoryValue = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            var highOrderMemoryValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

            memory.ReadValues.Add(Address1000h, new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One));
            memory.ReadValues.Add(Address1001h, new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One));

            cpu.PerformInstruction(LoadCommand.PopIX_First, LoadCommand.PopIX_Second);
            cpu.PerformInstruction(LoadCommand.IXintoNN_First, LoadCommand.IXintoNN_Second, new ZByte(), new ZByte());
            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.SPintoaNN_second, new ZByte(), new ZByte());

            Assert.AreEqual(lowOrderMemoryValue, memory.Saved[0].Item2);
            Assert.AreEqual(highOrderMemoryValue, memory.Saved[1].Item2);

            Assert.AreEqual(finalStackPointerLowByte, memory.Saved[2].Item2);
            Assert.AreEqual(highOrderSPValue, memory.Saved[3].Item2);
        }

        [TestMethod]
        public void POPIY_IY3355hIntoSP1000h()
        {
            //Populate SP - 1000h
            var lowOrderSPValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var highOrderSPValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, lowOrderSPValue, highOrderSPValue);
            cpu.PerformInstruction(LoadCommand.IXIntoSP_First, LoadCommand.IXIntoSP_Second);

            var finalStackPointerLowByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

            var Address1000h = new SixteenBitAddress(new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero), new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero));
            var Address1001h = new SixteenBitAddress(new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero), new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One));

            //Populate Memory - 3355h
            var lowOrderMemoryValue = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            var highOrderMemoryValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

            memory.ReadValues.Add(Address1000h, new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One));
            memory.ReadValues.Add(Address1001h, new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One));

            cpu.PerformInstruction(LoadCommand.PopIY_First, LoadCommand.PopIY_Second);
            cpu.PerformInstruction(LoadCommand.IYintoNN_First, LoadCommand.IYintoNN_Second, new ZByte(), new ZByte());
            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.SPintoaNN_second, new ZByte(), new ZByte());

            Assert.AreEqual(lowOrderMemoryValue, memory.Saved[0].Item2);
            Assert.AreEqual(highOrderMemoryValue, memory.Saved[1].Item2);

            Assert.AreEqual(finalStackPointerLowByte, memory.Saved[2].Item2);
            Assert.AreEqual(highOrderSPValue, memory.Saved[3].Item2);
        }
    }
}
