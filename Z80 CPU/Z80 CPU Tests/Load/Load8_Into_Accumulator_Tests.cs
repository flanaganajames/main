﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using Z80_CPU_Tests.Tests.Classes;
using BitClasses;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.EightBit.Load
{
    [TestClass]
    public class Load8_Into_Accumulator_Tests
    {
        private ListOfActionsMemory memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new ListOfActionsMemory();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void Given_BCHasValue1C42hAndAddress1C42hHasValueF4h_When_LD_A_aBC_Then_AHasValueF4h()
        {
            var hex_42 = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var hex_1C = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero);
            var hex_F4 = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);

            memory.ReadValues.Add(new SixteenBitAddress(hex_1C, hex_42), hex_F4);
            TestHelper.LD_BC_NN(cpu, hex_1C, hex_42);

            cpu.PerformInstruction(LoadCommand.BCIntoA);
            cpu.PerformInstruction(LoadCommand.AIntoMemory);

            Assert.AreEqual(hex_F4, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void Given_DEHasValue1C42hAndAddress1C42hHasValueF4h_When_LD_A_aDE_Then_AHasValueF4h()
        {
            var hex_42 = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var hex_1C = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero);
            var hex_F4 = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);

            memory.ReadValues.Add(new SixteenBitAddress(hex_1C, hex_42), hex_F4);
            TestHelper.LD_DE_NN(cpu, hex_1C, hex_42);

            cpu.PerformInstruction(LoadCommand.DEIntoA);
            cpu.PerformInstruction(LoadCommand.AIntoMemory);

            Assert.AreEqual(hex_F4, memory.Saved[0].Item2);
        }


        [TestMethod]
        public void Given_NNHasValue1C42hAndAddress1C42hHasValue54h_When_LD_A_aNN_Then_AHasValue54h()
        {            
            var hex_42 = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var hex_1C = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero);
            var hex_54 = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);

            memory.ReadValues.Add(new SixteenBitAddress(hex_1C, hex_42), hex_54);

            cpu.PerformInstruction(LoadCommand.LD_A_aNN, hex_42, hex_1C);
            cpu.PerformInstruction(LoadCommand.AIntoMemory);

            Assert.AreEqual(hex_54, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void Given_AIs00handIIs10h_When_LD_A_I_Then_AIs10h()
        {                        
            var hex_10 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            memory.ReadValues.Add(SixteenBitAddress.EmptyAddress, hex_10);
            memory.ReadValues.Add(new SixteenBitAddress(ZByte.Empty, hex_10), ZByte.Empty);

            cpu.PerformInstruction(LoadCommand.LD_A_aNN, ZByte.Empty, ZByte.Empty);
            cpu.PerformInstruction(LoadCommand.LD_I_A_First, LoadCommand.LD_I_A_Second);
            cpu.PerformInstruction(LoadCommand.LD_A_aNN, hex_10, ZByte.Empty);
            cpu.PerformInstruction(LoadCommand.LD_A_I_First, LoadCommand.LD_A_I_Second);
            cpu.PerformInstruction(LoadCommand.AIntoMemory);

            Assert.AreEqual(hex_10, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void Given_AIs00h_When_LD_A_I_Then_ZFlagIsSet()
        {
            var mymemory = new TrueMemorySimulator();
            cpu = new CPU(mymemory);

            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(LoadCommand.LD_A_I_First, LoadCommand.LD_A_I_Second);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, mymemory.GetValue(expectedStackAddress)[EightBitLocation.Six]);
        }

        [TestMethod]
        public void Given_AIs10h_When_LD_A_I_Then_ZFlagIsNotSet()
        {
            var mymemory = new TrueMemorySimulator();
            cpu = new CPU(mymemory);

            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();
            var hex_10h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            mymemory._internal[0] = hex_10h;

            cpu.PerformInstruction(LoadCommand.LD_A_aNN, ZByte.Empty, ZByte.Empty);
            cpu.PerformInstruction(LoadCommand.LD_I_A_First, LoadCommand.LD_I_A_Second);
            cpu.PerformInstruction(LoadCommand.LD_A_I_First, LoadCommand.LD_A_I_Second);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, mymemory.GetValue(expectedStackAddress)[EightBitLocation.Six]);
        }

        [TestMethod]
        public void Given_AIsF0h_When_LD_A_I_Then_SFlagIsSet()
        {
            var mymemory = new TrueMemorySimulator();
            cpu = new CPU(mymemory);

            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();
            var hex_F0h = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            mymemory._internal[0] = hex_F0h;

            cpu.PerformInstruction(LoadCommand.LD_A_aNN, ZByte.Empty, ZByte.Empty);
            cpu.PerformInstruction(LoadCommand.LD_I_A_First, LoadCommand.LD_I_A_Second);
            cpu.PerformInstruction(LoadCommand.LD_A_I_First, LoadCommand.LD_A_I_Second);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, mymemory.GetValue(expectedStackAddress)[EightBitLocation.Seven]);
        }

        [TestMethod]
        public void Given_IFF2IsSet_When_LD_A_I_Then_PVFlagIsSet()
        {
            var mymemory = new TrueMemorySimulator();
            cpu = new CPU(mymemory);

            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(CPUControlCommand.EI);
            cpu.PerformInstruction(LoadCommand.LD_A_I_First, LoadCommand.LD_A_I_Second);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, mymemory.GetValue(expectedStackAddress)[EightBitLocation.Two]);
        }


    }
}
