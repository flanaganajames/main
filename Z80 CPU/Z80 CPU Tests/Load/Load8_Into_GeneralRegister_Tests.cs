﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using BitClasses;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.EightBit.Load
{
    [TestClass]
    public class Load8_Into_GeneralRegister_Tests
    {
        private ListOfActionsMemory memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new ListOfActionsMemory();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void RegisterBContainsConstantValueLoadedIn()
        {
            var valueToLoad = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);

            cpu.PerformInstruction(LoadCommand.ConstantIntoB, valueToLoad);
            cpu.PerformInstruction(LoadCommand.BIntoMemory);

            Assert.AreEqual(valueToLoad, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void RegisterDContainsConstantValueLoadedIn()
        {
            var valueToLoad = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

            cpu.PerformInstruction(LoadCommand.ConstantIntoD, valueToLoad);
            cpu.PerformInstruction(LoadCommand.DIntoMemory);

            Assert.AreEqual(valueToLoad, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void MovingValuesBetweenRegistersCorrect()
        {
            var valueToLoad = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            cpu.PerformInstruction(LoadCommand.ConstantIntoB, valueToLoad);
            cpu.PerformInstruction(LoadCommand.BIntoD);
            cpu.PerformInstruction(LoadCommand.DIntoMemory);

            Assert.AreEqual(valueToLoad, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void LoadingValueOfLocation0IntoRegisterB()
        {
            var constantValue = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
            var HvalueToLoad = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);
            var LvalueToLoad = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero);

            cpu.PerformInstruction(LoadCommand.ConstantIntoH, HvalueToLoad);
            cpu.PerformInstruction(LoadCommand.ConstantIntoL, LvalueToLoad);
            cpu.PerformInstruction(LoadCommand.LD_aHL_n, constantValue);

            Assert.AreEqual(constantValue, memory.Saved[0].Item2);
            Assert.AreEqual(new SixteenBitAddress(HvalueToLoad, LvalueToLoad), memory.Saved[0].Item1);
        }


        [TestMethod]
        public void LoadingAValueIntoC_ValueCorrect()
        {
            var lowByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var highByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);
            var value = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);
            memory.ReadValues.Add(new SixteenBitAddress(highByte, lowByte), value);

            cpu.PerformInstruction(LoadCommand.ConstantIntoH, highByte);
            cpu.PerformInstruction(LoadCommand.ConstantIntoL, lowByte);
            cpu.PerformInstruction(LoadCommand.MemoryIntoC);
            cpu.PerformInstruction(LoadCommand.CIntoMemory);

            Assert.AreEqual(value, memory.Saved[0].Item2);
        }



        [TestMethod]
        public void LoadNonPopulatedValueIntoC()
        {
            var lowByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var highByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);

            var value = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);

            memory.ReadValues.Add(new SixteenBitAddress(highByte, lowByte), value);

            lowByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

            cpu.PerformInstruction(LoadCommand.ConstantIntoH, highByte);
            cpu.PerformInstruction(LoadCommand.ConstantIntoL, lowByte);
            cpu.PerformInstruction(LoadCommand.MemoryIntoC);
            cpu.PerformInstruction(LoadCommand.CIntoMemory);

            var returnedValue = memory.Saved[0].Item2;

            Assert.AreEqual(new ZByte(), returnedValue);
        }

        [TestMethod]
        public void LoadIYdAddressValueIntoRegisterB()
        {
            var loworderIYAddress = new ZByte(Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);
            var highorderIYAddress = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            var valueByte = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One);
            var displacementByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One);

            var resultLowOrderAddress = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);

            var resultAddress = new SixteenBitAddress(highorderIYAddress, resultLowOrderAddress);
            memory.ReadValues.Add(resultAddress, valueByte);

            cpu.PerformInstruction(LoadCommand.NNintoIY_First, LoadCommand.NNintoIY_Second, loworderIYAddress, highorderIYAddress);
            cpu.PerformInstruction(LoadCommand.IYdIntoR_first, LoadCommand.IYdIntoB_second, displacementByte);
            cpu.PerformInstruction(LoadCommand.BIntoMemory);

            Assert.AreEqual(valueByte, memory.Saved[0].Item2);
        }



        [TestMethod]
        public void LoadIXdAddressValueIntoRegisterB()
        {
            var loworderIXAddress = new ZByte(Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);
            var highorderIXAddress = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            var valueByte = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One);
            var displacementByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One);

            var resultLowOrderAddress = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);

            var resultAddress = new SixteenBitAddress(highorderIXAddress, resultLowOrderAddress);
            memory.ReadValues.Add(resultAddress, valueByte);

            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, loworderIXAddress, highorderIXAddress);
            cpu.PerformInstruction(LoadCommand.IXdIntoR_first, LoadCommand.IXdIntoB_second, displacementByte);
            cpu.PerformInstruction(LoadCommand.BIntoMemory);

            Assert.AreEqual(valueByte, memory.Saved[0].Item2);
        }



    }
}
