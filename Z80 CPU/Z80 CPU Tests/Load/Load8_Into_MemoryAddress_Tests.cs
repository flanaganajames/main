﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using BitClasses;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.EightBit.Load
{
    [TestClass]
    public class Load8_Into_MemoryAddress_Tests
    {
        private ListOfActionsMemory memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new ListOfActionsMemory();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void LD_aHL_B_ValueCorrect_00h_01h()
        {
            var valueToLoad = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            cpu.PerformInstruction(LoadCommand.ConstantIntoB, valueToLoad);
            cpu.PerformInstruction(LoadCommand.BIntoMemory);

            Assert.AreEqual(valueToLoad, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void Saving_29h_ToMemoryLocation_2146h_FromRegisterB()
        {
            var BvalueToLoad = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
            var HvalueToLoad = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);
            var LvalueToLoad = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

            cpu.PerformInstruction(LoadCommand.ConstantIntoB, BvalueToLoad);
            cpu.PerformInstruction(LoadCommand.ConstantIntoH, HvalueToLoad);
            cpu.PerformInstruction(LoadCommand.ConstantIntoL, LvalueToLoad);
            cpu.PerformInstruction(LoadCommand.BIntoMemory);

            Assert.AreEqual(BvalueToLoad, memory.Saved[0].Item2);
            Assert.AreEqual(new SixteenBitAddress(HvalueToLoad, LvalueToLoad), memory.Saved[0].Item1);
        }


        [TestMethod]
        public void SavingConstantToMemoryLocation_0h()
        {
            var valueToLoad = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            cpu.PerformInstruction(LoadCommand.LD_aHL_n, valueToLoad);

            Assert.AreEqual(valueToLoad, memory.Saved[0].Item2);
        }


        [TestMethod]
        public void Saving_Constant28h_ToMemoryLocation_4444h()
        {
            var constantValue = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
            var HvalueToLoad = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);
            var LvalueToLoad = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero);

            cpu.PerformInstruction(LoadCommand.ConstantIntoH, HvalueToLoad);
            cpu.PerformInstruction(LoadCommand.ConstantIntoL, LvalueToLoad);
            cpu.PerformInstruction(LoadCommand.LD_aHL_n, constantValue);

            Assert.AreEqual(constantValue, memory.Saved[0].Item2);
            Assert.AreEqual(new SixteenBitAddress(HvalueToLoad, LvalueToLoad), memory.Saved[0].Item1);
        }



        [TestMethod]
        public void LoadAIntoBCAddressValue()
        {
            var lowByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var highByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero);
            var value = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            var address = new SixteenBitAddress(highByte, lowByte);

            cpu.PerformInstruction(LoadCommand.ConstantIntoB, highByte);
            cpu.PerformInstruction(LoadCommand.ConstantIntoC, lowByte);
            cpu.PerformInstruction(LoadCommand.LD_A_n, value);
            cpu.PerformInstruction(LoadCommand.AIntoBC);

            var returnedValue = memory.Saved[0].Item2;

            Assert.AreEqual(value, returnedValue);
            Assert.AreEqual(address, memory.Saved[0].Item1);
        }

        [TestMethod]
        public void LoadAIntoDEAddressValue()
        {
            var lowByte = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var highByte = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero);
            var value = new ZByte(Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);

            var address = new SixteenBitAddress(highByte, lowByte);

            cpu.PerformInstruction(LoadCommand.ConstantIntoD, highByte);
            cpu.PerformInstruction(LoadCommand.ConstantIntoE, lowByte);
            cpu.PerformInstruction(LoadCommand.LD_A_n, value);
            cpu.PerformInstruction(LoadCommand.AIntoDE);

            var returnedValue = memory.Saved[0].Item2;

            Assert.AreEqual(value, returnedValue);
            Assert.AreEqual(address, memory.Saved[0].Item1);
        }

        [TestMethod]
        public void LoadAIntoNNAddressValue()
        {
            var lowByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highByte = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero);

            var value = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
            var address = new SixteenBitAddress(highByte, lowByte);

            cpu.PerformInstruction(LoadCommand.LD_A_n, value);
            cpu.PerformInstruction(LoadCommand.LD_aNN_A, lowByte, highByte);

            Assert.AreEqual(address, memory.Saved[0].Item1);
            Assert.AreEqual(value, memory.Saved[0].Item2);
        }

    }
}
