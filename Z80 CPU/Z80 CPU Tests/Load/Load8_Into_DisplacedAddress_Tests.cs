﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using BitClasses;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.EightBit.Load
{
    [TestClass]
    public class Load8_Into_DisplacedAddress_Tests
    {
        private ListOfActionsMemory memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new ListOfActionsMemory();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void LoadConstantIntoIXdAddressValue()
        {  

            var loworderIXAddress = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highorderIXAddress = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            var valueByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var displacementByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            var resultLowOrderAddress = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            var resultAddress = new SixteenBitAddress(highorderIXAddress, resultLowOrderAddress);

            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, loworderIXAddress, highorderIXAddress);
            cpu.PerformInstruction(LoadCommand.ConstantIntoIXd_first, LoadCommand.ConstantIntoIXd_second, displacementByte, valueByte);

            Assert.AreEqual(resultAddress, memory.Saved[0].Item1);
            Assert.AreEqual(valueByte, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void LoadConstantIntoIXdAddressValueWithNegativeDisplacement()
        {                       

            var loworderIXAddress = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highorderIXAddress = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            var valueByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var displacementByte = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One);

            var resultLowOrderAddress = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            var resultAddress = new SixteenBitAddress(highorderIXAddress, resultLowOrderAddress);

            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, loworderIXAddress, highorderIXAddress);
            cpu.PerformInstruction(LoadCommand.ConstantIntoIXd_first, LoadCommand.ConstantIntoIXd_second, displacementByte, valueByte);

            Assert.AreEqual(resultAddress, memory.Saved[0].Item1);
            Assert.AreEqual(valueByte, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void LoadConstantIntoIXdAddressValueWithNegativeAddress()
        {
            var valueByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var displacementByte = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One);

            var resulthighorderAddress = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);
            var resultLowOrderAddress = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One);

            var resultAddress = new SixteenBitAddress(resulthighorderAddress, resultLowOrderAddress);

            cpu.PerformInstruction(LoadCommand.ConstantIntoIXd_first, LoadCommand.ConstantIntoIXd_second, displacementByte, valueByte);

            Assert.AreEqual(resultAddress, memory.Saved[0].Item1);
            Assert.AreEqual(valueByte, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void LoadConstantIntoIYdAddressValue()
        {            
            var loworderIYAddress = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highorderIYAddress = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            var valueByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var displacementByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            var resultLowOrderAddress = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            var resultAddress = new SixteenBitAddress(highorderIYAddress, resultLowOrderAddress);

            cpu.PerformInstruction(LoadCommand.NNintoIY_First, LoadCommand.NNintoIY_Second, loworderIYAddress, highorderIYAddress);
            cpu.PerformInstruction(LoadCommand.ConstantIntoIYd_first, LoadCommand.ConstantIntoIYd_second, displacementByte, valueByte);

            Assert.AreEqual(resultAddress, memory.Saved[0].Item1);
            Assert.AreEqual(valueByte, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void LoadConstantIntoIYdAddressValueWithNegativeDisplacement()
        {
            var loworderIYAddress = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highorderIYAddress = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            var valueByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var displacementByte = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One);

            var resultLowOrderAddress = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            var resultAddress = new SixteenBitAddress(highorderIYAddress, resultLowOrderAddress);

            cpu.PerformInstruction(LoadCommand.NNintoIY_First, LoadCommand.NNintoIY_Second, loworderIYAddress, highorderIYAddress);
            cpu.PerformInstruction(LoadCommand.ConstantIntoIYd_first, LoadCommand.ConstantIntoIYd_second, displacementByte, valueByte);

            Assert.AreEqual(resultAddress, memory.Saved[0].Item1);
            Assert.AreEqual(valueByte, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void LoadConstantIntoIYdAddressValueWithNegativeAddress()
        {
            var valueByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var displacementByte = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One);

            var resulthighorderAddress = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);
            var resultLowOrderAddress = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One);

            var resultAddress = new SixteenBitAddress(resulthighorderAddress, resultLowOrderAddress);

            cpu.PerformInstruction(LoadCommand.ConstantIntoIYd_first, LoadCommand.ConstantIntoIYd_second, displacementByte, valueByte);

            Assert.AreEqual(resultAddress, memory.Saved[0].Item1);
            Assert.AreEqual(valueByte, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void LoadRegisterBIntoIYdAddressValue()
        {
            var loworderIYAddress = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highorderIYAddress = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            var valueByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var displacementByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            var resultLowOrderAddress = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            var resultAddress = new SixteenBitAddress(highorderIYAddress, resultLowOrderAddress);

            cpu.PerformInstruction(LoadCommand.ConstantIntoB, valueByte);
            cpu.PerformInstruction(LoadCommand.NNintoIY_First, LoadCommand.NNintoIY_Second, loworderIYAddress, highorderIYAddress);
            cpu.PerformInstruction(LoadCommand.RIntoIYd_first, LoadCommand.BIntoIYd_second, displacementByte);

            Assert.AreEqual(resultAddress, memory.Saved[0].Item1);
            Assert.AreEqual(valueByte, memory.Saved[0].Item2);
        }

        [TestMethod]
        public void LoadRegisterBIntoIXdAddressValue()
        {
            var loworderIXAddress = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highorderIXAddress = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            var valueByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var displacementByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            var resultLowOrderAddress = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            var resultAddress = new SixteenBitAddress(highorderIXAddress, resultLowOrderAddress);

            cpu.PerformInstruction(LoadCommand.ConstantIntoB, valueByte);
            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, loworderIXAddress, highorderIXAddress);
            cpu.PerformInstruction(LoadCommand.RIntoIXd_first, LoadCommand.BIntoIXd_second, displacementByte);

            Assert.AreEqual(resultAddress, memory.Saved[0].Item1);
            Assert.AreEqual(valueByte, memory.Saved[0].Item2);
        }
    }
}
