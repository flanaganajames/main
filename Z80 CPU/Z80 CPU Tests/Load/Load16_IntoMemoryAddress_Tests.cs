﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using BitClasses;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.SixteenBit.Load
{
    [TestClass]
    public class Load16_IntoMemoryAddress_Tests
    {
        private ListOfActionsMemory memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new ListOfActionsMemory();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void Load0hOutOfIXRegisterToAddress()
        {
            var lowAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero);
            var highAddressByte = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);

            var address = new SixteenBitAddress(highAddressByte, lowAddressByte);
            var secondAddress = new SixteenBitAddress(highAddressByte, secondLowAddressByte);

            cpu.PerformInstruction(LoadCommand.IXintoNN_First, LoadCommand.IXintoNN_Second, lowAddressByte, highAddressByte);

            Assert.AreEqual(address, memory.Saved[0].Item1);
            Assert.AreEqual(new ZByte(), memory.Saved[0].Item2);

            Assert.AreEqual(secondAddress, memory.Saved[1].Item1);
            Assert.AreEqual(new ZByte(), memory.Saved[1].Item2);
        }

        [TestMethod]
        public void Load5A30hOutOfIXRegisterToAddress4392h()
        {
            var lowOrderIXValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var highOrderIXValue = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, lowOrderIXValue, highOrderIXValue);

            var lowAddressByte = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var highAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

            var secondLowAddressByte = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

            var address = new SixteenBitAddress(highAddressByte, lowAddressByte);
            var secondAddress = new SixteenBitAddress(highAddressByte, secondLowAddressByte);

            cpu.PerformInstruction(LoadCommand.IXintoNN_First, LoadCommand.IXintoNN_Second, lowAddressByte, highAddressByte);

            Assert.AreEqual(address, memory.Saved[0].Item1);
            Assert.AreEqual(lowOrderIXValue, memory.Saved[0].Item2);

            Assert.AreEqual(secondAddress, memory.Saved[1].Item1);
            Assert.AreEqual(highOrderIXValue, memory.Saved[1].Item2);
        }

        [TestMethod]
        public void Load2A50hOutOfIXRegisterToAddress7694h()
        {
            var lowOrderIXValue = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var highOrderIXValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, lowOrderIXValue, highOrderIXValue);

            var lowAddressByte = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);
            var highAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            var address = new SixteenBitAddress(highAddressByte, lowAddressByte);
            var secondAddress = new SixteenBitAddress(highAddressByte, secondLowAddressByte);

            cpu.PerformInstruction(LoadCommand.IXintoNN_First, LoadCommand.IXintoNN_Second, lowAddressByte, highAddressByte);

            Assert.AreEqual(address, memory.Saved[0].Item1);
            Assert.AreEqual(lowOrderIXValue, memory.Saved[0].Item2);

            Assert.AreEqual(secondAddress, memory.Saved[1].Item1);
            Assert.AreEqual(highOrderIXValue, memory.Saved[1].Item2);
        }

        [TestMethod]
        public void Load0hOutOfIYRegisterToAddress()
        {
            var lowAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero);
            var highAddressByte = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);

            var address = new SixteenBitAddress(highAddressByte, lowAddressByte);
            var secondAddress = new SixteenBitAddress(highAddressByte, secondLowAddressByte);

            cpu.PerformInstruction(LoadCommand.IYintoNN_First, LoadCommand.IYintoNN_Second, lowAddressByte, highAddressByte);

            Assert.AreEqual(address, memory.Saved[0].Item1);
            Assert.AreEqual(new ZByte(), memory.Saved[0].Item2);

            Assert.AreEqual(secondAddress, memory.Saved[1].Item1);
            Assert.AreEqual(new ZByte(), memory.Saved[1].Item2);
        }

        [TestMethod]
        public void Load5A30hOutOfIYRegisterToAddress4392h()
        {
            var lowOrderIYValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var highOrderIYValue = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

            cpu.PerformInstruction(LoadCommand.NNintoIY_First, LoadCommand.NNintoIY_Second, lowOrderIYValue, highOrderIYValue);

            var lowAddressByte = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var highAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

            var secondLowAddressByte = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

            var address = new SixteenBitAddress(highAddressByte, lowAddressByte);
            var secondAddress = new SixteenBitAddress(highAddressByte, secondLowAddressByte);

            cpu.PerformInstruction(LoadCommand.IYintoNN_First, LoadCommand.IYintoNN_Second, lowAddressByte, highAddressByte);

            Assert.AreEqual(address, memory.Saved[0].Item1);
            Assert.AreEqual(lowOrderIYValue, memory.Saved[0].Item2);

            Assert.AreEqual(secondAddress, memory.Saved[1].Item1);
            Assert.AreEqual(highOrderIYValue, memory.Saved[1].Item2);
        }

        [TestMethod]
        public void Load2A50hOutOfIYRegisterToAddress7694h()
        {
            var lowOrderIYValue = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var highOrderIYValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

            cpu.PerformInstruction(LoadCommand.NNintoIY_First, LoadCommand.NNintoIY_Second, lowOrderIYValue, highOrderIYValue);

            var lowAddressByte = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);
            var highAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            var address = new SixteenBitAddress(highAddressByte, lowAddressByte);
            var secondAddress = new SixteenBitAddress(highAddressByte, secondLowAddressByte);

            cpu.PerformInstruction(LoadCommand.IYintoNN_First, LoadCommand.IYintoNN_Second, lowAddressByte, highAddressByte);

            Assert.AreEqual(address, memory.Saved[0].Item1);
            Assert.AreEqual(lowOrderIYValue, memory.Saved[0].Item2);

            Assert.AreEqual(secondAddress, memory.Saved[1].Item1);
            Assert.AreEqual(highOrderIYValue, memory.Saved[1].Item2);
        }



        [TestMethod]
        public void Load483AhOutOfHLRegisterToAddressB229h()
        {
            var lowOrderHLValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highOrderHLValue = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);

            cpu.PerformInstruction(LoadCommand.ConstantIntoH, highOrderHLValue);
            cpu.PerformInstruction(LoadCommand.ConstantIntoL, lowOrderHLValue);

            var lowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One);
            var highAddressByte = new ZByte(Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

            var address = new SixteenBitAddress(highAddressByte, lowAddressByte);
            var secondAddress = new SixteenBitAddress(highAddressByte, secondLowAddressByte);

            cpu.PerformInstruction(LoadCommand.HLIntoaNN, lowAddressByte, highAddressByte);

            Assert.AreEqual(address, memory.Saved[0].Item1);
            Assert.AreEqual(lowOrderHLValue, memory.Saved[0].Item2);

            Assert.AreEqual(secondAddress, memory.Saved[1].Item1);
            Assert.AreEqual(highOrderHLValue, memory.Saved[1].Item2);
        }

        [TestMethod]
        public void Load483AhOutOfBCRegisterToAddressB229h()
        {
            var lowOrderHLValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highOrderHLValue = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);

            cpu.PerformInstruction(LoadCommand.ConstantIntoB, highOrderHLValue);
            cpu.PerformInstruction(LoadCommand.ConstantIntoC, lowOrderHLValue);

            var lowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One);
            var highAddressByte = new ZByte(Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

            var address = new SixteenBitAddress(highAddressByte, lowAddressByte);
            var secondAddress = new SixteenBitAddress(highAddressByte, secondLowAddressByte);

            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.BCintoaNN_second, lowAddressByte, highAddressByte);

            Assert.AreEqual(address, memory.Saved[0].Item1);
            Assert.AreEqual(lowOrderHLValue, memory.Saved[0].Item2);

            Assert.AreEqual(secondAddress, memory.Saved[1].Item1);
            Assert.AreEqual(highOrderHLValue, memory.Saved[1].Item2);
        }

        [TestMethod]
        public void Load483AhOutOfDERegisterToAddressB229h()
        {
            var lowOrderHLValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highOrderHLValue = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);

            cpu.PerformInstruction(LoadCommand.ConstantIntoD, highOrderHLValue);
            cpu.PerformInstruction(LoadCommand.ConstantIntoE, lowOrderHLValue);

            var lowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One);
            var highAddressByte = new ZByte(Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

            var address = new SixteenBitAddress(highAddressByte, lowAddressByte);
            var secondAddress = new SixteenBitAddress(highAddressByte, secondLowAddressByte);

            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.DEintoaNN_second, lowAddressByte, highAddressByte);

            Assert.AreEqual(address, memory.Saved[0].Item1);
            Assert.AreEqual(lowOrderHLValue, memory.Saved[0].Item2);

            Assert.AreEqual(secondAddress, memory.Saved[1].Item1);
            Assert.AreEqual(highOrderHLValue, memory.Saved[1].Item2);
        }
    }
}
