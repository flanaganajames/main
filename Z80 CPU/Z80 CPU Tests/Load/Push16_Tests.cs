﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using BitClasses;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.SixteenBit.Load
{
    [TestClass]
    public class Push16_Tests
    {
        private ListOfActionsMemory memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new ListOfActionsMemory();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void PUSHIX_IX2233hIntoSP1007h()
        {
            //Populate SP - 1007h
            var lowOrderSPValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One);
            var highOrderSPValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, lowOrderSPValue, highOrderSPValue);
            cpu.PerformInstruction(LoadCommand.IXIntoSP_First, LoadCommand.IXIntoSP_Second);

            //Populate IX - 2233h
            var lowOrderIXValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One);
            var highOrderIXValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, lowOrderIXValue, highOrderIXValue);

            var highOrder_lowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
            var lowOrder_lowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            var highOrderAddress = new SixteenBitAddress(highOrderSPValue, highOrder_lowAddressByte);
            var lowOrderAddress = new SixteenBitAddress(highOrderSPValue, lowOrder_lowAddressByte);

            var finalStackPointerLowByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            cpu.PerformInstruction(LoadCommand.PushIX_First, LoadCommand.PushIX_Second);
            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.SPintoaNN_second, new ZByte(), new ZByte());

            Assert.AreEqual(highOrderAddress, memory.Saved[0].Item1);
            Assert.AreEqual(highOrderIXValue, memory.Saved[0].Item2);

            Assert.AreEqual(lowOrderAddress, memory.Saved[1].Item1);
            Assert.AreEqual(lowOrderIXValue, memory.Saved[1].Item2);

            Assert.AreEqual(finalStackPointerLowByte, memory.Saved[2].Item2);
            Assert.AreEqual(highOrderSPValue, memory.Saved[3].Item2);
        }

        [TestMethod]
        public void PUSHIY_IY2233hIntoSP1007h()
        {
            //Populate SP - 1007h
            var lowOrderSPValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One);
            var highOrderSPValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, lowOrderSPValue, highOrderSPValue);
            cpu.PerformInstruction(LoadCommand.IXIntoSP_First, LoadCommand.IXIntoSP_Second);

            //Populate IY - 2233h
            var lowOrderIYValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One);
            var highOrderIYValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            cpu.PerformInstruction(LoadCommand.NNintoIY_First, LoadCommand.NNintoIX_Second, lowOrderIYValue, highOrderIYValue);

            var highOrder_lowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
            var lowOrder_lowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            var highOrderAddress = new SixteenBitAddress(highOrderSPValue, highOrder_lowAddressByte);
            var lowOrderAddress = new SixteenBitAddress(highOrderSPValue, lowOrder_lowAddressByte);

            var finalStackPointerLowByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            cpu.PerformInstruction(LoadCommand.PushIY_First, LoadCommand.PushIY_Second);
            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.SPintoaNN_second, new ZByte(), new ZByte());

            Assert.AreEqual(highOrderAddress, memory.Saved[0].Item1);
            Assert.AreEqual(highOrderIYValue, memory.Saved[0].Item2);

            Assert.AreEqual(lowOrderAddress, memory.Saved[1].Item1);
            Assert.AreEqual(lowOrderIYValue, memory.Saved[1].Item2);

            Assert.AreEqual(finalStackPointerLowByte, memory.Saved[2].Item2);
            Assert.AreEqual(highOrderSPValue, memory.Saved[3].Item2);
        }

        [TestMethod]
        public void PUSHQQ_BC2233hIntoSP1007h()
        {
            //Populate SP - 1007h
            var lowOrderSPValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One);
            var highOrderSPValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, lowOrderSPValue, highOrderSPValue);
            cpu.PerformInstruction(LoadCommand.IXIntoSP_First, LoadCommand.IXIntoSP_Second);

            //Populate BC - 2233h
            var lowOrderBCValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One);
            var highOrderBCValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            cpu.PerformInstruction(LoadCommand.NNintoBC, lowOrderBCValue, highOrderBCValue);

            var highOrder_lowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
            var lowOrder_lowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            var highOrderAddress = new SixteenBitAddress(highOrderSPValue, highOrder_lowAddressByte);
            var lowOrderAddress = new SixteenBitAddress(highOrderSPValue, lowOrder_lowAddressByte);

            var finalStackPointerLowByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            cpu.PerformInstruction(LoadCommand.PushBC);
            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.SPintoaNN_second, new ZByte(), new ZByte());

            Assert.AreEqual(highOrderAddress, memory.Saved[0].Item1);
            Assert.AreEqual(highOrderBCValue, memory.Saved[0].Item2);

            Assert.AreEqual(lowOrderAddress, memory.Saved[1].Item1);
            Assert.AreEqual(lowOrderBCValue, memory.Saved[1].Item2);

            Assert.AreEqual(finalStackPointerLowByte, memory.Saved[2].Item2);
            Assert.AreEqual(highOrderSPValue, memory.Saved[3].Item2);
        }

    }
}
