﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using BitClasses;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.SixteenBit.Load
{
    [TestClass]
    public class Load16_Into_GeneralRegister_Tests
    {
        private ListOfActionsMemory memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new ListOfActionsMemory();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void LoadDA92hFromMemoryAddress6666hIntoIX()
        {
            var lowAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
            var highAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One);

            var NineTwoH = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var DaH = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

            memory.ReadValues.Add(new SixteenBitAddress(highAddressByte, lowAddressByte), NineTwoH);
            memory.ReadValues.Add(new SixteenBitAddress(highAddressByte, secondLowAddressByte), DaH);

            cpu.PerformInstruction(LoadCommand.aNNintoIX_First, LoadCommand.aNNintoIX_Second, lowAddressByte, highAddressByte);
            cpu.PerformInstruction(LoadCommand.IXintoNN_First, LoadCommand.IXintoNN_Second, lowAddressByte, highAddressByte);

            Assert.AreEqual(new SixteenBitAddress(highAddressByte, lowAddressByte), memory.Saved[0].Item1);
            Assert.AreEqual(NineTwoH, memory.Saved[0].Item2);

            Assert.AreEqual(new SixteenBitAddress(highAddressByte, secondLowAddressByte), memory.Saved[1].Item1);
            Assert.AreEqual(DaH, memory.Saved[1].Item2);
        }

        [TestMethod]
        public void LoadDA92hFromMemoryAddress6666hIntoIY()
        {
            var lowAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
            var highAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One);

            var NineTwoH = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var DaH = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

            memory.ReadValues.Add(new SixteenBitAddress(highAddressByte, lowAddressByte), NineTwoH);
            memory.ReadValues.Add(new SixteenBitAddress(highAddressByte, secondLowAddressByte), DaH);

            cpu.PerformInstruction(LoadCommand.aNNintoIY_First, LoadCommand.aNNintoIY_Second, lowAddressByte, highAddressByte);
            cpu.PerformInstruction(LoadCommand.IYintoNN_First, LoadCommand.IYintoNN_Second, lowAddressByte, highAddressByte);

            Assert.AreEqual(new SixteenBitAddress(highAddressByte, lowAddressByte), memory.Saved[0].Item1);
            Assert.AreEqual(NineTwoH, memory.Saved[0].Item2);

            Assert.AreEqual(new SixteenBitAddress(highAddressByte, secondLowAddressByte), memory.Saved[1].Item1);
            Assert.AreEqual(DaH, memory.Saved[1].Item2);
        }


        [TestMethod]
        public void Load483AIntoDERegister()
        {
            var lowOrderValue = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var highOrderValue = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            var address = new SixteenBitAddress(new ZByte(), new ZByte());
            var secondAddress = new SixteenBitAddress(new ZByte(), secondLowAddressByte);

            cpu.PerformInstruction(LoadCommand.NNintoDE, lowOrderValue, highOrderValue);
            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.DEintoaNN_second, new ZByte(), new ZByte());

            Assert.AreEqual(lowOrderValue, memory.Saved[0].Item2);
            Assert.AreEqual(highOrderValue, memory.Saved[1].Item2);
        }

        [TestMethod]
        public void LoadDA92hFromMemoryAddress6666hIntoHL()
        {
            var lowAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
            var highAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One);

            var NineTwoH = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var DaH = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

            memory.ReadValues.Add(new SixteenBitAddress(highAddressByte, lowAddressByte), NineTwoH);
            memory.ReadValues.Add(new SixteenBitAddress(highAddressByte, secondLowAddressByte), DaH);

            cpu.PerformInstruction(LoadCommand.aNNintoHL, lowAddressByte, highAddressByte);
            cpu.PerformInstruction(LoadCommand.HLIntoaNN, lowAddressByte, highAddressByte);

            Assert.AreEqual(new SixteenBitAddress(highAddressByte, lowAddressByte), memory.Saved[0].Item1);
            Assert.AreEqual(NineTwoH, memory.Saved[0].Item2);

            Assert.AreEqual(new SixteenBitAddress(highAddressByte, secondLowAddressByte), memory.Saved[1].Item1);
            Assert.AreEqual(DaH, memory.Saved[1].Item2);
        }

        [TestMethod]
        public void LoadDA92hFromMemoryAddress6666hIntoBC()
        {
            var lowAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
            var highAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

            var secondLowAddressByte = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One);

            var NineTwoH = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var DaH = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

            memory.ReadValues.Add(new SixteenBitAddress(highAddressByte, lowAddressByte), NineTwoH);
            memory.ReadValues.Add(new SixteenBitAddress(highAddressByte, secondLowAddressByte), DaH);

            cpu.PerformInstruction(LoadCommand.LD_dd_aNN_First, LoadCommand.aNNintoBC_second, lowAddressByte, highAddressByte);
            cpu.PerformInstruction(LoadCommand.DDintoaNN_first, LoadCommand.BCintoaNN_second, lowAddressByte, highAddressByte);

            Assert.AreEqual(new SixteenBitAddress(highAddressByte, lowAddressByte), memory.Saved[0].Item1);
            Assert.AreEqual(NineTwoH, memory.Saved[0].Item2);

            Assert.AreEqual(new SixteenBitAddress(highAddressByte, secondLowAddressByte), memory.Saved[1].Item1);
            Assert.AreEqual(DaH, memory.Saved[1].Item2);
        }


    }
}
