﻿using BaseClasses;
using BitClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using Z80_CPU_Tests.Tests.Classes;

namespace Z80_CPU_Tests.RotateAndShift
{
    [TestClass]
    public class RLC_Tests
    {
        TrueMemorySimulator memory;
        CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            cpu = new CPU();
            memory = new TrueMemorySimulator();
            cpu.SetMemory(memory);
        }

        [TestMethod]
        public void Given_AIs88h_When_RLCA_ThenAIs11hAndCarryFlagSet()
        {
            var hex_88 = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);
            var hex_11 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);            

            memory[0] = LoadCommand.LD_A_n;
            memory[1] = hex_88;            
            cpu.PerformInstruction();

            memory[2] = RotateCommand.RLCA;
            cpu.PerformInstruction();

            memory[3] = SixteenBitArithmeticCommand.Increment_SP;
            memory[4] = SixteenBitArithmeticCommand.Increment_SP;
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            
            memory[5] = LoadCommand.PushAF;
            cpu.PerformInstruction();

            Assert.AreEqual(hex_11, memory[1]);
            Assert.AreEqual(Bit.One, memory[2][EightBitLocation.Zero]);
        }

        [TestMethod]
        public void Given_HFlagSet_When_RLCA_Then_HFlagClear()
        {
            var hex_88 = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);            
            var hex_FF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            memory[0] = LoadCommand.LD_SP_nn;
            memory[1] = hex_88;
            memory[2] = ZByte.Empty;
            cpu.PerformInstruction();

            memory[136] = hex_FF;
            memory[3] = LoadCommand.PopAF;
            cpu.PerformInstruction();

            memory[4] = RotateCommand.RLCA;
            cpu.PerformInstruction();

            memory[5] = LoadCommand.PushAF;
            cpu.PerformInstruction();

            Assert.AreEqual(Bit.Zero, memory[136][EightBitLocation.Four]);
        }

        [TestMethod]
        public void Given_NFlagSet_When_RLCA_Then_NFlagClear()
        {
            var hex_88 = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);
            var hex_FF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            memory[0] = LoadCommand.LD_SP_nn;
            memory[1] = hex_88;
            memory[2] = ZByte.Empty;
            cpu.PerformInstruction();

            memory[136] = hex_FF;
            memory[3] = LoadCommand.PopAF;
            cpu.PerformInstruction();

            memory[4] = RotateCommand.RLCA;
            cpu.PerformInstruction();

            memory[5] = LoadCommand.PushAF;
            cpu.PerformInstruction();

            Assert.AreEqual(Bit.Zero, memory[136][EightBitLocation.One]);
        }
    }
}
