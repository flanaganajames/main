﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using Z80_CPU_Tests.Tests.Classes;
using BitClasses;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.EightBit.Arithmetic
{
    [TestClass]
    public class Inc8_Tests
    {
        private TrueMemorySimulator memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new TrueMemorySimulator();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void Given_ValueOfaHLIs0h_When_aHLIncremented_Then_ValueOfaHLIs01h()
        {
            var expectedValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.INC_aHL);

            Assert.AreEqual(expectedValue, memory.GetValue(SixteenBitAddress.EmptyAddress));
        }

        [TestMethod]
        public void Given_ValueOfaHLIsF6h_When_aHLIncremented_Then_SFlagIsSet()
        {
            var initialValue = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
            memory.SetValue(SixteenBitAddress.EmptyAddress, initialValue);

            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();
            
            cpu.PerformInstruction(EightBitArithmeticCommand.INC_aHL);            
            cpu.PerformInstruction(LoadCommand.PushAF);
                        
            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress))[EightBitLocation.Seven]);
        }

        [TestMethod]
        public void Given_ValueOfaHLIsFFh_When_aHLIncremented_Then_ZFlagIsSet()
        {
            var initialValue = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);
            memory.SetValue(SixteenBitAddress.EmptyAddress, initialValue);

            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.INC_aHL);
            cpu.PerformInstruction(LoadCommand.PushAF);
                        
            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress))[EightBitLocation.Six]);
        }

        [TestMethod]
        public void Given_ValueOfaHLIs0Fh_When_aHLIncremented_Then_HFlagIsSet()
        {
            var initialValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);
            memory.SetValue(SixteenBitAddress.EmptyAddress, initialValue);

            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.INC_aHL);
            cpu.PerformInstruction(LoadCommand.PushAF);
                        
            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress))[EightBitLocation.Four]);
        }

        [TestMethod]
        public void Given_ValueOfaHLIs7Fh_When_aHLIncremented_Then_PVFlagIsSet()
        {
            var initialValue = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);
            memory.SetValue(SixteenBitAddress.EmptyAddress, initialValue);

            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.INC_aHL);
            cpu.PerformInstruction(LoadCommand.PushAF);
                        
            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress))[EightBitLocation.Two]);
        }

        [TestMethod]
        public void When_aHLIncremented_Then_NFlagIsClear()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.INC_aHL);
            cpu.PerformInstruction(LoadCommand.PushAF);
                        
            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress))[EightBitLocation.One]);
        }

        [TestMethod]
        public void Given_ValueOfBIs00h_When_BIncremented_Then_ValueOfBIs01h()
        {
            var expectedValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.INC_B);
            cpu.PerformInstruction(LoadCommand.BIntoMemory);

            Assert.AreEqual(expectedValue, memory.GetValue(SixteenBitAddress.EmptyAddress));
        }

        [TestMethod]
        public void Given_ValueOfaIXdIs0h_When_aHLIncremented_Then_ValueOfaIXdIs01h()
        {
            var expectedValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);
            var displacementValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.INC_aIXd_First, EightBitArithmeticCommand.INC_aIXd_Second, displacementValue);

            Assert.AreEqual(expectedValue, memory.GetValue(new SixteenBitAddress(new ZByte(), displacementValue)));
        }

        [TestMethod]
        public void Given_ValueOfaIYdIs0h_When_aHLIncremented_Then_ValueOfaIYdIs01h()
        {
            var expectedValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);
            var displacementValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.INC_aIYd_First, EightBitArithmeticCommand.INC_aIYd_Second, displacementValue);

            Assert.AreEqual(expectedValue, memory.GetValue(new SixteenBitAddress(new ZByte(), displacementValue)));
        }

    }
}
