﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using Z80_CPU_Tests.Tests.Classes;
using BitClasses;
using BaseClasses;

namespace Z80_CPU_Tests
{
    [TestClass]
    public class Dec8_Tests
    {
        private TrueMemorySimulator memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new TrueMemorySimulator();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void Given_ValueOfaHLIs0h_When_aHLDecremented_Then_ValueOfaHLIsFFh()
        {
            var expectedValue = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_aHL);
            
            Assert.AreEqual(expectedValue, memory.GetValue(SixteenBitAddress.EmptyAddress));
        }

        [TestMethod]
        public void Given_ValueOfaHLIs0h_When_aHLDecremented_Then_SFlagIsSet()
        {   
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_aHL);
            cpu.PerformInstruction(LoadCommand.PushAF);
                        
            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress)[EightBitLocation.Seven]));
        }

        [TestMethod]
        public void Given_ValueOfaHLIs1h_When_aHLDecremented_Then_SFlagIsNotSet()
        {
            memory.SetValue(SixteenBitAddress.EmptyAddress, new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One));

            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_aHL);
            cpu.PerformInstruction(LoadCommand.PushAF);            
                        
            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Seven]));

        }

        [TestMethod]
        public void Given_ValueOfaHLIs0h_When_aHLDecremented_Then_ZFlagIsNotSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_aHL);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Six]));
        }

        [TestMethod]
        public void Given_ValueOfaHLIs1h_When_aHLDecremented_Then_ZFlagIsSet()
        {
            memory.SetValue(SixteenBitAddress.EmptyAddress, new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One));

            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_aHL);
            cpu.PerformInstruction(LoadCommand.PushAF);
                        
            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress)[EightBitLocation.Six]));
        }

        [TestMethod]
        public void Given_ValueOfaHLIs0h_When_aHLDecremented_Then_HFlagIsSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_aHL);
            cpu.PerformInstruction(LoadCommand.PushAF);
                        
            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress)[EightBitLocation.Four]));
        }

        [TestMethod]
        public void Given_ValueOfaHLIs1h_When_aHLDecremented_Then_HFlagIsNotSet()
        {
            memory.SetValue(SixteenBitAddress.EmptyAddress, new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One));

            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_aHL);
            cpu.PerformInstruction(LoadCommand.PushAF);
            
            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Four]));
        }

        [TestMethod]
        public void Given_ValueOfaHLIs80h_When_aHLDecremented_Then_PVFlagIsSet()
        {
            memory.SetValue(SixteenBitAddress.EmptyAddress, new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero));

            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_aHL);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress)[EightBitLocation.Two]));
        }

        [TestMethod]
        public void Given_ValueOfaHLIs79h_When_aHLDecremented_Then_PVFlagIsNotSet()
        {
            memory.SetValue(SixteenBitAddress.EmptyAddress, new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One));

            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_aHL);
            cpu.PerformInstruction(LoadCommand.PushAF);
            
            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Two]));
        }

        [TestMethod]
        public void When_aHLDecremented_Then_NFlagIsSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_aHL);
            cpu.PerformInstruction(LoadCommand.PushAF);
                        
            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress)[EightBitLocation.One]));
        }

        [TestMethod]
        public void Given_ValueOfBIs0h_When_aHLDecremented_Then_ValueOfBIsFFh()
        {            
            var expectedValue = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_B);
            cpu.PerformInstruction(LoadCommand.BIntoMemory);

            Assert.AreEqual(expectedValue, memory.GetValue(SixteenBitAddress.EmptyAddress));
        }

        [TestMethod]
        public void Given_ValueOfaIXdIs0h_When_aHLDecremented_Then_ValueOfaIXdIsFFh()
        {
            var expectedValue = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);
            var displacementValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_aIXd_First, EightBitArithmeticCommand.DEC_aIXd_Second, displacementValue);

            Assert.AreEqual(expectedValue, memory.GetValue(new SixteenBitAddress(new ZByte(), displacementValue)));
        }

        [TestMethod]
        public void Given_ValueOfaIYdIs0h_When_aHLDecremented_Then_ValueOfaIYdIsFFh()
        {
            var expectedValue = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);
            var displacementValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_aIYd_First, EightBitArithmeticCommand.DEC_aIYd_Second, displacementValue);

            Assert.AreEqual(expectedValue, memory.GetValue(new SixteenBitAddress(new ZByte(),  displacementValue)));
        }

    }
}
