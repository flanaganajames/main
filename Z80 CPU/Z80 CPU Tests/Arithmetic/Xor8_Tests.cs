﻿using BitClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using Z80_CPU_Tests.Tests.Classes;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.EightBit.Arithmetic
{
    [TestClass]
    public class Xor8_Tests
    {
        private TrueMemorySimulator memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new TrueMemorySimulator();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void Given_AIsFFh_When_XOR_FFh_Then_AIs00h()
        {
            var hex_FF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);
            
            cpu.PerformInstruction(LoadCommand.LD_A_n, hex_FF);
            cpu.PerformInstruction(EightBitArithmeticCommand.XOR_n, hex_FF);
            cpu.PerformInstruction(LoadCommand.AIntoMemory);

            Assert.AreEqual(ZByte.Empty, memory._internal[0]);
        }

        [TestMethod]
        public void Given_AIs0Fh_When_XOR_F0h_Then_AIsFFh()
        {
            var hex_F0 = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var hex_0F = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);
            var hex_FF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            cpu.PerformInstruction(LoadCommand.LD_A_n, hex_0F);
            cpu.PerformInstruction(EightBitArithmeticCommand.XOR_n, hex_F0);
            cpu.PerformInstruction(LoadCommand.AIntoMemory);

            Assert.AreEqual(hex_FF, memory._internal[0]);
        }

        [TestMethod]
        public void Given_AIs0Fh_When_XOR_F0h_Then_SFlagIsSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();
            var hex_F0 = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var hex_0F = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);
            var hex_FF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            cpu.PerformInstruction(LoadCommand.LD_A_n, hex_0F);
            cpu.PerformInstruction(EightBitArithmeticCommand.XOR_n, hex_F0);            
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress)[EightBitLocation.Seven]));
        }

        [TestMethod]
        public void Given_AIs00h_When_XOR_00h_Then_SFlagIsClear()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();            
                        
            cpu.PerformInstruction(EightBitArithmeticCommand.XOR_n, ZByte.Empty);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Seven]));
        }

        [TestMethod]
        public void Given_AIs00h_When_XOR_00h_Then_ZFlagIsSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.XOR_n, ZByte.Empty);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress)[EightBitLocation.Six]));
        }

        [TestMethod]
        public void Given_AIs0Fh_When_XOR_F0h_Then_ZFlagIsClear()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();
            var hex_F0 = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var hex_0F = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);
            var hex_FF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            cpu.PerformInstruction(LoadCommand.LD_A_n, hex_0F);
            cpu.PerformInstruction(EightBitArithmeticCommand.XOR_n, hex_F0);
            cpu.PerformInstruction(LoadCommand.PushAF);
            
            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Six]));
        }

        [TestMethod]
        public void When_XOR_00h_Then_HFlagIsClear()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.XOR_n, ZByte.Empty);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Four]));
        }

        [TestMethod]
        public void When_XOR_00h_Then_NFlagIsClear()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.XOR_n, ZByte.Empty);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.One]));
        }

        [TestMethod]
        public void When_XOR_00h_Then_CFlagIsClear()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.XOR_n, ZByte.Empty);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Zero]));
        }

        [TestMethod]
        public void Given_AIs0h_When_XOR_00h_Then_ParityFlagIsSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.XOR_n, ZByte.Empty);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress)[EightBitLocation.Two]));
        }

        [TestMethod]
        public void Given_AIs0h_When_XOR_01h_Then_ParityFlagIsClear()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.XOR_n, hex_01);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Two]));
        }

    }
}
