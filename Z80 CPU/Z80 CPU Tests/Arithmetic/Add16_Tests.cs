﻿using BitClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using Z80_CPU_Tests.Tests.Classes;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.SixteenBit.Arithmetic
{
    [TestClass]
    public class Add16_Tests
    {
        private TrueMemorySimulator memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new TrueMemorySimulator();
            cpu = new CPU(memory);
        }
        
        [TestMethod]
        public void Given_HLIs4242hAndDEIs1111h_When_ADD_HL_DE_Then_HLIs5353h()
        {
            var hex_42 = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var hex_11 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);
            var hex_53 = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One);
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            TestHelper.LD_HL_NN(cpu, hex_42, hex_42);
            TestHelper.LD_DE_NN(cpu, hex_11, hex_11);
            cpu.PerformInstruction(SixteenBitArithmeticCommand.ADD_HL_DE);
            cpu.PerformInstruction(LoadCommand.HLIntoaNN, new ZByte(), new ZByte());

            Assert.AreEqual(hex_53, memory.GetValue(SixteenBitAddress.EmptyAddress));
            Assert.AreEqual(hex_53, memory.GetValue(new SixteenBitAddress(new ZByte(), hex_01)));
        }

        [TestMethod]
        public void Given_HLIs00FFhAndDEIs00FFh_When_ADD_HL_DE_Then_HalfCarryFlagIsSet()
        {
            var hex_F = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            TestHelper.LD_HL_NN(cpu, hex_F, ZByte.Empty);
            TestHelper.LD_DE_NN(cpu, hex_F, ZByte.Empty);
            cpu.PerformInstruction(SixteenBitArithmeticCommand.ADD_HL_DE);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, memory.GetValue(expectedStackAddress)[EightBitLocation.Four]);
        }

        [TestMethod]
        public void Given_HLIs00FFhAndDEIs00FFh_When_ADD_HL_DE_Then_NIsClear()
        {
            var hex_F = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            TestHelper.LD_HL_NN(cpu, hex_F, ZByte.Empty);
            TestHelper.LD_DE_NN(cpu, hex_F, ZByte.Empty);            
            cpu.PerformInstruction(SixteenBitArithmeticCommand.ADD_HL_DE);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, memory.GetValue(expectedStackAddress)[EightBitLocation.One]);
        }

        [TestMethod]
        public void Given_HLIsFF00hAndDEIsFF00h_When_ADD_HL_DE_Then_CarryFlagIsSet()
        {
            var hex_FF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            TestHelper.LD_HL_NN(cpu, hex_FF, ZByte.Empty);
            TestHelper.LD_DE_NN(cpu, hex_FF, ZByte.Empty);
            cpu.PerformInstruction(SixteenBitArithmeticCommand.ADD_HL_DE);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, memory.GetValue(expectedStackAddress)[EightBitLocation.Zero]);
        }

        [TestMethod]
        public void Given_HLIs00FFhAndDEIs00FFh_When_ADD_HL_DE_Then_CarryFlagIsClear()
        {
            var hex_FF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            TestHelper.LD_HL_NN(cpu, ZByte.Empty, hex_FF);
            TestHelper.LD_DE_NN(cpu, ZByte.Empty, hex_FF);
            cpu.PerformInstruction(SixteenBitArithmeticCommand.ADD_HL_DE);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, memory.GetValue(expectedStackAddress)[EightBitLocation.Zero]);
        }
    }
}
