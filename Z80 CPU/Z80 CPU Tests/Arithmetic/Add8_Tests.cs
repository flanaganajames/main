﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using Z80_CPU_Tests.Tests.Classes;
using BitClasses;

using BaseClasses;

namespace Z80_CPU_Tests.Tests.EightBit.Arithmetic
{
    [TestClass]
    public class Add8_Tests
    {
        private TrueMemorySimulator memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new TrueMemorySimulator();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void Given_AIs0h_When_LD_A_5h_Then_AIs5h()
        {
            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);            

            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_05h);
            cpu.PerformInstruction(LoadCommand.LD_aNN_A, new ZByte(), new ZByte());

            Assert.AreEqual(hex_05h, memory.GetValue(SixteenBitAddress.EmptyAddress));
        }

        [TestMethod]
        public void Given_AIs5h_When_LD_A_5h_Then_AIs10h()
        {
            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            var hex_0Ah = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_05h);
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_05h);
            cpu.PerformInstruction(LoadCommand.LD_aNN_A, new ZByte(), new ZByte());

            Assert.AreEqual(hex_0Ah, memory.GetValue(SixteenBitAddress.EmptyAddress));
        }


        [TestMethod]
        public void Given_AIsF0h_When_LD_A_5h_Then_SFlagIsSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            var hex_F0h = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_F0h);
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_05h);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress)[EightBitLocation.Seven]));
        }

        [TestMethod]
        public void Given_AIs0h_When_LD_A_5h_Then_SFlagIsNotSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
                                    
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_05h);            
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Seven]));
        }

        [TestMethod]
        public void Given_AIs0h_When_LD_A_0h_Then_ZFlagIsSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, new ZByte());
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, new ZByte());            
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress)[EightBitLocation.Six]));
        }

        [TestMethod]
        public void Given_AIs05h_When_LD_A_0h_Then_ZFlagIsNotSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_05h);
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, new ZByte());            
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Six]));
        }

        [TestMethod]
        public void Given_AIs0Fh_When_LD_A_05h_Then_HFlagIsSet()
        {            
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();
            var hex_F0h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);
            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_F0h);
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_05h);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress)[EightBitLocation.Four]));
        }

        [TestMethod]
        public void Given_AIs00h_When_LD_A_05h_Then_HFlagIsNotSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();
            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, new ZByte());
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_05h);
            cpu.PerformInstruction(LoadCommand.PushAF);
                        
            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Four]));
        }

        [TestMethod]
        public void Given_AIsFFh_When_LD_A_05h_Then_PVFlagIsSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();
            var hex_FFh = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);
            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_FFh);
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_05h);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress)[EightBitLocation.Two]));
        }

        [TestMethod]
        public void Given_AIs00h_When_LD_A_05h_Then_PVFlagIsNotSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();            
            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, new ZByte());
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_05h);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Two]));
        }

        [TestMethod]
        public void When_LD_A_05h_Then_NFlagIsNotSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();         

            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, new ZByte());
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.One]));
        }

        [TestMethod]
        public void Given_NFlagIsSet_When_LD_A_05h_Then_NFlagIsNotSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(EightBitArithmeticCommand.DEC_B);
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, new ZByte());            
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.One] ));
        }

        [TestMethod]
        public void Given_AIsFFh_When_ADD_A_05h_Then_CFlagIsSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();
            var hex_FFh = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);
            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_FFh);
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_05h);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.One, (memory.GetValue(expectedStackAddress)[EightBitLocation.Zero]));
        }

        [TestMethod]
        public void Given_AIs00h_When_ADD_A_05h_Then_CFlagIsNotSet()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();
            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, new ZByte());
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_N, hex_05h);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, (memory.GetValue(expectedStackAddress)[EightBitLocation.Zero]));
        }

        [TestMethod]
        public void Given_AIs00hAndBIS05h_When_LD_A_B_Then_AIs05h()
        {
            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            cpu.PerformInstruction(LoadCommand.NNintoBC, new ZByte(), hex_05h);
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_B);
            cpu.PerformInstruction(LoadCommand.LD_aNN_A, new ZByte(), new ZByte());

            Assert.AreEqual(hex_05h, memory.GetValue(SixteenBitAddress.EmptyAddress));
        }

        [TestMethod]
        public void Given_AIs00hAndaHLIs05h_When_LD_A_aHL_Then_AIs05h()
        {
            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

            memory.SetValue(SixteenBitAddress.EmptyAddress, hex_05h);
            
            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_aHL);
            cpu.PerformInstruction(LoadCommand.LD_aNN_A, new ZByte(), new ZByte());

            Assert.AreEqual(hex_05h, memory.GetValue(SixteenBitAddress.EmptyAddress));
        }

        [TestMethod]
        public void Given_AIs0hAndaIXdIs5h_When_aIXdAdded_Then_AIs05h()
        {
            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            var displacementValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);
            var displacedAddress = new SixteenBitAddress(new ZByte(), displacementValue);
            memory.SetValue(displacedAddress, hex_05h);

            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_aIXd_First, EightBitArithmeticCommand.ADD_A_aIXd_Second, displacementValue);

            Assert.AreEqual(hex_05h, memory.GetValue(displacedAddress));
        }

        [TestMethod]
        public void Given_AIs0hAndaIYdIs5h_When_aIYdAdded_Then_AIs05h()
        {
            var hex_05h = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
            var displacementValue = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);
            var displacedAddress = new SixteenBitAddress(new ZByte(), displacementValue);
            memory.SetValue(displacedAddress, hex_05h);

            cpu.PerformInstruction(EightBitArithmeticCommand.ADD_A_aIYd_First, EightBitArithmeticCommand.ADD_A_aIYd_Second, displacementValue);

            Assert.AreEqual(hex_05h, memory.GetValue(displacedAddress));
        }
    }
}
