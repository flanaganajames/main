﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using BitClasses;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.SixteenBit.Arithmetic
{
    [TestClass]
    public class Increment16_Tests
    {
        private TrueMemorySimulator memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new TrueMemorySimulator();
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void Given_HLIs1000h_When_INC_HL_Then_HLIs1001h()
        {
            var hex_10 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            cpu.PerformInstruction(LoadCommand.NNintoHL, new ZByte(), hex_10);
            cpu.PerformInstruction(SixteenBitArithmeticCommand.Increment);
            cpu.PerformInstruction(LoadCommand.HLIntoaNN, new ZByte(), new ZByte());

            Assert.AreEqual(hex_01, memory.GetValue(SixteenBitAddress.EmptyAddress));
            Assert.AreEqual(hex_10, memory.GetValue(new SixteenBitAddress(new ZByte(), hex_01)));
        }

        [TestMethod]
        public void Given_IXIs1000h_When_INC_IX_Then_IXIs1001h()
        {
            var hex_10 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            cpu.PerformInstruction(LoadCommand.NNintoIX_First, LoadCommand.NNintoIX_Second, new ZByte(), hex_10);
            cpu.PerformInstruction(SixteenBitArithmeticCommand.IX_Register, SixteenBitArithmeticCommand.Increment);
            cpu.PerformInstruction(LoadCommand.IXintoNN_First, LoadCommand.IXintoNN_Second, new ZByte(), new ZByte());

            Assert.AreEqual(hex_01, memory.GetValue(SixteenBitAddress.EmptyAddress));
            Assert.AreEqual(hex_10, memory.GetValue(new SixteenBitAddress(new ZByte(), hex_01)));
        }

        [TestMethod]
        public void Given_IYIs1000h_When_INC_IY_Then_IYIs1001h()
        {
            var hex_10 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            cpu.PerformInstruction(LoadCommand.NNintoIY_First, LoadCommand.NNintoIY_Second, new ZByte(), hex_10);
            cpu.PerformInstruction(SixteenBitArithmeticCommand.IY_Register, SixteenBitArithmeticCommand.Increment);
            cpu.PerformInstruction(LoadCommand.IYintoNN_First, LoadCommand.IYintoNN_Second, new ZByte(), new ZByte());

            Assert.AreEqual(hex_01, memory.GetValue(SixteenBitAddress.EmptyAddress));
            Assert.AreEqual(hex_10, memory.GetValue(new SixteenBitAddress(new ZByte(), hex_01)));
        }


    }
}
