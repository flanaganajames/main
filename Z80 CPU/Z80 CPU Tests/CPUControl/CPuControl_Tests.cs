﻿using BitClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;
using Z80_CPU_Tests.Tests.Classes;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.EightBit.CPUControl
{
    [TestClass]
    public class CPuControl_Tests
    {

        private TrueMemorySimulator memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new TrueMemorySimulator();
            cpu = new CPU(memory);
        }


        [TestMethod]
        public void Given_IFF2IsSet_When_DI_Then_PVFlagIsClear()
        {
            var expectedStackAddress = new SixteenBitAddress(TestHelper.LD_SP_89h_ViaHLWithClear(cpu)).Decrement().Decrement();

            cpu.PerformInstruction(CPUControlCommand.EI);
            cpu.PerformInstruction(CPUControlCommand.DI);
            cpu.PerformInstruction(LoadCommand.LD_A_I_First, LoadCommand.LD_A_I_Second);
            cpu.PerformInstruction(LoadCommand.PushAF);

            Assert.AreEqual(Bit.Zero, memory.GetValue(expectedStackAddress)[EightBitLocation.Two]);
        }


        [TestMethod]
        public void Given_AIsB4_When_CPL_Then_AIs4B()
        {
            var hex_B4 = new ZByte(Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);
            var hex_4B = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One);

            memory[0] = EightBitArithmeticCommand.ADD_A_N;
            memory[1] = hex_B4;
            memory[2] = CPUControlCommand.CPL;
            memory[3] = LoadCommand.AIntoMemory;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(hex_4B, memory[0]);
        }

        [TestMethod]
        public void When_CPL_Then_HFlagSet()
        {
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            memory[0] = LoadCommand.ConstantIntoH;
            memory[1] = hex_01;
            memory[2] = LoadCommand.HLIntoSP;
            memory[3] = CPUControlCommand.CPL;
            memory[4] = LoadCommand.PushAF;
            

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(Bit.One, memory[254][EightBitLocation.Four]);
        }


        [TestMethod]
        public void When_CPL_Then_NFlagSet()
        {
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            memory[0] = LoadCommand.ConstantIntoH;
            memory[1] = hex_01;
            memory[2] = LoadCommand.HLIntoSP;
            memory[3] = CPUControlCommand.CPL;
            memory[4] = LoadCommand.PushAF;


            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(Bit.One, memory[254][EightBitLocation.One]);
        }

        [TestMethod]
        public void Given_CarryFlagIsClear_When_CCF_Then_CarryFlagSetAndHFlagClearAndNIsClear()
        {
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            memory[0] = LoadCommand.ConstantIntoH;
            memory[1] = hex_01;
            memory[2] = LoadCommand.HLIntoSP;
            memory[3] = CPUControlCommand.CCF;
            memory[4] = LoadCommand.PushAF;


            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(Bit.One, memory[254][EightBitLocation.Zero]);
            Assert.AreEqual(Bit.Zero, memory[254][EightBitLocation.Four]);
            Assert.AreEqual(Bit.Zero, memory[254][EightBitLocation.One]);
        }

        [TestMethod]
        public void Given_CarryFlagIsSet_When_CCF_Then_CarryFlagClearAndHFlagSetAndNIsClear()
        {
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            memory[256] = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One);
            memory[0] = LoadCommand.ConstantIntoH;
            memory[1] = hex_01;
            memory[2] = LoadCommand.HLIntoSP;
            memory[3] = LoadCommand.PopAF;
            memory[4] = CPUControlCommand.CCF;
            memory[5] = LoadCommand.PushAF;
                        
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(Bit.Zero, memory[256][EightBitLocation.Zero]);
            Assert.AreEqual(Bit.One, memory[256][EightBitLocation.Four]);
            Assert.AreEqual(Bit.Zero, memory[256][EightBitLocation.One]);
        }


        [TestMethod]
        public void When_SCF_Then_CarryFlagSetAndHFlagClearAndNIsClear()
        {
            var hex_01 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            memory[256] = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero);
            memory[0] = LoadCommand.ConstantIntoH;
            memory[1] = hex_01;
            memory[2] = LoadCommand.HLIntoSP;
            memory[3] = LoadCommand.PopAF;
            memory[4] = CPUControlCommand.SCF;
            memory[5] = LoadCommand.PushAF;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(Bit.One, memory[256][EightBitLocation.Zero]);            
            Assert.AreEqual(Bit.Zero, memory[256][EightBitLocation.One]);
            Assert.AreEqual(Bit.Zero, memory[256][EightBitLocation.Four]);
        }


    }
}



