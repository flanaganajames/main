﻿using BitClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;

namespace Z80_CPU_Tests.SetReset
{
    [TestClass]
    public class Set_Tests
    {
        TrueMemorySimulator memory;
        CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            cpu = new CPU();
            memory = new TrueMemorySimulator();
            cpu.SetMemory(memory);
        }

        [TestMethod]
        public void Given_Bis0h_When_Set_4_B_Then_BIs10h()
        {
            var hex_10 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);

            memory[0] = BitSetResetCommand.Set_b_r_First;
            memory[1] = BitSetResetCommand.Set_4_B_Second;
            memory[2] = LoadCommand.BIntoMemory;

            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(hex_10, memory[0]);
        }

        [TestMethod]
        public void Given_HLis0hAnda0hisCBh_When_Set_2_aHL_Then_a0hIsCFh()
        {
            var hex_CF = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);

            memory[0] = BitSetResetCommand.Set_b_r_First;
            memory[1] = BitSetResetCommand.Set_2_aHL_Second;

            cpu.PerformInstruction();

            Assert.AreEqual(hex_CF, memory[0]);
        }

        [TestMethod]
        public void Given_IXIs0h_When_Set_2_aIX0Fh_Then_a0FhIs04h()
        {
            var hex_04 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);
            var hex_0F = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);

            memory[0] = BitSetResetCommand.Set_b_IXd_First;
            memory[1] = BitSetResetCommand.Set_b_IXd_Second;
            memory[2] = hex_0F;
            memory[3] = BitSetResetCommand.Set_2_IXd_Forth;

            cpu.PerformInstruction();

            Assert.AreEqual(hex_04, memory[15]);
        }


    }
}
