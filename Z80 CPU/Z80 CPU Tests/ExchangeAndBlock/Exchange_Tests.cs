﻿using BitClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using Z80_CPU_Tests.Classes;

namespace Z80_CPU_Tests.ExchangeAndBlock
{
    [TestClass]
    public class Exchange_Tests
    {
        private TrueMemorySimulator memory;
        private CPU cpu;

        [TestInitialize]
        public void Initialise()
        {
            memory = new TrueMemorySimulator(1500);
            cpu = new CPU(memory);
        }

        [TestMethod]
        public void Given_DEIs2822hANDHLIs499Ah_When_EX_DE_HL_Then_DEIs499AhAndHLIs2822h()
        {
            var hex_49 = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One);
            var hex_9A = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var hex_28 = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);
            var hex_22 = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var hex_02 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

            memory[0] = LoadCommand.ConstantIntoH;
            memory[1] = hex_49;
            memory[2] = LoadCommand.ConstantIntoL;
            memory[3] = hex_9A;
            memory[4] = LoadCommand.ConstantIntoD;
            memory[5] = hex_28;
            memory[6] = LoadCommand.ConstantIntoE;
            memory[7] = hex_22;
            memory[8] = ExchangeCommand.EX_DE_HL;
            memory[9] = LoadCommand.DDintoaNN_first;
            memory[10] = LoadCommand.DEintoaNN_second;
            memory[11] = ZByte.Empty;
            memory[12] = ZByte.Empty;
            memory[13] = LoadCommand.HLIntoaNN;
            memory[14] = hex_02;
            memory[15] = ZByte.Empty;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();

            Assert.AreEqual(hex_9A, memory[0]);
            Assert.AreEqual(hex_49, memory[1]);
            Assert.AreEqual(hex_22, memory[2]);
            Assert.AreEqual(hex_28, memory[3]);
        }

        [TestMethod]
        public void Given_HLIs499AhAnda0hIsE3And1hIs49h_When_EX_aSP_HL_Then_a0hIs9AhAnda1hIs49hAndHLIs49E3h()
        {
            var hex_49 = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One);
            var hex_9A = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var hex_26 = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
            var hex_02 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

            memory[0] = LoadCommand.ConstantIntoH;
            memory[1] = hex_49;
            memory[2] = LoadCommand.ConstantIntoL;
            memory[3] = hex_9A;            
            memory[4] = ExchangeCommand.EX_aSP_HL;
            memory[5] = LoadCommand.HLIntoaNN;
            memory[6] = hex_02;
            memory[7] = ZByte.Empty;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();


            Assert.AreEqual(hex_9A, memory[0]);
            Assert.AreEqual(hex_49, memory[1]);
            Assert.AreEqual(hex_26, memory[2]);
            Assert.AreEqual(hex_49, memory[3]); 
        }


        [TestMethod]
        public void Given_IXIs499AhAnda0hIsDDAnd1hIs21h_When_EX_aSP_IX_Then_a0hIs9AhAnda1hIs49hAndIXIs21DDh()
        {
            var hex_49 = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One);
            var hex_9A = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);            
            var hex_02 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

            memory[0] = LoadCommand.NNintoIX_First;
            memory[1] = LoadCommand.NNintoIX_Second;
            memory[2] = hex_9A;
            memory[3] = hex_49;
            memory[4] = ExchangeCommand.EX_aSP_IX_First;
            memory[5] = ExchangeCommand.EX_aSP_IXY_Second;
            memory[6] = LoadCommand.IXintoNN_First;
            memory[7] = LoadCommand.IXintoNN_Second;
            memory[8] = hex_02;
            memory[9] = ZByte.Empty;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();


            Assert.AreEqual(hex_9A, memory[0]);
            Assert.AreEqual(hex_49, memory[1]);
            Assert.AreEqual(LoadCommand.NNintoIX_First, memory[2]);
            Assert.AreEqual(LoadCommand.NNintoIX_Second, memory[3]);
        }

        [TestMethod]
        public void Given_IXIs499AhAnda0hIsFDAnd1hIs21h_When_EX_aSP_IX_Then_a0hIs9AhAnda1hIs49hAndIXIs21FDh()
        {
            var hex_49 = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One);
            var hex_9A = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
            var hex_02 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

            memory[0] = LoadCommand.NNintoIY_First;
            memory[1] = LoadCommand.NNintoIY_Second;
            memory[2] = hex_9A;
            memory[3] = hex_49;
            memory[4] = ExchangeCommand.EX_aSP_IY_First;
            memory[5] = ExchangeCommand.EX_aSP_IXY_Second;
            memory[6] = LoadCommand.IYintoNN_First;
            memory[7] = LoadCommand.IYintoNN_Second;
            memory[8] = hex_02;
            memory[9] = ZByte.Empty;

            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();
            cpu.PerformInstruction();


            Assert.AreEqual(hex_9A, memory[0]);
            Assert.AreEqual(hex_49, memory[1]);
            Assert.AreEqual(LoadCommand.NNintoIY_First, memory[2]);
            Assert.AreEqual(LoadCommand.NNintoIY_Second, memory[3]);
        }
    }
}

