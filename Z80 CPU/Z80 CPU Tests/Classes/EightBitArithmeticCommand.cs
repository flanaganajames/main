﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU_Tests.Classes
{
    class EightBitArithmeticCommand
    {
        //INC (HL)
        public static ZByte INC_aHL = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);

        //INC r
        public static ZByte INC_B = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);

        //DEC (HL)
        public static ZByte DEC_aHL = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

        // DEC B
        public static ZByte DEC_B = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

        // DEC (IX + d)
        public static ZByte DEC_aIXd_First = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte DEC_aIXd_Second = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

        // DEC (IY + d)
        public static ZByte DEC_aIYd_First = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte DEC_aIYd_Second = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

        // DEC (IX + d)
        public static ZByte INC_aIXd_First = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte INC_aIXd_Second = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);

        // DEC (IY + d)
        public static ZByte INC_aIYd_First = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte INC_aIYd_Second = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);

        // ADD A, n
        public static ZByte ADD_A_N = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
        
        // ADD A, r
        public static ZByte ADD_A_B = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);

        // ADD A, (HL)
        public static ZByte ADD_A_aHL = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

        // ADD A, (IX + d)
        public static ZByte ADD_A_aIXd_First = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte ADD_A_aIXd_Second = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

        // ADD A, (IX + d)
        public static ZByte ADD_A_aIYd_First = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte ADD_A_aIYd_Second = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
        
        //XOR n        
        public static ZByte XOR_n = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero);

    }
}
