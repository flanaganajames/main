﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BitClasses;

namespace Z80_CPU_Tests
{
    [TestClass]
    public class ByteTests
    {
        [TestMethod]
        public void ReturnsAllZerosOnStartUp()
        {
            var operand = new ZByte();

            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.Seven]);
            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.Six]);
            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.Five]);
            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.Four]);
            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.Three]);
            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.Two]);
            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.One]);
            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.Zero]);
        }

        [TestMethod]
        public void SettingDefaultValuesToAllOneCorrect()
        {
            var operand = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

            Assert.AreEqual(Bit.One, operand[EightBitLocation.Seven]);
            Assert.AreEqual(Bit.One, operand[EightBitLocation.Six]);
            Assert.AreEqual(Bit.One, operand[EightBitLocation.Five]);
            Assert.AreEqual(Bit.One, operand[EightBitLocation.Four]);
            Assert.AreEqual(Bit.One, operand[EightBitLocation.Three]);
            Assert.AreEqual(Bit.One, operand[EightBitLocation.Two]);
            Assert.AreEqual(Bit.One, operand[EightBitLocation.One]);
            Assert.AreEqual(Bit.One, operand[EightBitLocation.Zero]);
        }

        [TestMethod]
        public void SettingDefaultValuesToHaveOneSetCorrect()
        {
            var operand = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);

            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.Seven]);
            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.Six]);
            Assert.AreEqual(Bit.One, operand[EightBitLocation.Five]);
            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.Four]);
            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.Three]);
            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.Two]);
            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.One]);
            Assert.AreEqual(Bit.Zero, operand[EightBitLocation.Zero]);
        }

        [TestMethod]
        public void IncrementingZByteReturnsCorrectValue()
        {
            var value = new ZByte();
            value.Increment();

            var expected = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

            Assert.AreEqual(expected, value);            
        }

        private ZByte Increment(ZByte value)
        {
            value.Increment();
            return value;
        }

        [TestMethod]
        public void When_ByteIsPassedIntoAMethodAndIncremented_Then_ItShouldNotBeEqualToTheOriginal()
        {
            ZByte target = ZByte.Empty;

            Assert.AreNotEqual(target, Increment(target));
            Assert.AreNotSame(target, Increment(target));
        }

     
    }

}
