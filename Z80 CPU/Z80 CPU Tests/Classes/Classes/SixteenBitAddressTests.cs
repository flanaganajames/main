﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using BitClasses;
using BaseClasses;

namespace Z80_CPU_Tests
{
    [TestClass]
    public class SixteenBitAddressTests
    {
        [TestMethod]
        public void PopulatingAddressWithTwoEmptyBytesCorrect()
        {
            var address = new SixteenBitAddress(new ZByte(), new ZByte());

            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Fifteen]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Fourteen]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Thirteen]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Twelve]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Eleven]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Ten]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Nine]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Eight]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Seven]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Six]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Five]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Four]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Three]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Two]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.One]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Zero]);
        }

        [TestMethod]
        public void PopulatingAddressWithTwoFullBytesCorrect()
        {
            var lowByte = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);
            var highByte = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero);

            var address = new SixteenBitAddress(highByte, lowByte);

            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Fifteen]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Fourteen]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Thirteen]);
            Assert.AreEqual(Bit.One, address[SixteenBitLocation.Twelve]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Eleven]);
            Assert.AreEqual(Bit.One, address[SixteenBitLocation.Ten]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Nine]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Eight]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Seven]);
            Assert.AreEqual(Bit.One, address[SixteenBitLocation.Six]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Five]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Four]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Three]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Two]);
            Assert.AreEqual(Bit.One, address[SixteenBitLocation.One]);
            Assert.AreEqual(Bit.Zero, address[SixteenBitLocation.Zero]);
        }

        private SixteenBitAddress Increment(SixteenBitAddress value)
        {
            value.Increment();
            return value;
        }

        [TestMethod]
        public void When_AddressIsPassedIntoAMethodAndIncremented_Then_ItShouldNotBeEqualToTheOriginal()
        {
            SixteenBitAddress target = SixteenBitAddress.EmptyAddress;

            Assert.AreNotEqual(target, Increment(target));
            Assert.AreNotSame(target, Increment(target));
        }

    }
}
