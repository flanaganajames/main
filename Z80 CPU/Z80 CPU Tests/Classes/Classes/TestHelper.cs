﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using BitClasses;
using Z80_CPU_Tests.Classes;
using BaseClasses;

namespace Z80_CPU_Tests.Tests.Classes
{
    static class TestHelper
    {
        
        public static void LD_HL_NN(CPU cpu, ZByte highOrderByte, ZByte lowOrderByte)
        {
            cpu.PerformInstruction(LoadCommand.NNintoHL, lowOrderByte, highOrderByte);
        }

        public static void LD_BC_NN(CPU cpu, ZByte highOrderByte, ZByte lowOrderByte)
        {
            cpu.PerformInstruction(LoadCommand.NNintoBC, lowOrderByte, highOrderByte);
        }

        public static void LD_DE_NN(CPU cpu, ZByte highOrderByte, ZByte lowOrderByte)
        {
            cpu.PerformInstruction(LoadCommand.NNintoDE, lowOrderByte, highOrderByte);
        }

        public static void LD_SP_NN_ViaHLWithClear(CPU cpu, ZByte highOrderByte, ZByte lowOrderByte)
        {
            cpu.PerformInstruction(LoadCommand.NNintoHL, lowOrderByte, highOrderByte);
            cpu.PerformInstruction(LoadCommand.HLIntoSP);
            cpu.PerformInstruction(LoadCommand.NNintoHL, new ZByte(), new ZByte());
        }

        public static SixteenBitAddress LD_SP_89h_ViaHLWithClear(CPU cpu)
        {
            var lowOrderByte = new ZByte(Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One);

            cpu.PerformInstruction(LoadCommand.NNintoHL, lowOrderByte, new ZByte());
            cpu.PerformInstruction(LoadCommand.HLIntoSP);
            cpu.PerformInstruction(LoadCommand.NNintoHL, new ZByte(), new ZByte());

            return new SixteenBitAddress(new ZByte(), lowOrderByte);
        }

    }
}
