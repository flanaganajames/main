﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z80_CPU_Tests.Classes
{
    static class JumpCommand
    {
        //JP nn
        public static ZByte JP_nn = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

        //JR e
        public static ZByte JR_e = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);

        //JR C, e
        public static ZByte JR_C_e = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);

        //JR NC, e
        public static ZByte JR_NC_e = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);

        //JR Z, e
        public static ZByte JR_Z_e = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero);

        //JR NZ, e
        public static ZByte JR_NZ_e = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);

        //JR (HL)
        public static ZByte JP_aHL = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One);

        //JP (IX)
        public static ZByte JP_aIX = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte JP_aIXY = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One);

        //JP (IY)
        public static ZByte JP_aIY = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
    }
}
