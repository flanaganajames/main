﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU
{
    public static class LoadCommand
    {
        // LD r, r'

        public static ZByte BIntoD = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);

        // LD r, n
        public static ZByte LD_A_n = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero);
        public static ZByte ConstantIntoB = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
        public static ZByte ConstantIntoC = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero);
        public static ZByte ConstantIntoD = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
        public static ZByte ConstantIntoE = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero);
        public static ZByte ConstantIntoH = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
        public static ZByte ConstantIntoL = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero);

        //LD (HL), r        
        public static ZByte AIntoMemory = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One);
        public static ZByte BIntoMemory = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
        public static ZByte CIntoMemory = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);
        public static ZByte DIntoMemory = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

        //LD (HL), n
        public static ZByte LD_aHL_n = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

        //LD r, (HL)
        public static ZByte MemoryIntoC = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero);

        //LD A, (BC)
        public static ZByte BCIntoA = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

        //LD A, (DE)
        public static ZByte DEIntoA = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);        

        //LD A, (nn)
        public static ZByte LD_A_aNN = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

        //LD (BC), A
        public static ZByte AIntoBC = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

        //LD (DE), A
        public static ZByte AIntoDE = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

        //LD (nn), A
        public static ZByte LD_aNN_A = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

        //LD  IX, nn
        public static ZByte NNintoIX_First = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte NNintoIX_Second = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

        //LD (nn), IX
        public static ZByte IXintoNN_First =  new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte IXintoNN_Second = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

        //LD  IX, (nn)
        public static ZByte aNNintoIX_First = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte aNNintoIX_Second = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero);
        
        //LD  IY, nn
        public static ZByte NNintoIY_First = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte NNintoIY_Second = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

        //LD (nn), IY
        public static ZByte IYintoNN_First = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte IYintoNN_Second = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

        //LD  IY, (nn)
        public static ZByte aNNintoIY_First = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte aNNintoIY_Second = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

        //LD  (nn), HL
        public static ZByte HLIntoaNN = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero);

        //LD (nn), dd
        public static ZByte DDintoaNN_first = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte BCintoaNN_second = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One);
        public static ZByte DEintoaNN_second = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One);
        public static ZByte SPintoaNN_second = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

        //LD dd, nn
        public static ZByte NNintoBC = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);
        public static ZByte NNintoDE = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);
        public static ZByte NNintoHL = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);
        public static ZByte LD_SP_nn = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

        //LD  HL, (nn)
        public static ZByte aNNintoHL = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero);

        //LD  dd, (nn)
        public static ZByte LD_dd_aNN_First = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte aNNintoBC_second = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One);
        public static ZByte aNNintoHL_second = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One);
        public static ZByte LD_sp_aNN_Second = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One);
        

        //LD  (IX+d), n
        public static ZByte ConstantIntoIXd_first = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte ConstantIntoIXd_second = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

        //LD  (IY+d), n
        public static ZByte ConstantIntoIYd_first = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte ConstantIntoIYd_second = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

        //LD  (IY+d), r
        public static ZByte RIntoIYd_first = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte BIntoIYd_second = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);

        //LD  (IX+d), r
        public static ZByte RIntoIXd_first = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte BIntoIXd_second = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);

        //LD  r, (IY+d)
        public static ZByte IYdIntoR_first = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte IYdIntoB_second = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

        //LD  r, (IX+d)
        public static ZByte IXdIntoR_first = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte IXdIntoB_second = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

        // LD SP, HL
        public static ZByte HLIntoSP = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One);

        // LD SP, IX
        public static ZByte IXIntoSP_First = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte IXIntoSP_Second = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One);

        // LD SP, IY
        public static ZByte IYIntoSP_First = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte IYIntoSP_Second = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One);

        //PUSH IX
        public static ZByte PushIX_First = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte PushIX_Second = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

        //PUSH IY
        public static ZByte PushIY_First = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte PushIY_Second = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

        //PUSH qq
        public static ZByte PushBC = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
        public static ZByte PushHL = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One);
        public static ZByte PushAF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One);

        //POP qq
        public static ZByte PopBC = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);
        public static ZByte PopAF = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

        //POP IX
        public static ZByte PopIX_First = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte PopIX_Second = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

        //POP IX
        public static ZByte PopIY_First = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte PopIY_Second = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One);

        //LD A, I
        public static ZByte LD_A_I_First = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte LD_A_I_Second = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One);

        //LD I, A
        public static ZByte LD_I_A_First = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte LD_I_A_Second = new ZByte(Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One);
    }
}

