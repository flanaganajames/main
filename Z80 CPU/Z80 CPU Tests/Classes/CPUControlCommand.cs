﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z80_CPU_Tests.Classes
{
    class CPUControlCommand
    {
        public static ZByte EI = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One);
        public static ZByte DI = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One);
        public static ZByte CPL = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One);
        public static ZByte CCF = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);
        public static ZByte SCF = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One);
    }
}
