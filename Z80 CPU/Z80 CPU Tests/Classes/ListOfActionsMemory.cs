﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;
using Z80_CPU;
using BaseClasses;


namespace Z80_CPU_Tests.Classes
{
    class ListOfActionsMemory : IMemory
    {
       public List<Tuple<SixteenBitAddress, ZByte>> Saved = new List<Tuple<SixteenBitAddress, ZByte>>();
       public Dictionary<SixteenBitAddress, ZByte> ReadValues = new Dictionary<SixteenBitAddress, ZByte>();

        public ZByte GetValue(SixteenBitAddress address)
        {
            ZByte output;
            if (ReadValues.TryGetValue(address, out output))
            {
                return output;
            }
            else
            {
                return new ZByte();
            }                    

        }

        public void SetValue(SixteenBitAddress address, ZByte value)
        {
            Saved.Add(new Tuple<SixteenBitAddress, ZByte>(address, value));
        }
    }
}
