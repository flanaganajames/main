﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z80_CPU_Tests.Classes
{
    static class BitSetResetCommand
    {

       public static ZByte Set_b_r_First = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One);
       public static ZByte Set_4_B_Second = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);


       public static ZByte Set_2_aHL_Second = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero);
        
       public static ZByte Set_b_IXd_First = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
       public static ZByte Set_b_IXd_Second = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One);
       public static ZByte Set_2_IXd_Forth = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.Zero);

    }
}
