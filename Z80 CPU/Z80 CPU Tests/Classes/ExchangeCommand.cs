﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z80_CPU_Tests.Classes
{
    static class ExchangeCommand
    {
        public static ZByte EX_DE_HL = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One);
        public static ZByte EX_aSP_HL = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

        public static ZByte EX_aSP_IX_First = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte EX_aSP_IY_First = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte EX_aSP_IXY_Second = new ZByte(Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

        
    }
}
