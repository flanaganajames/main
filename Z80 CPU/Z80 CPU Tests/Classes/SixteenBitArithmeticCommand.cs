﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using BitClasses;

namespace Z80_CPU_Tests.Classes
{
    class SixteenBitArithmeticCommand
    {
        //ConsistenRegisters
        public static ZByte IX_Register = new ZByte(Bit.One, Bit.One, Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        public static ZByte IY_Register = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.One);
        

        //INC ss, IX, IY
        public static ZByte Increment = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One);
        public static ZByte Increment_SP = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One, Bit.One);

        //DEC SS
        public static ZByte Decrement = new ZByte(Bit.Zero, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.Zero, Bit.One, Bit.One);

        //ADD HL, ss
        public static ZByte ADD_HL_DE = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.One);
        

    }
}
