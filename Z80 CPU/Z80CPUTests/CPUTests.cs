﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Z80_CPU;
using BitClasses;
using Z80CPUTests.Instructions;

namespace Z80CPUTests
{
    [TestClass]
    public class CPUTests
    {
        private MemorySimulator memory;
        private CPU cpu;


        [TestInitialize]
        public void Setup()
        {
            memory = new MemorySimulator(1000);
            cpu = new CPU();
            cpu.SetMemory(memory);
        }


        [TestMethod]
        public void When_HalfTick_Then_NoErrorOccurs()
        {
            cpu.HalfTick();
        }

        [TestMethod]
        public void When_Tick_Then_NoErrorOccurs()
        {
            cpu.Tick();
        }

        public void Given_HLis0000h_When_LD_aHL_05h_Then_a0000hContains05h()
        {
            var hex_05 = new ZByte(Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);
            memory[0] = LoadInstructions.LD_aHL_n;
            memory[1] = hex_05;

            cpu.HalfTick();

        }
    }
}
