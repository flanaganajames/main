﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;
using Z80_CPU;
using System.IO;
using BaseClasses;

namespace ZXSpectrum
{
    class ROMMemory : IMemory
    {
        ZByte[] _data = new ZByte[16384];

        public ROMMemory()
        {
            int current = 0;

            using (var file = new FileStream("Resources/48k.rom", FileMode.Open))
            {
                int value = file.ReadByte();

                while (value != -1)
                {
                    _data[current] = new ZByte((byte) value);

                    value = file.ReadByte();
                    current++;
                }
            }
        }

        public ZByte GetValue(SixteenBitAddress address)
        {
            return _data[Conversion.DecodeAddress(address)];
        }

        public void SetValue(SixteenBitAddress address, ZByte value)
        {
            //ROM - So Writing does nothing
        }

        public int Size()
        {
            return _data.Length;
        }
    }
}
