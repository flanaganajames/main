﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;

namespace ZXSpectrum
{
    class Program
    {
        static void Main(string[] args)
        {
            ZXSpectrumFullMemory memory = new ZXSpectrumFullMemory();
            CPU cpu = new CPU(memory);

            int count = 0;
                    
            while (true)
            {
                Console.WriteLine(count++);
                cpu.PerformInstruction();
            }
        }
    }
}
