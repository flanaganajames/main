﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU;
using BaseClasses;

namespace ZXSpectrum
{
    static class Conversion
    {
        public static int DecodeAddress(SixteenBitAddress value)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append((int)value[SixteenBitLocation.Fifteen]);
            builder.Append((int)value[SixteenBitLocation.Fourteen]);
            builder.Append((int)value[SixteenBitLocation.Thirteen]);
            builder.Append((int)value[SixteenBitLocation.Twelve]);
            builder.Append((int)value[SixteenBitLocation.Eleven]);
            builder.Append((int)value[SixteenBitLocation.Ten]);
            builder.Append((int)value[SixteenBitLocation.Nine]);
            builder.Append((int)value[SixteenBitLocation.Eight]);
            builder.Append((int)value[SixteenBitLocation.Seven]);
            builder.Append((int)value[SixteenBitLocation.Six]);
            builder.Append((int)value[SixteenBitLocation.Five]);
            builder.Append((int)value[SixteenBitLocation.Four]);
            builder.Append((int)value[SixteenBitLocation.Three]);
            builder.Append((int)value[SixteenBitLocation.Two]);
            builder.Append((int)value[SixteenBitLocation.One]);
            builder.Append((int)value[SixteenBitLocation.Zero]);

            return Convert.ToInt16(builder.ToString(), 2);
        }        
    }
}
