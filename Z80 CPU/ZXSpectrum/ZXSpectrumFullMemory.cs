﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;
using Z80_CPU;
using BaseClasses;

namespace ZXSpectrum
{
    class ZXSpectrumFullMemory : IMemory
    {
        private ROMMemory _rom;
        private RAMMemory _ram;
        
        public ZXSpectrumFullMemory()
        {
            _rom = new ROMMemory();
            _ram = new RAMMemory(49152);
        }

        public ZByte GetValue(SixteenBitAddress address)
        {
            return memoryToUse(address).GetValue(address);
        }

        public void SetValue(SixteenBitAddress address, ZByte value)
        {
            memoryToUse(address).SetValue(address, value);
        }

        private IMemory memoryToUse(SixteenBitAddress address)
        {
            if (Conversion.DecodeAddress(address) <= _rom.Size())
            {
                return _rom;
            }
            else
            {
                return _ram;
            }
        }
    }
}
