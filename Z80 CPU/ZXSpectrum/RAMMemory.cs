﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;
using Z80_CPU;
using BaseClasses;

namespace ZXSpectrum
{
    class RAMMemory : IMemory
    {
        ZByte[] _data;

        public RAMMemory(int bytesAvailable)
        {
            _data = new ZByte[bytesAvailable];
        }

        public ZByte GetValue(SixteenBitAddress address)
        {
            return _data[Conversion.DecodeAddress(address)];
        }

        public void SetValue(SixteenBitAddress address, ZByte value)
        {
            _data[Conversion.DecodeAddress(address)] = value;
        }        
    }
}
