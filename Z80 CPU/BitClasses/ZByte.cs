﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitClasses
{
    public struct ZByte
    {
        public static ZByte Empty = new ZByte();        

        private byte _internalByte;

        public ZByte(byte value)
        {
            _internalByte = value;
        }        

        public ZByte(Bit seven, Bit six, Bit five, Bit four, Bit three, Bit two, Bit one, Bit zero)           
        {
            _internalByte = (byte) (((byte)seven << 7) + ((byte)six << 6) + ((byte)five << 5)
                          + ((byte)four << 4) + ((byte)three << 3) + ((byte)two << 2)
                          + ((byte)one << 1) + ((byte)zero));
        }

        public ZByte(ZNibble highOrderNibble, ZNibble lowOrderNibble)
        {
            _internalByte = (byte) ((highOrderNibble.AsByte() << 4) + lowOrderNibble.AsByte());
        }

        public Bit this[EightBitLocation location]
        {
            get
            {
                byte mask = (byte)(1 << (7 - (int)location));

                return (_internalByte & mask) == 0 ? Bit.Zero : Bit.One;
            }
            set
            {
                byte mask = (byte)(1 << (7 - (int)location));

                if (value == Bit.One)
                {
                    _internalByte |= mask;
                }
                else
                {
                    _internalByte &= (byte)~mask;
                }
            }
        }

        public ZNibble HighNibble
        {
            get
            {
                return ZNibble.GenerateHighOrderNibble(_internalByte);
            }
        }

        public ZNibble LowNibble
        {
            get
            {
                return ZNibble.GenerateLowOrderNibble(_internalByte);
            }
        }

        public void Increment()
        {
            _internalByte += 1;
        }

        public void Decrement()
        {
            _internalByte -= 1;
        }

        public ZByte Add(ZByte rhs)
        {
            return new ZByte((byte)(_internalByte + rhs._internalByte));
        }

        public ZByte Subtract(ZByte rhs)
        {
            return new ZByte((byte)(_internalByte - rhs._internalByte));
        }

        public ZByte Xor(ZByte rhs)
        {
            return new ZByte((byte)(_internalByte ^ rhs._internalByte));
        }

        public override bool Equals(object obj)
        {
            return obj is ZByte && this == (ZByte) obj;
        }

        public override int GetHashCode()
        {
            return _internalByte.GetHashCode();
        }

        public override string ToString()
        {
            return _internalByte.ToString("X2");
        }

        //public static explicit operator sbyte(ZByte value)
        //{
        //    return (sbyte)value._internalByte;
        //}

        public static bool operator == (ZByte lhs, ZByte rhs)
        {
            return lhs._internalByte == rhs._internalByte;
        }

        public static bool operator !=(ZByte lhs, ZByte rhs)
        {
            return lhs._internalByte != rhs._internalByte;
        }

        public static ZWord operator +(ZByte lhs, ZByte rhs)
        {
            return new ZWord((Int16) (lhs._internalByte + rhs._internalByte));
        }

        public static ZWord operator -(ZByte lhs, ZByte rhs)
        {
            return new ZWord((Int16)(lhs._internalByte - rhs._internalByte));
        }

        public static bool operator > (ZByte lhs, ZByte rhs)
        {
            return lhs._internalByte > rhs._internalByte;
        }

        public static bool operator <(ZByte lhs, ZByte rhs)
        {
            return lhs._internalByte < rhs._internalByte;
        }

        internal byte ToByte()
        {
            return _internalByte;
        }

        internal sbyte ToSignedByte()
        {
            return (sbyte) _internalByte;
        }

        public bool IsParityEven()
        {
            var sumOfOnes =
                (this[EightBitLocation.Seven] == Bit.One ? 1 : 0) +
                (this[EightBitLocation.Six] == Bit.One ? 1 : 0) +
                (this[EightBitLocation.Five] == Bit.One ? 1 : 0) +
                (this[EightBitLocation.Four] == Bit.One ? 1 : 0) +
                (this[EightBitLocation.Three] == Bit.One ? 1 : 0) +
                (this[EightBitLocation.Two] == Bit.One ? 1 : 0) +
                (this[EightBitLocation.One] == Bit.One ? 1 : 0) +
                (this[EightBitLocation.Zero] == Bit.One ? 1 : 0);

            return (sumOfOnes % 2) == 0;
        }
    }
}

