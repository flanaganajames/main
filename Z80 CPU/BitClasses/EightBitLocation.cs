﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitClasses
{
    public enum EightBitLocation : byte
    {
        Seven = 0,
        Six = 1,
        Five = 2,
        Four = 3,
        Three = 4,
        Two = 5,
        One = 6,
        Zero = 7
    }
}
