﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitClasses
{
    public enum ThirtyTwoBitLocation
    {
        Zero = 31,
        One = 30,
        Two = 29,
        Three = 28,
        Four = 27,
        Five = 26,
        Six = 25,
        Seven = 24,
        Eight = 23,
        Nine = 22,
        Ten = 21,
        Eleven = 20,
        Twelve = 19,
        Thirteen = 18,
        Fourteen = 17,
        Fifteen = 16,
        Sixteen = 15,
        Seventeen = 14,
        Eightteen = 13,
        Nineteen = 12,
        Twenty = 11,
        TwentyOne = 10,
        TwentyTwo = 9,
        TwentyThree = 8,
        TwentyFour = 7,
        TwentyFive = 6,
        TwentySix = 5,
        TwentySeven = 4,
        TwentyEight = 3,
        TwentyNine = 2,
        Thirty = 1,
        ThirtyOne = 0,
    }
}
