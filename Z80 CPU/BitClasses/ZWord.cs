﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitClasses
{
    public struct ZWord
    {
        private Int16 _internalValue;

        public static ZWord Empty = new ZWord(0);
        
        public ZWord(ZByte highOrderByte, ZByte lowOrderByte)
        {
            _internalValue = (Int16)((((Int16)highOrderByte.ToByte()) << 8) + (lowOrderByte.ToByte()));
        }

        internal ZWord(Int16 value)
        {
            _internalValue = value;
        }

        internal ZWord(ZWord value)
        {
            _internalValue = value._internalValue;
        }

        public Bit this[SixteenBitLocation location]
        {
            get
            {
                Int16 mask = (Int16)(1 << (15 - (int)location));

                return (_internalValue & mask) == 0 ? Bit.Zero : Bit.One;
            }
            set
            {
                Int16 mask = (Int16)(1 << (15 - (int)location));

                if (value == Bit.One)
                {
                    _internalValue |= mask;
                }
                else
                {
                    _internalValue &= (Int16)~mask;
                }
            }
        }

        public ZByte HighOrderByte {
            get
            {
                return new ZByte((byte)(_internalValue >> 8));
            }

            set
            {
                _internalValue = (Int16)((_internalValue & 0x00FF) + (value.ToByte() << 8));
            }
        }

        public ZByte LowOrderByte
        {
            get
            {
                return new ZByte((byte)(_internalValue & 0x00FF));
            }
            set
            {
                _internalValue = (Int16)((_internalValue & 0xFF00) + (value.ToByte()));
            }
        }

        public ZWord IncreaseBy(int value)
        {
            _internalValue += (Int16)value;
            return new ZWord(this);
        }

        public ZWord Increment()
        {
            _internalValue += 1;
            return new ZWord(this);
        }

        public ZWord Decrement()
        {
            _internalValue -= 1;
            return new ZWord(this);
        }

        public void AddSignedByte(ZByte value)
        {
            _internalValue += value.ToSignedByte();
        }

        public override int GetHashCode()
        {
            return _internalValue.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is ZWord && this == ((ZWord)obj);
        }

        public override string ToString()
        {
            return _internalValue.ToString("X4");
        }

        public static bool operator ==(ZWord lhs, ZWord rhs)
        {
            return lhs._internalValue == rhs._internalValue;
        }

        public static bool operator !=(ZWord lhs, ZWord rhs)
        {
            return lhs._internalValue != rhs._internalValue;
        }

        public static ZDoubleWord operator +(ZWord lhs, ZWord rhs)
        {
            return new ZDoubleWord(lhs._internalValue + rhs._internalValue);
        }

        public static ZDoubleWord operator -(ZWord lhs, ZWord rhs)
        {
            return new ZDoubleWord(lhs._internalValue - rhs._internalValue);
        }
    }
}
