﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitClasses
{
    public enum SixteenBitLocation : int    
    {
        Zero = 15,
        One = 14,
        Two = 13,
        Three = 12,
        Four = 11,
        Five = 10,
        Six = 9,
        Seven = 8,
        Eight = 7,
        Nine = 6,
        Ten = 5,
        Eleven = 4,
        Twelve = 3,
        Thirteen = 2,
        Fourteen = 1,
        Fifteen = 0
    }
}

