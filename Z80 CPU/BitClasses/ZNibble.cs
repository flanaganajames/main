﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitClasses
{
    public struct ZNibble
    {
        //Only use the 4 least significant bytes
        private byte _internalByte;

        public static readonly ZNibble Empty = new ZNibble(0);

        private ZNibble(byte value)
        {
            _internalByte = value;
        }

        internal static ZNibble GenerateLowOrderNibble(byte value)
        {
            return new ZNibble((byte)(value & 0x0F));
        }

        internal static ZNibble GenerateHighOrderNibble(byte value)
        {
            return new ZNibble((byte)(value >> 4));
        }

        public static ZByte operator +(ZNibble lhs, ZNibble rhs)
        {
            return new ZByte((byte) (lhs._internalByte + rhs._internalByte));
        }

        public static ZByte operator -(ZNibble lhs, ZNibble rhs)
        {
            return new ZByte((byte)(lhs._internalByte - rhs._internalByte));
        }

        internal byte AsByte()
        {
            return _internalByte;
        }
    }
}
