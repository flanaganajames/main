﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitClasses
{
    public enum Bit : byte 
    {        
        Zero = 0,
        One = 1
    }
}
