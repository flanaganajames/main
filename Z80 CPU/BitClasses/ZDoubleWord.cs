﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitClasses
{
    public struct ZDoubleWord
    {
        private Int32 _internalValue;

        internal ZDoubleWord(Int32 value)
        {
            _internalValue = value;
        }

        public Bit this[ThirtyTwoBitLocation location]
        {
            get
            {
                Int32 mask = (Int32)(1 << (31 - (int)location));

                return (_internalValue & mask) == 0 ? Bit.Zero : Bit.One;
            }
            set
            {
                Int32 mask = (Int32)(1 << (31 - (int)location));

                if (value == Bit.One)
                {
                    _internalValue |= mask;
                }
                else
                {
                    _internalValue &= (Int32)~mask;
                }
            }
        }

        public ZWord HighOrderWord { get
            {
                return new ZWord((Int16)(_internalValue >> 16));
            }
        }

        public ZWord LowOrderWord { get
            {
                return new ZWord((Int16)(_internalValue & 0x0000FFFF));
            }
        }

        public ZDoubleWord DecreaseBy(int value)
        {
            return new ZDoubleWord(_internalValue - value);
        }

        public static explicit operator ZWord(ZDoubleWord obj)
        {
            return new ZWord((Int16) obj._internalValue);
        }



    }
}
