﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace BaseClasses
{
    public struct SixteenBitAddress
    {
        private ZWord _internalValue;

        public static SixteenBitAddress EmptyAddress
        {
            get {
                return new SixteenBitAddress(new ZByte(), new ZByte());
            }
        }

        public SixteenBitAddress(SixteenBitAddress value)
        {
            _internalValue = value._internalValue;
        }

        public SixteenBitAddress(ZByte highByte, ZByte lowByte)
        {
            _internalValue = new ZWord(highByte, lowByte);
        }

        public SixteenBitAddress(ZWord value)
        {
            _internalValue = value;
        }        

        public Bit this[SixteenBitLocation location]
        {
            get
            {
                return _internalValue[location];                
            }
            set
            {
                _internalValue[location] = value;
            }
        }

        public SixteenBitAddress Increment()
        {
            _internalValue.Increment();
            return this;
        }

        public SixteenBitAddress Decrement()
        {
            _internalValue.Decrement();
            return this;
        }

        public void AddSignedByte(ZByte value)
        {
            _internalValue.AddSignedByte(value);
        }

        public override bool Equals(object obj)
        {
            return obj is SixteenBitAddress && this == (SixteenBitAddress)obj;
        }

        public override int GetHashCode()
        {
            return _internalValue.GetHashCode();
        }

        public override string ToString()
        {
            return _internalValue.ToString();
        }

        //public static explicit operator UInt16(SixteenBitAddress address)
        //{
        //    return address._internalValue;
        //}

        public static bool operator ==(SixteenBitAddress lhs, SixteenBitAddress rhs)
        {
            return lhs._internalValue == rhs._internalValue;
        }

        public static bool operator !=(SixteenBitAddress lhs, SixteenBitAddress rhs)
        {
            return lhs._internalValue != rhs._internalValue;
        }
    }
}
