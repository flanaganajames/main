﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace BaseClasses
{
    public interface IMemory
    {
        void SetValue(SixteenBitAddress address, ZByte value);

        ZByte GetValue(SixteenBitAddress address);
    }
}
