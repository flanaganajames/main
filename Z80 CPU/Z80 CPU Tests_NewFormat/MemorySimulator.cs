﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;
using Z80_CPU;

namespace Z80_CPU_Tests_NewFormat
{
    class MemorySimulator : IMemory
    {
        public ZByte[] _internal;

        public MemorySimulator() : this(1000) { }

        public MemorySimulator(int size)
        {
            _internal = new ZByte[size];

            for (int i = 0; i < size; i++)
            {
                _internal[i] = new ZByte();
            }
        }

        public ZByte GetValue(SixteenBitAddress address)
        {
            return _internal[DecodeValue(address)];
        }

        public void SetValue(SixteenBitAddress address, ZByte value)
        {
            _internal[DecodeValue(address)] = value;
        }

        private int DecodeValue(SixteenBitAddress value)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append((int)value[SixteenBitLocation.Fifteen]);
            builder.Append((int)value[SixteenBitLocation.Fourteen]);
            builder.Append((int)value[SixteenBitLocation.Thirteen]);
            builder.Append((int)value[SixteenBitLocation.Twelve]);
            builder.Append((int)value[SixteenBitLocation.Eleven]);
            builder.Append((int)value[SixteenBitLocation.Ten]);
            builder.Append((int)value[SixteenBitLocation.Nine]);
            builder.Append((int)value[SixteenBitLocation.Eight]);
            builder.Append((int)value[SixteenBitLocation.Seven]);
            builder.Append((int)value[SixteenBitLocation.Six]);
            builder.Append((int)value[SixteenBitLocation.Five]);
            builder.Append((int)value[SixteenBitLocation.Four]);
            builder.Append((int)value[SixteenBitLocation.Three]);
            builder.Append((int)value[SixteenBitLocation.Two]);
            builder.Append((int)value[SixteenBitLocation.One]);
            builder.Append((int)value[SixteenBitLocation.Zero]);

            return Convert.ToInt16(builder.ToString(), 2);
        }

        public ZByte this[int index]
        {
            get
            {
                return _internal[index];
            }
            set {
                _internal[index] = value;
            }
        }
    }
}
