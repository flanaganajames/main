﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class In_A_Command : ICPUOperation
    {
        public void HandleOperation(OperationData data)
        {
            data.CPU.SetAddressBus_LowByte(data.Second);
            data.CPU.SetAddressBus_HighByte(data.CPU.GetEightBitRegister(EightBitRegisterNames.Accumulator).ToByte());
            
            var peripheral = data.CPU.GetPeripheral();

            var value = peripheral.DataOut();

            data.CPU.GetEightBitRegister(EightBitRegisterNames.Accumulator).FromByte(value);
        }
    }
}
