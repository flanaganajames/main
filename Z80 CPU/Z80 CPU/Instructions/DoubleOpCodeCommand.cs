﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Command
{
    class DoubleOpCodeCommand : ICPUOperation_DoubleByteOpcode
    {
        private ICPUOperation _operation;

        public DoubleOpCodeCommand(ZByte firstOpCode, ZByte secondOpCode, ICPUOperation operation, int numberOfBytes) : this(firstOpCode, new List<ZByte>() { secondOpCode}, operation, numberOfBytes)
        {
        }

        public DoubleOpCodeCommand(ZByte firstOpCode, List<ZByte> secondOpCodes, ICPUOperation operation, int numberOfBytes)
        {
            FirstOpCode = firstOpCode;
            HandledSecondOpCodes = secondOpCodes;
            _operation = operation;
            NumberOfBytes = numberOfBytes;
        }

        public int NumberOfBytes
        {
            get;
            private set;
        }

        public void HandleOperation(OperationData data)
        {
            _operation.HandleOperation(data);
        }

        public ZByte FirstOpCode
        {
            get;
            private set;
        }

        public List<ZByte> HandledSecondOpCodes
        {
            get;
            private set;
        }



    }
}
