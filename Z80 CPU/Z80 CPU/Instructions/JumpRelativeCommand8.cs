﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Command.Jump
{
    class JumpRelativeCommand8 : ICPUOperation
    {
        private ISource8 _src;

        public JumpRelativeCommand8(ISource8 src)
        {
            _src = src;
        }

        public virtual void HandleOperation(OperationData data)
        {
            var programCounter = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter);
            var programCounterValue = programCounter.Value;
            var value = _src.Value(data);
            value.Decrement();
            value.Decrement();

            programCounterValue.AddSignedByte(value);
            
            programCounter.Value = programCounterValue;
            
        }
    }
}
