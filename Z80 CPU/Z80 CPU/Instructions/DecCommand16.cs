﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Command.Arithmetic
{
    class DecCommand16 : ICPUOperation
    {
        private ISourceDestination16 _srcDest;

        public DecCommand16(ISourceDestination16 srcDest)
        {
            _srcDest = srcDest;
        }

        public void HandleOperation(OperationData data)
        {
            ZWord originalValue = _srcDest.Value(data);
            originalValue.Decrement();

            _srcDest.SetValue(data, originalValue.HighOrderByte, originalValue.LowOrderByte);
        }
    }
}
