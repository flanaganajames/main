﻿using BaseClasses;
using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class LDICommand : ICPUOperation
    {
        public void HandleOperation(OperationData data)
        {
            var valueOfHLMemory = data.CPU.Memory.GetValue(new SixteenBitAddress(data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL).Value));
            data.CPU.Memory.SetValue(new SixteenBitAddress(data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.DE).Value), valueOfHLMemory);

            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.DE).Increment();
            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL).Increment();
            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.BC).Decrement();

            data.CPU.ClearFlag(FlagNames.HalfCarry);
            data.CPU.SetFlag(FlagNames.ParityOverflow, data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.BC).Value == ZWord.Empty);
            data.CPU.ClearFlag(FlagNames.AddSubtract);
        }
    }
}
