﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Command.Load
{
    class LoadCommand8 : ICPUOperation
    {
        protected IDestination8 _dest;
        protected ISource8 _src;

        public LoadCommand8(IDestination8 dest, ISource8 src)
        {
            _dest = dest;
            _src = src;
        }

        public virtual void HandleOperation(OperationData data)
        {
            _dest.SetValue(data, _src.Value(data));
        }        
    }
}
