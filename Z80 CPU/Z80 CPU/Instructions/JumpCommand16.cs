﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Command.Jump
{
    class JumpCommand16 : ICPUOperation
    {
        private ISource16 _src;

        public JumpCommand16(ISource16 src)
        {
            _src = src;
        }

        public virtual void HandleOperation(OperationData data)
        {
            var programCounter = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter);

            programCounter.Value = _src.Value(data);
        }
    }
}
