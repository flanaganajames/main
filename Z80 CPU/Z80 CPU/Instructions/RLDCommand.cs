﻿using BaseClasses;
using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class RLDCommand : ICPUOperation
    {
        public void HandleOperation(OperationData data)
        {
            var accumulator = data.CPU.GetEightBitRegister(EightBitRegisterNames.Accumulator);
            var accumulatorStartValue = accumulator.ToByte();

            var HLAddress = new SixteenBitAddress(data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL).Value);
            var HLStartValue = data.CPU.Memory.GetValue(HLAddress);


            var newAccumulatorValue = new ZByte(accumulatorStartValue.HighNibble, HLStartValue.HighNibble);
            var newHLValue = new ZByte(HLStartValue.LowNibble, accumulatorStartValue.LowNibble);

            accumulator.FromByte(newAccumulatorValue);
            data.CPU.Memory.SetValue(HLAddress, newHLValue);
            
            data.CPU.SetFlag(FlagNames.Sign, newAccumulatorValue[EightBitLocation.Seven] == Bit.One);
            data.CPU.SetFlag(FlagNames.Zero, newAccumulatorValue == ZByte.Empty);
            data.CPU.ClearFlag(FlagNames.HalfCarry);
            data.CPU.SetFlag(FlagNames.ParityOverflow, newAccumulatorValue.IsParityEven());
            data.CPU.ClearFlag(FlagNames.AddSubtract);            
        }
    }
}
