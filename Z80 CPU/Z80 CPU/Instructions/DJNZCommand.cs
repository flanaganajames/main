﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using Z80_CPU.Command.Addressing;
using Z80_CPU.Command.Jump;

namespace Z80_CPU.Instructions
{
    class DJNZCommand : JumpRelativeCommand8
    {
        private ISource8 _src;

        public DJNZCommand(ISource8 src) : base(src)
        {
            _src = src;
        }

        public override void HandleOperation(OperationData data)
        {
            var registerB = data.CPU.GetEightBitRegister(EightBitRegisterNames.B);
            var registerBValue = registerB.ToByte();
            registerBValue.Decrement();
            registerB.FromByte(registerBValue);

            if (registerBValue != ZByte.Empty)            
            {
                base.HandleOperation(data);
            }
        }
    }
}
