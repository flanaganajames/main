﻿using BaseClasses;
using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class INICommand : ICPUOperation
    {
        public void HandleOperation(OperationData data)
        {
            data.CPU.SetAddressBus_LowByte(data.CPU.GetEightBitRegister(EightBitRegisterNames.C).ToByte());
            data.CPU.SetAddressBus_HighByte(data.CPU.GetEightBitRegister(EightBitRegisterNames.B).ToByte());

            var value = data.CPU.GetPeripheral().DataOut();
            var outputAddress = new SixteenBitAddress(data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL).Value);

            data.CPU.Memory.SetValue(outputAddress, value);

            data.CPU.SetAddressBus_LowByte(data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL).LowOrderByte);
            data.CPU.SetAddressBus_HighByte(data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL).HighOrderByte);
            
            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL).Increment();
            data.CPU.GetEightBitRegister(EightBitRegisterNames.B).Decrement();
            
            data.CPU.SetFlag(FlagNames.Zero, data.CPU.GetEightBitRegister(EightBitRegisterNames.B).ToByte() == ZByte.Empty);
            data.CPU.SetFlag(FlagNames.AddSubtract);
        }
    }
}
