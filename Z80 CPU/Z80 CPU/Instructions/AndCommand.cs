﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Instructions
{
    class AndCommand : ICPUOperation
    {
        private ISource8 _src;

        public AndCommand(ISource8 src)
        {
            _src = src;
        }

        public void HandleOperation(OperationData data)
        {
            var accumulator = data.CPU.GetEightBitRegister(EightBitRegisterNames.Accumulator);
            var accumulatorValue = accumulator.ToByte();
            var srcByte = _src.Value(data);

            var result = new ZByte(
                    accumulatorValue[EightBitLocation.Seven] == Bit.One && srcByte[EightBitLocation.Seven] == Bit.One ? Bit.One : Bit.Zero,
                    accumulatorValue[EightBitLocation.Six] == Bit.One && srcByte[EightBitLocation.Six] == Bit.One ? Bit.One : Bit.Zero,
                    accumulatorValue[EightBitLocation.Five] == Bit.One && srcByte[EightBitLocation.Five] == Bit.One ? Bit.One : Bit.Zero,
                    accumulatorValue[EightBitLocation.Four] == Bit.One && srcByte[EightBitLocation.Four] == Bit.One ? Bit.One : Bit.Zero,
                    accumulatorValue[EightBitLocation.Three] == Bit.One && srcByte[EightBitLocation.Three] == Bit.One ? Bit.One : Bit.Zero,
                    accumulatorValue[EightBitLocation.Two] == Bit.One && srcByte[EightBitLocation.Two] == Bit.One ? Bit.One : Bit.Zero,
                    accumulatorValue[EightBitLocation.One] == Bit.One && srcByte[EightBitLocation.One] == Bit.One ? Bit.One : Bit.Zero,
                    accumulatorValue[EightBitLocation.Zero] == Bit.One && srcByte[EightBitLocation.Zero] == Bit.One ? Bit.One : Bit.Zero
                );

            var sumOfOnes =
                (accumulatorValue[EightBitLocation.Seven] == Bit.One && srcByte[EightBitLocation.Seven] == Bit.One ? 1 : 0) +
                (accumulatorValue[EightBitLocation.Six] == Bit.One && srcByte[EightBitLocation.Six] == Bit.One ? 1 : 0) +
                (accumulatorValue[EightBitLocation.Five] == Bit.One && srcByte[EightBitLocation.Five] == Bit.One ? 1 : 0) +
                (accumulatorValue[EightBitLocation.Four] == Bit.One && srcByte[EightBitLocation.Four] == Bit.One ? 1 : 0) +
                (accumulatorValue[EightBitLocation.Three] == Bit.One && srcByte[EightBitLocation.Three] == Bit.One ? 1 : 0) +
                (accumulatorValue[EightBitLocation.Two] == Bit.One && srcByte[EightBitLocation.Two] == Bit.One ? 1 : 0) +
                (accumulatorValue[EightBitLocation.One] == Bit.One && srcByte[EightBitLocation.One] == Bit.One ? 1 : 0) +
                (accumulatorValue[EightBitLocation.Zero] == Bit.One && srcByte[EightBitLocation.Zero] == Bit.One ? 1 : 0);

            var parityEven = (sumOfOnes % 2) == 0;

            accumulator.FromByte(result);

            data.CPU.SetFlag(FlagNames.Sign, result[EightBitLocation.Seven] == Bit.One);
            data.CPU.SetFlag(FlagNames.Zero, result == ZByte.Empty);
            data.CPU.SetFlag(FlagNames.HalfCarry);
            data.CPU.SetFlag(FlagNames.ParityOverflow, parityEven);
            data.CPU.ClearFlag(FlagNames.AddSubtract);
            data.CPU.ClearFlag(FlagNames.Carry);
        }



    }
}
