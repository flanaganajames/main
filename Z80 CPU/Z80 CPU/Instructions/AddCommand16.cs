﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using Z80_CPU.Register.Utility;

namespace Z80_CPU.Command.Arithmetic
{
    class AddCommand16 : ICPUOperation
    {
        private ISourceDestination16 _dest;
        private ISource16 _src;


        public AddCommand16(ISourceDestination16 dest, ISource16 src)
        {
            _dest = dest;
            _src = src;
        }

        public void HandleOperation(OperationData data)
        {
            var destinationInitialValue = _dest.Value(data);            
            var srcValue = _src.Value(data);

            var DestValueExceptHighestNibble = new ZWord(new ZByte(ZNibble.Empty, destinationInitialValue.HighOrderByte.LowNibble), destinationInitialValue.LowOrderByte);
            var SrcValueExceptHighestNibble = new ZWord(new ZByte(ZNibble.Empty, srcValue.HighOrderByte.LowNibble), srcValue.LowOrderByte);

            var value = (destinationInitialValue + srcValue);

            _dest.SetValue(data, value.LowOrderWord.HighOrderByte, value.LowOrderWord.LowOrderByte);            
            
            data.CPU.SetFlag(FlagNames.HalfCarry, ((ZWord)(DestValueExceptHighestNibble + SrcValueExceptHighestNibble))[SixteenBitLocation.Twelve]);
            data.CPU.ClearFlag(FlagNames.AddSubtract);
            data.CPU.SetFlag(FlagNames.Carry, value[ThirtyTwoBitLocation.Sixteen]);
        }        
    }
}
