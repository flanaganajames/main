﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using Z80_CPU.Command.Addressing;
using Z80_CPU.SourceDestinations;

namespace Z80_CPU.Instructions
{
    class SetResetCommand : ICPUOperation
    {
        private BitPattern _bitPattern;
        private ISourceDestination8 _destination;
        private Bit _valueToAssign;

        public SetResetCommand(BitPattern bitPattern, ISourceDestination8 destination, Bit valueToAssign)
        {
            _bitPattern = bitPattern;
            _destination = destination;
            _valueToAssign = valueToAssign;
        }

        public void HandleOperation(OperationData data)
        {
            var value = _destination.Value(data);
            value[_bitPattern.BitAddressed(data)] = _valueToAssign;

            _destination.SetValue(data, value);
        }
    }
}
