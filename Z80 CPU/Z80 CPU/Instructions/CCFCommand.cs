﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class CCFCommand : ICPUOperation
    {
        public void HandleOperation(OperationData data)
        {            
            data.CPU.SetFlag(FlagNames.HalfCarry, data.CPU.GetFlag(FlagNames.Carry));
            data.CPU.SetFlag(FlagNames.Carry, data.CPU.GetFlag(FlagNames.Carry) != Bit.One);
            data.CPU.ClearFlag(FlagNames.AddSubtract);
        }
    }
}
