﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Command.Arithmetic
{
    class XorCommand8 : ICPUOperation
    {
        private ISource8 _src;

        public XorCommand8(ISource8 src)
        {
            _src = src;
        }

        public void HandleOperation(OperationData data)
        {
            var ARegister = data.CPU.GetEightBitRegister(EightBitRegisterNames.Accumulator);

            var aValue = ARegister.ToByte();
            var srcValue = _src.Value(data);

            var outputValue = aValue.Xor(srcValue);

            ARegister.FromByte(outputValue);
            data.CPU.SetFlag(FlagNames.Sign, outputValue[EightBitLocation.Seven] == Bit.One);
            data.CPU.SetFlag(FlagNames.Zero, outputValue == ZByte.Empty);
            data.CPU.ClearFlag(FlagNames.HalfCarry);
            data.CPU.SetFlag(FlagNames.ParityOverflow, outputValue[EightBitLocation.Zero] == Bit.Zero);
            data.CPU.ClearFlag(FlagNames.AddSubtract);
            data.CPU.ClearFlag(FlagNames.Carry);

        }
    }
}
