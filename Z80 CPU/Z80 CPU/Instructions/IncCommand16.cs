﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using BitClasses;

namespace Z80_CPU.Command.Arithmetic
{
    class IncCommand16 : ICPUOperation
    {
        private ISourceDestination16 _srcDest;

        public IncCommand16(ISourceDestination16 srcDest)
        {
            _srcDest = srcDest;
        }

        public void HandleOperation(OperationData data)
        {
            ZWord originalValue = _srcDest.Value(data);            
            originalValue.Increment();

            _srcDest.SetValue(data, originalValue.HighOrderByte, originalValue.LowOrderByte);
        }
    }
}

