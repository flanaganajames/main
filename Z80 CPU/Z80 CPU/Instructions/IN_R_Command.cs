﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Instructions
{
    class IN_R_Command : ICPUOperation
    {
        private IDestination8 _dest;
        
        public IN_R_Command(IDestination8 dest)
        {
            _dest = dest;
        }

        public void HandleOperation(OperationData data)
        {
            data.CPU.SetAddressBus_LowByte(data.CPU.GetEightBitRegister(EightBitRegisterNames.C).ToByte());
            data.CPU.SetAddressBus_HighByte(data.CPU.GetEightBitRegister(EightBitRegisterNames.B).ToByte());

            var value = data.CPU.GetPeripheral().DataOut();
            _dest.SetValue(data, value);

            data.CPU.SetFlag(FlagNames.Sign, value[EightBitLocation.Seven] == Bit.One);
            data.CPU.SetFlag(FlagNames.Zero, value == ZByte.Empty);
            data.CPU.ClearFlag(FlagNames.HalfCarry);
            data.CPU.SetFlag(FlagNames.ParityOverflow, value.IsParityEven());
            data.CPU.ClearFlag(FlagNames.AddSubtract);
        }
    }
}
