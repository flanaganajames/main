﻿using BaseClasses;
using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class CallCommand : ICPUOperation
    {
        public CallCommand()
        {

        }

        public virtual void HandleOperation(OperationData data)
        {
            var programCounter = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter);
            var programCounterValue = programCounter.Value;
            programCounterValue.IncreaseBy(3); // 3 byte Instruction

            var stackPointer = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.StackPointer);

            stackPointer.Decrement();

            data.CPU.Memory.SetValue(new SixteenBitAddress(stackPointer.Value), programCounterValue.HighOrderByte);

            stackPointer.Decrement();

            data.CPU.Memory.SetValue(new SixteenBitAddress(stackPointer.Value), programCounterValue.LowOrderByte);

            var newProgramCounterValue = new ZWord(data.Third, data.Second);
            newProgramCounterValue.Decrement();// To counteract the three bytes being added on by instruction handler
            newProgramCounterValue.Decrement();
            newProgramCounterValue.Decrement();


            programCounter.Value = newProgramCounterValue;

        }
    }
}
