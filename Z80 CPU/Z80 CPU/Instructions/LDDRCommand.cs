﻿using BaseClasses;
using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class LDDRCommand : ICPUOperation
    {
        public void HandleOperation(OperationData data)
        {
            var valueOfHLMemory = data.CPU.Memory.GetValue(new SixteenBitAddress(data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL).Value));
            data.CPU.Memory.SetValue(new SixteenBitAddress(data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.DE).Value), valueOfHLMemory);

            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.DE).Decrement();
            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL).Decrement();
            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.BC).Decrement();

            if (data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.BC).Value != ZWord.Empty)
            {
                data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter).Decrement();
                data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter).Decrement();
                data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter).Decrement(); // By 4 to counteract the adding two afterwards
                data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter).Decrement();
            }

            data.CPU.ClearFlag(FlagNames.HalfCarry);
            data.CPU.ClearFlag(FlagNames.ParityOverflow);
            data.CPU.ClearFlag(FlagNames.AddSubtract);
        }
    }
}
