﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Command.Load
{
    class Load_A_IR_Command8 : LoadCommand8
    {
        public Load_A_IR_Command8(IDestination8 dest, ISource8 src) : base(dest, src)
        {
            
        }

        public override void HandleOperation(OperationData data)
        {
            base.HandleOperation(data);

            data.CPU.SetFlag(FlagNames.Sign, _src.Value(data)[EightBitLocation.Seven] == Bit.One);
            data.CPU.SetFlag(FlagNames.ParityOverflow, data.CPU.GetFlipFlop(Enums.FlipFlopNames.IFF2));
            data.CPU.SetFlag(FlagNames.Zero, _src.Value(data) == ZByte.Empty);
            data.CPU.ClearFlag(FlagNames.HalfCarry);
            data.CPU.ClearFlag(FlagNames.AddSubtract);
        }
    }
}
