﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Command
{
    class SingleOpCodeCommand : ICPUOperation_SingleByteOpcode
    {
        private ICPUOperation _operation;

        public SingleOpCodeCommand(ZByte opcode, ICPUOperation operation, int numberOfBytes) : this( new List<ZByte>() { opcode }, operation, numberOfBytes) { }

        public SingleOpCodeCommand(List<ZByte> opcodes, ICPUOperation operation, int numberOfBytes)
        {
            HandledOpCodes = opcodes;
            _operation = operation;
            NumberOfBytes = numberOfBytes;
        }

        public List<ZByte> HandledOpCodes
        {
            get;
            private set;
        }

        public int NumberOfBytes
        {
            get;
            private set;
        }

        public void HandleOperation(OperationData data)
        {
            _operation.HandleOperation(data);
        }

        
    }
}
