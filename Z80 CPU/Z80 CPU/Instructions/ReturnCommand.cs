﻿using BaseClasses;
using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class ReturnCommand : ICPUOperation
    {
        public virtual void HandleOperation(OperationData data)
        {
            var stackpointer = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.StackPointer);

            var lowByte = data.CPU.Memory.GetValue(new SixteenBitAddress(stackpointer.Value));
            stackpointer.Increment();
            var highByte = data.CPU.Memory.GetValue(new SixteenBitAddress(stackpointer.Value));
            stackpointer.Increment();

            var programCounter = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter);

            programCounter.Value = new ZWord(highByte, lowByte);
            programCounter.Decrement(); // Counteract the program counter being incremented by instructionhandler
        }
    }
}
