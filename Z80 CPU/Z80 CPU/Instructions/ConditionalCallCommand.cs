﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class ConditionalCallCommand : CallCommand
    {
        public override void HandleOperation(OperationData data)
        {
            var bitList = ToBinaryString(data.First[EightBitLocation.Five]) + ToBinaryString(data.First[EightBitLocation.Four]) + ToBinaryString(data.First[EightBitLocation.Three]);
            var conditionTrue = false;

            switch (bitList)
            {
                case "000":
                    {
                        conditionTrue = data.CPU.GetFlag(FlagNames.Zero) != Bit.One;
                        break;
                    }

                case "001":
                    {
                        conditionTrue = data.CPU.GetFlag(FlagNames.Zero) == Bit.One;
                        break;
                    }

                case "010":
                    {
                        conditionTrue = data.CPU.GetFlag(FlagNames.Carry) != Bit.One;
                        break;
                    }

                case "011":
                    {
                        conditionTrue = data.CPU.GetFlag(FlagNames.Carry) == Bit.One;
                        break;
                    }

                case "100":
                    {
                        conditionTrue = data.CPU.GetFlag(FlagNames.ParityOverflow) != Bit.One;
                        break;
                    }

                case "101":
                    {
                        conditionTrue = data.CPU.GetFlag(FlagNames.ParityOverflow) == Bit.One;
                        break;
                    }

                case "110":
                    {
                        conditionTrue = data.CPU.GetFlag(FlagNames.Sign) != Bit.One;
                        break;
                    }

                case "111":
                    {
                        conditionTrue = data.CPU.GetFlag(FlagNames.Sign) == Bit.One;
                        break;
                    }
            }

            if (conditionTrue)
            {
                base.HandleOperation(data);
            }
        }

        private string ToBinaryString(Bit value)
        {
            return value == Bit.One ? "1" : "0";
        }
    }
}
