﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Instructions
{
    class CPCommand : ICPUOperation
    {
        ISource8 _src = null;

        public CPCommand(ISource8 source)
        {
            _src = source;
        }


        public void HandleOperation(OperationData data)
        {
            var accumulator = data.CPU.GetEightBitRegister(EightBitRegisterNames.Accumulator);
            
            var accMinusSrcValue = accumulator.ToByte().Subtract(_src.Value(data));

            var lowAccumulatorNibbleByte = new ZByte(ZNibble.Empty, accumulator.ToByte().LowNibble);            

            data.CPU.SetFlag(FlagNames.Sign, accMinusSrcValue[EightBitLocation.Seven]);
            data.CPU.SetFlag(FlagNames.Zero, accMinusSrcValue == ZByte.Empty);
            data.CPU.SetFlag(FlagNames.HalfCarry, (lowAccumulatorNibbleByte.Subtract(_src.Value(data)))[EightBitLocation.Four]);
            data.CPU.SetFlag(FlagNames.ParityOverflow, accumulator.ToByte()[EightBitLocation.Seven] == accMinusSrcValue[EightBitLocation.Seven]);
            data.CPU.SetFlag(FlagNames.AddSubtract);
            data.CPU.SetFlag(FlagNames.Carry, _src.Value(data) > accumulator.ToByte());
        }
    }
}
