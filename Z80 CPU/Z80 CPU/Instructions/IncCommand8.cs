﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using BitClasses;

namespace Z80_CPU.Command.Arithmetic
{
    class IncCommand8 : ICPUOperation
    {
        private static readonly ZByte _hex127 = new ZByte(Bit.Zero, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One, Bit.One);

        private ISourceDestination8 _srcDes;

        public IncCommand8(ISourceDestination8 srcDes)
        {
            _srcDes = srcDes;
        }

        public void HandleOperation(OperationData data)
        {
            var value = _srcDes.Value(data);

            Bit oldbit4 = value[EightBitLocation.Four];

            data.CPU.SetFlag(FlagNames.ParityOverflow, value == _hex127);

            value.Increment();

            Bit newbit4 = value[EightBitLocation.Four];

            data.CPU.SetFlag(FlagNames.Sign, value[EightBitLocation.Seven] == Bit.One);
            data.CPU.SetFlag(FlagNames.Zero, value == ZByte.Empty);
            data.CPU.SetFlag(FlagNames.HalfCarry, oldbit4 != newbit4);
            data.CPU.ClearFlag(FlagNames.AddSubtract);

            _srcDes.SetValue(data, value);

            
        }
    }
}
