﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Instructions
{
    class RRCommand : ICPUOperation
    {
        private ISourceDestination8 _srcDes;

        public RRCommand(ISourceDestination8 srcDes)
        {
            _srcDes = srcDes;
        }

        public void HandleOperation(OperationData data)
        {
            var sourceValue = _srcDes.Value(data);

            var newValue = new
                ZByte(
                        data.CPU.GetFlag(FlagNames.Carry),
                        sourceValue[EightBitLocation.Seven],
                        sourceValue[EightBitLocation.Six],
                        sourceValue[EightBitLocation.Five],
                        sourceValue[EightBitLocation.Four],
                        sourceValue[EightBitLocation.Three],
                        sourceValue[EightBitLocation.Two],
                        sourceValue[EightBitLocation.One]
                     );

            _srcDes.SetValue(data, newValue);

            data.CPU.SetFlag(FlagNames.Sign, newValue[EightBitLocation.Seven] == Bit.One);
            data.CPU.SetFlag(FlagNames.Zero, newValue == ZByte.Empty);
            data.CPU.ClearFlag(FlagNames.HalfCarry);
            data.CPU.SetFlag(FlagNames.ParityOverflow, newValue.IsParityEven());
            data.CPU.ClearFlag(FlagNames.AddSubtract);
            data.CPU.SetFlag(FlagNames.Carry, sourceValue[EightBitLocation.Zero]);
        }
    }
}
