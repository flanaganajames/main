﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class CPLCommand : ICPUOperation
    {   
        public void HandleOperation(OperationData data)
        {
            var accumulator = data.CPU.GetEightBitRegister(EightBitRegisterNames.Accumulator);
            var originalValue = accumulator.ToByte();

            var newValue = OneComplement(originalValue);

            accumulator.FromByte(newValue);
            data.CPU.SetFlag(FlagNames.HalfCarry);
            data.CPU.SetFlag(FlagNames.AddSubtract);
        }

        private ZByte OneComplement(ZByte value)
        {
            return new ZByte(
                    InvertBit(value[EightBitLocation.Seven]),
                    InvertBit(value[EightBitLocation.Six]),
                    InvertBit(value[EightBitLocation.Five]),
                    InvertBit(value[EightBitLocation.Four]),
                    InvertBit(value[EightBitLocation.Three]),
                    InvertBit(value[EightBitLocation.Two]),
                    InvertBit(value[EightBitLocation.One]),
                    InvertBit(value[EightBitLocation.Zero])
                );
        }

        private Bit InvertBit(Bit bit)
        {
            return bit == Bit.Zero ? Bit.One : Bit.Zero;
        }


    }
}

