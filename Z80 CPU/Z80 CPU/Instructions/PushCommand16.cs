﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using BaseClasses;

namespace Z80_CPU.Command.Load
{
    class PushCommand16 : ICPUOperation
    {        
        private ISource16 _src;

        public PushCommand16(ISource16 src)
        {            
            _src = src;
        }

        public void HandleOperation(OperationData data)
        {
            var SPRegister = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.StackPointer);
            SPRegister.Decrement();

            var highOrderAddress = new SixteenBitAddress(SPRegister.Value);
            data.CPU.Memory.SetValue(highOrderAddress, _src.HighOrderByte(data));

            SPRegister.Decrement();

            var lowOrderAddress = new SixteenBitAddress(SPRegister.Value);
            data.CPU.Memory.SetValue(lowOrderAddress, _src.LowOrderByte(data));
        }        
    }
}
