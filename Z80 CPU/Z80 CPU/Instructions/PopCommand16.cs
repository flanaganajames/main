﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using BaseClasses;

namespace Z80_CPU.Command.Load
{
    class PopCommand16: ICPUOperation
    {
        private IDestination16 _destination;

        public PopCommand16(IDestination16 dest)
        {
            _destination = dest;
        }

        public void HandleOperation(OperationData data)
        {
            var SPRegister = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.StackPointer);

            var lowOrderAddress = new SixteenBitAddress(SPRegister.Value);
            SPRegister.Increment();
            var highOrderAddress = new SixteenBitAddress(SPRegister.Value);

            _destination.SetValue(data, data.CPU.Memory.GetValue(highOrderAddress), data.CPU.Memory.GetValue(lowOrderAddress));

            SPRegister.Increment();
        }

    }
}
