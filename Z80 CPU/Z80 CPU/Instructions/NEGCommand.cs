﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class NEGCommand : ICPUOperation
    {
        ZByte h_80 = new ZByte(Bit.One, Bit.One, Bit.One, Bit.One, Bit.Zero, Bit.Zero, Bit.Zero, Bit.Zero);

        public void HandleOperation(OperationData data)
        {
            var accumulatorRegister = data.CPU.GetEightBitRegister(EightBitRegisterNames.Accumulator);
            var accumulatorValue = accumulatorRegister.ToByte();

            var lowAccumulatorNibbleByte = new ZByte(ZNibble.Empty, accumulatorValue.LowNibble);
            
            var newValue = ZByte.Empty.Subtract(accumulatorValue);
            
            data.CPU.SetFlag(FlagNames.Sign, newValue[EightBitLocation.Seven] == Bit.Zero);
            data.CPU.SetFlag(FlagNames.Zero, newValue == ZByte.Empty);
            data.CPU.SetFlag(FlagNames.HalfCarry, ((ZByte.Empty.Subtract(lowAccumulatorNibbleByte))[EightBitLocation.Four]));
            data.CPU.SetFlag(FlagNames.ParityOverflow, accumulatorValue == h_80);
            data.CPU.SetFlag(FlagNames.AddSubtract, Bit.One);
            data.CPU.SetFlag(FlagNames.Carry, accumulatorValue != ZByte.Empty);

            accumulatorRegister.FromByte(newValue);

        }
    }
}
