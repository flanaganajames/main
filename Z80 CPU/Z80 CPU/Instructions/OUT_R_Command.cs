﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Instructions
{
    class OUT_R_Command : ICPUOperation
    {
        private ISource8 _src;

        public OUT_R_Command(ISource8 src)
        {
            _src = src;
        }

        public void HandleOperation(OperationData data)
        {
            data.CPU.SetAddressBus_LowByte(data.CPU.GetEightBitRegister(EightBitRegisterNames.C).ToByte());
            data.CPU.SetAddressBus_HighByte(data.CPU.GetEightBitRegister(EightBitRegisterNames.B).ToByte());

            var value = _src.Value(data);
            var peripheral = data.CPU.GetPeripheral();
            peripheral.DataIn(value);            
        }
    }
}
