﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using Z80_CPU.Command.Addressing;
using Z80_CPU.SourceDestinations;

namespace Z80_CPU.Instructions
{
    class BitCommand : ICPUOperation
    {
        private ISource8 _src;
        private BitPattern _bitPattern;

        public BitCommand (BitPattern bitPattern, ISource8 src)
        {
            _src = src;
            _bitPattern = bitPattern;
        }
        
        public void HandleOperation(OperationData data)
        {
            var srcValue = _src.Value(data);

            data.CPU.SetFlag(FlagNames.Zero, srcValue[_bitPattern.BitAddressed(data)] == Bit.Zero);
            data.CPU.SetFlag(FlagNames.HalfCarry);
            data.CPU.ClearFlag(FlagNames.AddSubtract);
        }
    }
}
