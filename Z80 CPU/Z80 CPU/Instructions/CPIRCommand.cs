﻿using BaseClasses;
using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class CPIRCommand : ICPUOperation
    {
        public void HandleOperation(OperationData data)
        {
            var valueOfHLMemory = data.CPU.Memory.GetValue(new SixteenBitAddress(data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL).Value));
            var accumulatorValue = data.CPU.GetEightBitRegister(EightBitRegisterNames.Accumulator).ToByte();
            var accMinusSrcValue = accumulatorValue.Subtract(valueOfHLMemory);
            
            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL).Increment();
            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.BC).Decrement();
            
            if (data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.BC).Value != ZWord.Empty && valueOfHLMemory != accumulatorValue)
            {
                data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter).Decrement();
                data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter).Decrement();
                data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter).Decrement(); // By 4 to counteract the adding two afterwards
                data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter).Decrement();
            }

            var lowAccumulatorNibbleByte = new ZByte(ZNibble.Empty, accumulatorValue.LowNibble);

            data.CPU.SetFlag(FlagNames.Sign, accMinusSrcValue[EightBitLocation.Seven]);
            data.CPU.SetFlag(FlagNames.Zero, valueOfHLMemory == accumulatorValue);
            data.CPU.SetFlag(FlagNames.HalfCarry, (lowAccumulatorNibbleByte.Subtract(valueOfHLMemory))[EightBitLocation.Four]);
            data.CPU.SetFlag(FlagNames.ParityOverflow, data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.BC).Value != ZWord.Empty);
            data.CPU.SetFlag(FlagNames.AddSubtract);
        }    
}
