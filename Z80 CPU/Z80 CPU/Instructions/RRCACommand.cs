﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class RRCACommand : ICPUOperation
    {
        public void HandleOperation(OperationData data)
        {
            var accumulator = data.CPU.GetEightBitRegister(EightBitRegisterNames.Accumulator);

            var startValue = accumulator.ToByte();

            var newValue = new
                ZByte(
                        accumulator[EightBitLocation.Zero],
                        accumulator[EightBitLocation.Seven],
                        accumulator[EightBitLocation.Six],
                        accumulator[EightBitLocation.Five],
                        accumulator[EightBitLocation.Four],
                        accumulator[EightBitLocation.Three],
                        accumulator[EightBitLocation.Two],
                        accumulator[EightBitLocation.One]                                                
                     );

            accumulator.FromByte(newValue);

            data.CPU.SetFlag(FlagNames.Carry, startValue[EightBitLocation.Zero]);
            data.CPU.ClearFlag(FlagNames.HalfCarry);
            data.CPU.ClearFlag(FlagNames.AddSubtract);
        }
    }
}
