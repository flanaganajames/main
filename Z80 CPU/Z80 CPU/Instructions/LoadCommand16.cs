﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Command.Load
{
    class LoadCommand16 : ICPUOperation
    {
        private IDestination16 _dest;
        private ISource16 _src;

        public LoadCommand16(IDestination16 dest, ISource16 src)
        {
            _dest = dest;
            _src = src;
        }

        public void HandleOperation(OperationData data)
        {
            _dest.SetValue(data, _src.HighOrderByte(data), _src.LowOrderByte(data));
        }
    }
}
