﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Instructions
{
    class ADCCommand16 : ICPUOperation
    {
        private ISource16 _src;

        public ADCCommand16(ISource16 src)
        {
            _src = src;
        }

        public void HandleOperation(OperationData data)
        {
            var HLRegister = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL);
            var srcValue = _src.Value(data);

            var HLValueExceptHighestNibble = new ZWord(new ZByte(ZNibble.Empty, HLRegister.HighOrderByte.LowNibble), HLRegister.LowOrderByte);
            var SrcValueExceptHighestNibble = new ZWord(new ZByte(ZNibble.Empty, srcValue.HighOrderByte.LowNibble), srcValue.LowOrderByte);            

            var value = (HLRegister.Value + srcValue.IncreaseBy(data.CPU.GetFlag(FlagNames.Carry) == Bit.One ? 1 : 0));            

            HLRegister.Value = value.LowOrderWord;

            data.CPU.SetFlag(FlagNames.Sign, value.LowOrderWord[SixteenBitLocation.Fifteen] == Bit.One);
            data.CPU.SetFlag(FlagNames.Zero, value.LowOrderWord == ZWord.Empty);
            data.CPU.SetFlag(FlagNames.HalfCarry, ((ZWord)(HLValueExceptHighestNibble + SrcValueExceptHighestNibble.IncreaseBy(data.CPU.GetFlag(FlagNames.Carry) == Bit.One ? 1 : 0)))[SixteenBitLocation.Twelve]);
            data.CPU.SetFlag(FlagNames.ParityOverflow, value[ThirtyTwoBitLocation.Sixteen]);
            data.CPU.ClearFlag(FlagNames.AddSubtract);
            data.CPU.SetFlag(FlagNames.Carry, value[ThirtyTwoBitLocation.Sixteen]);
        }
    }
}
