﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class EXXCommand : ICPUOperation
    {
        public void HandleOperation(OperationData data)
        {

            var BC_Value = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.BC).Value;
            var DE_Value = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.DE).Value;
            var HL_Value = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL).Value;
            
            var BC_Dash_Value = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.BC_Dash).Value;
            var DE_Dash_Value = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.DE_Dash).Value;
            var HL_Dash_Value = data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL_Dash).Value;

            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.BC_Dash).Value = BC_Value;
            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.DE_Dash).Value = DE_Value;
            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL_Dash).Value = HL_Value;

            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.BC).Value = BC_Dash_Value;
            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.DE).Value = DE_Dash_Value;
            data.CPU.GetSixteenBitRegister(SixteenBitRegisterNames.HL).Value = HL_Dash_Value;
        }
    }
}
