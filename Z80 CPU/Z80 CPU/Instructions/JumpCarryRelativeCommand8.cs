﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using Z80_CPU.Command.Addressing;
using Z80_CPU.Command.Jump;

namespace Z80_CPU.Instructions
{
    class JumpCarryRelativeCommand8 : JumpRelativeCommand8
    {   

        public JumpCarryRelativeCommand8(ISource8 src) : base(src)
        {            
        }

        public override void HandleOperation(OperationData data)
        {            
            if (data.CPU.GetFlag(FlagNames.Carry) == Bit.One)
            {
                base.HandleOperation(data);
            }
        }
    }
}
