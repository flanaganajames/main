﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Instructions
{
    class EXCommand : ICPUOperation
    {
        private ISourceDestination16 _first;
        private ISourceDestination16 _second;

        public EXCommand(ISourceDestination16 first, ISourceDestination16 second)
        {
            _first = first;
            _second = second;
        }

        public void HandleOperation(OperationData data)
        {
            var firstValue = _first.Value(data);

            _first.SetValue(data, _second.HighOrderByte(data), _second.LowOrderByte(data));

            _second.SetValue(data, firstValue.HighOrderByte, firstValue.LowOrderByte);
        }
    }
}
