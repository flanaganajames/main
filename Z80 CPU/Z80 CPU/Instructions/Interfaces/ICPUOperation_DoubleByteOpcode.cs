﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Load;
using BitClasses;

namespace Z80_CPU.Command
{
    interface ICPUOperation_DoubleByteOpcode : ICPUOperation
    {
        ZByte FirstOpCode
        {
            get;
        }

        List<ZByte> HandledSecondOpCodes
        {
            get;
        }

        int NumberOfBytes
        {
            get;
        }
    }
}
