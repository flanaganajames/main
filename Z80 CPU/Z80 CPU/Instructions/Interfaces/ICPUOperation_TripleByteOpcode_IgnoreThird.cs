﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions.Interfaces
{
    interface ICPUOperation_TripleByteOpcode_IgnoreThird : ICPUOperation
    {
        ZByte FirstOpCode
        {
            get;
        }

        ZByte SecondOpCode
        {
            get;
        }

        List<ZByte> HandledForthOpCodes
        {
            get;
        }

        int NumberOfBytes
        {
            get;
        }
    }
}
