﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Load;
using BitClasses;

namespace Z80_CPU.Command
{
    interface ICPUOperation_SingleByteOpcode : ICPUOperation
    {
        List<ZByte> HandledOpCodes
        {
            get;
        }

        int NumberOfBytes
        {
            get;
        }
    }
}
