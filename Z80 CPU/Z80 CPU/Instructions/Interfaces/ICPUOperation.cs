﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z80_CPU.Command
{
    interface ICPUOperation
    {
        void HandleOperation(OperationData data);
    }
}
