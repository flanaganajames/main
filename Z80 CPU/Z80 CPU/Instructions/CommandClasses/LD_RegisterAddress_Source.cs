﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z80_CPU.Commands.CommandClasses
{
    abstract class LD8_RegisterAddress_Source : ICPUOperation_SingleByteOpcode
    {
        private SixteenBitRegisterNames _name;

        public LD8_RegisterAddress_Source(SixteenBitRegisterNames name)
        {
            _name = name;
        }

        public abstract List<ZByte> HandledOpCodes
        {
            get;
        }

        protected abstract ZByte GetValue(CPU cpu, ZByte first, ZByte second, ZByte third, ZByte forth);



        public void HandleOperation(CPU cpu, ZByte first, ZByte second, ZByte third, ZByte forth)
        {
            var address = new SixteenBitAddress(cpu.GetSixteenBitRegister(_name));
            var value = GetValue(cpu, first, second, third, forth);

            cpu.Memory.SetValue(address, value);
        }
    }
}
