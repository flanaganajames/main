﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z80_CPU.Commands.CommandClasses
{
    class LD_RegisterAddress_Register : LD8_RegisterAddress_Source
    {
        private List<ZByte> _opcodes;
        private EightBitRegisterNames _registerName;

        public LD_RegisterAddress_Register(ZByte opCode, SixteenBitRegisterNames addressRegisterName, EightBitRegisterNames registerName) : this(new List<ZByte>(){ opCode }, addressRegisterName, registerName)
        {
        }

        public LD_RegisterAddress_Register(List<ZByte> opCodes, SixteenBitRegisterNames addressRegisterName, EightBitRegisterNames registerName) : base(addressRegisterName)
        {
            _opcodes = opCodes;
            _registerName = registerName;
        }

        public override List<ZByte> HandledOpCodes
        {
            get
            {
                return _opcodes;
            }
        }

        protected override ZByte GetValue(CPU cpu, ZByte first, ZByte second, ZByte third, ZByte forth)
        {
            return cpu.GetEightBitRegister(_registerName).ToByte();
        }
    }
}
