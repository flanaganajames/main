﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.Instructions
{
    class SCFCommand : ICPUOperation
    {
        public void HandleOperation(OperationData data)
        {
            data.CPU.SetFlag(FlagNames.Carry);
            data.CPU.ClearFlag(FlagNames.HalfCarry);
            data.CPU.ClearFlag(FlagNames.AddSubtract);
        }
    }
}

