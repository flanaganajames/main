﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using Z80_CPU.Instructions.Interfaces;

namespace Z80_CPU.Instructions
{
    class TripleByteOpcode_IgnoreThirdCommand : ICPUOperation_TripleByteOpcode_IgnoreThird
    {
        private ICPUOperation _operation;

        public TripleByteOpcode_IgnoreThirdCommand(ZByte firstOpCode, ZByte secondOpCode, ZByte forthbyteOpcode, ICPUOperation operation) : this(firstOpCode, secondOpCode, new List<ZByte>() { forthbyteOpcode }, operation)
        {
        }

        public TripleByteOpcode_IgnoreThirdCommand(ZByte firstOpCode, ZByte secondOpCode, List<ZByte> forthbyteOpcodes, ICPUOperation operation)
        {
            FirstOpCode = firstOpCode;
            SecondOpCode = secondOpCode;
            HandledForthOpCodes = forthbyteOpcodes;
            _operation = operation;
        }

        public int NumberOfBytes
        {
            get
            {
                return 4;
            }
        }

        public void HandleOperation(OperationData data)
        {
            _operation.HandleOperation(data);
        }

        public ZByte FirstOpCode
        {
            get;
            private set;
        }

        public ZByte SecondOpCode
        {
            get;
            private set;
        }

        public List<ZByte> HandledForthOpCodes
        {
            get;
            private set;
        }
    }
}
