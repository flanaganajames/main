﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using Z80_CPU.Command.Addressing;

namespace Z80_CPU.Instructions
{
    class SubCommand8 : ICPUOperation
    {
        private ISource8 _src;

        public SubCommand8(ISource8 src)
        {
            _src = src;
        }

        public void HandleOperation(OperationData data)
        {
            var accumulator = data.CPU.GetEightBitRegister(EightBitRegisterNames.Accumulator);

            var accumulatorOriginalByte = accumulator.ToByte();
            var accumulatorOriginalLowerNibble = accumulatorOriginalByte.LowNibble;

            var srcByte = _src.Value(data);
            var srcOriginalLowerNibble = srcByte.LowNibble;

            accumulator.FromByte(accumulator.ToByte().Subtract(_src.Value(data)));

            var halfBorrowOccurred = (accumulatorOriginalLowerNibble - srcOriginalLowerNibble)[EightBitLocation.Four] == Bit.One;
            var overFlowOccured = (accumulatorOriginalByte - srcByte)[SixteenBitLocation.Eight] == Bit.One;

            var newAccumulatorValue = accumulator.ToByte();

            data.CPU.SetFlag(FlagNames.Sign, newAccumulatorValue[EightBitLocation.Seven] == Bit.One);
            data.CPU.SetFlag(FlagNames.Zero, accumulator.ToByte() == ZByte.Empty);
            data.CPU.SetFlag(FlagNames.HalfCarry, halfBorrowOccurred);
            data.CPU.SetFlag(FlagNames.ParityOverflow, overFlowOccured);
            data.CPU.SetFlag(FlagNames.AddSubtract);
            data.CPU.SetFlag(FlagNames.Carry, overFlowOccured);
        }
    }
}
