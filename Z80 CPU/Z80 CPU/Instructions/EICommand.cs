﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Enums;

namespace Z80_CPU.Command.GeneralArithmeticAndCPUControl
{
    class EICommand : ICPUOperation
    {
        public void HandleOperation(OperationData data)
        {
            data.CPU.SetFlipFlop(FlipFlopNames.IFF1, Bit.One);
            data.CPU.SetFlipFlop(FlipFlopNames.IFF2, Bit.One);
        }
    }
}
