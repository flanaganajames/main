﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Command
{
    class OperationData
    {
        public OperationData(CPU cpu, ZByte first, ZByte second, ZByte third, ZByte forth)
        {
            CPU = cpu;
            First = first;
            Second = second;
            Third = third;
            Forth = forth;
        }

        public CPU CPU { get; private set; }
        public ZByte First { get; private set; }
        public ZByte Second { get; private set; }
        public ZByte Third { get; private set; }
        public ZByte Forth { get; private set; }

        public ZByte this[OperandNumber number]
        {
            get
            {
                switch (number)
                {
                    case OperandNumber.First: return First;
                    case OperandNumber.Second: return Second;
                    case OperandNumber.Third: return Third;
                    case OperandNumber.Forth: return Forth;
                }

                throw new ArgumentOutOfRangeException("Operand Number is an Invalid Value");
            }
        }
    }
}
