﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;
using BitClasses;
using Z80_CPU.Register;
using Z80_CPU.Enums;
using BaseClasses;
using Z80_CPU.Peripheral;

namespace Z80_CPU
{
    public class CPU : ICpu
    {
        private Dictionary<EightBitRegisterNames, EightBitRegister> _eightBitRegisters = new Dictionary<EightBitRegisterNames, EightBitRegister>();
        private Dictionary<SixteenBitRegisterNames, I16BitRegister> _sixteenBitRegisters = new Dictionary<SixteenBitRegisterNames, I16BitRegister>();
        private IMemory _memory;
        private InstructionHandler _handler = InstructionHandler.InstructionHandlerGenerator.Generate();
        private Dictionary<FlipFlopNames, Bit> _flipFlops = new Dictionary<FlipFlopNames, Bit>();
        private Dictionary<ZByte, IPeripheral> _outputPeripherals = new Dictionary<ZByte, IPeripheral>();
        private I16BitRegister _addressBus = new SixteenBitRegister();
        
        public CPU()
        {
            _eightBitRegisters.Add(EightBitRegisterNames.Accumulator, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.Flags, new EightBitRegister());            
            _eightBitRegisters.Add(EightBitRegisterNames.B, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.C, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.D, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.E, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.H, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.L, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.Interrupt, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.Refresh, new EightBitRegister());

            _eightBitRegisters.Add(EightBitRegisterNames.A_Dash, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.F_Dash, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.B_Dash, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.C_Dash, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.D_Dash, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.E_Dash, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.H_Dash, new EightBitRegister());
            _eightBitRegisters.Add(EightBitRegisterNames.L_Dash, new EightBitRegister());
            

            _sixteenBitRegisters.Add(SixteenBitRegisterNames.BC, new Double8BitRegister(GetEightBitRegister(EightBitRegisterNames.B), GetEightBitRegister(EightBitRegisterNames.C)));
            _sixteenBitRegisters.Add(SixteenBitRegisterNames.DE, new Double8BitRegister(GetEightBitRegister(EightBitRegisterNames.D), GetEightBitRegister(EightBitRegisterNames.E)));
            _sixteenBitRegisters.Add(SixteenBitRegisterNames.HL, new Double8BitRegister(GetEightBitRegister(EightBitRegisterNames.H), GetEightBitRegister(EightBitRegisterNames.L)));
            _sixteenBitRegisters.Add(SixteenBitRegisterNames.AF, new Double8BitRegister(GetEightBitRegister(EightBitRegisterNames.Accumulator), GetEightBitRegister(EightBitRegisterNames.Flags)));
            _sixteenBitRegisters.Add(SixteenBitRegisterNames.AF_Dash, new Double8BitRegister(GetEightBitRegister(EightBitRegisterNames.A_Dash), GetEightBitRegister(EightBitRegisterNames.F_Dash)));
            _sixteenBitRegisters.Add(SixteenBitRegisterNames.BC_Dash, new Double8BitRegister(GetEightBitRegister(EightBitRegisterNames.B_Dash), GetEightBitRegister(EightBitRegisterNames.C_Dash)));
            _sixteenBitRegisters.Add(SixteenBitRegisterNames.DE_Dash, new Double8BitRegister(GetEightBitRegister(EightBitRegisterNames.D_Dash), GetEightBitRegister(EightBitRegisterNames.E_Dash)));
            _sixteenBitRegisters.Add(SixteenBitRegisterNames.HL_Dash, new Double8BitRegister(GetEightBitRegister(EightBitRegisterNames.H_Dash), GetEightBitRegister(EightBitRegisterNames.L_Dash)));
            _sixteenBitRegisters.Add(SixteenBitRegisterNames.IX, new SixteenBitRegister());
            _sixteenBitRegisters.Add(SixteenBitRegisterNames.IY, new SixteenBitRegister());
            _sixteenBitRegisters.Add(SixteenBitRegisterNames.ProgramCounter, new SixteenBitRegister());
            _sixteenBitRegisters.Add(SixteenBitRegisterNames.StackPointer, new SixteenBitRegister());

            _flipFlops.Add(FlipFlopNames.IFF1, Bit.Zero);
            _flipFlops.Add(FlipFlopNames.IFF2, Bit.Zero);

            for (byte i = 0; i < byte.MaxValue; ++i) _outputPeripherals.Add(new ZByte(i), new EmptyOutputPeripheral());
        }

        public CPU(IMemory memory) : this()
        {
            _memory = memory;
        }

        public void SetMemory(IMemory memory)
        {
            _memory = memory;
        }

        internal EightBitRegister GetEightBitRegister(EightBitRegisterNames name)
        {
            return _eightBitRegisters[name];
        }

        internal void SetFlipFlop(FlipFlopNames name, Bit value)
        {
            _flipFlops.Remove(name);
            _flipFlops.Add(name, value);
        }

        internal void SetAddressBus_HighByte(ZByte newByte)
        {
            _addressBus.HighOrderByte = newByte;
        }

        internal void SetAddressBus_LowByte(ZByte newByte)
        {
            _addressBus.LowOrderByte = newByte;
        }

        internal IPeripheral GetPeripheral()
        {
            return _outputPeripherals[_addressBus.LowOrderByte];
        }

        internal Bit GetFlipFlop(FlipFlopNames name)
        {   
            return _flipFlops[name];
        }

        internal I16BitRegister GetSixteenBitRegister(SixteenBitRegisterNames name)
        {
            return _sixteenBitRegisters[name];
        }

        internal Bit GetFlag(FlagNames name)
        {
            return GetEightBitRegister(EightBitRegisterNames.Flags).ToByte()[(EightBitLocation) name];
        }

        internal void SetFlag(FlagNames name, Bit value)
        {
            var register = GetEightBitRegister(EightBitRegisterNames.Flags);
            EightBitLocation bitLocation = (EightBitLocation) name;
            register[bitLocation] = value;
        }

        internal void ClearFlag(FlagNames name)
        {
            SetFlag(name, Bit.Zero);
        }

        internal void SetFlag(FlagNames name)
        {
            SetFlag(name, Bit.One);
        }

        internal void SetFlag(FlagNames name, bool value)
        {
            SetFlag(name, value ? Bit.One : Bit.Zero);
        }

        internal IMemory Memory
        {
            get
            {
                return _memory;
            }
        }

        public void PerformInstruction()
        {
            var firstByte = Memory.GetValue(new SixteenBitAddress(GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter).Value));
            var secondByte = Memory.GetValue(new SixteenBitAddress(GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter).Value).Increment());
            var thirdByte = Memory.GetValue(new SixteenBitAddress(GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter).Value).Increment().Increment());
            var forthByte = Memory.GetValue(new SixteenBitAddress(GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter).Value).Increment().Increment().Increment());
            
            PerformInstruction(firstByte, secondByte, thirdByte, forthByte);            
        }

        public void PerformInstruction(ZByte opCode)
        {
            PerformInstruction(opCode, ZByte.Empty);
        }

        public void PerformInstruction(ZByte opCode, ZByte second)
        {
            PerformInstruction(opCode, second, ZByte.Empty);
        }

        public void PerformInstruction(ZByte opCode, ZByte second, ZByte third)
        {
            PerformInstruction(opCode, second, third, ZByte.Empty);
        }

        public void PerformInstruction(ZByte opCode, ZByte second, ZByte third, ZByte forth)
        {
            _handler.HandleOperation(this, opCode, second, third, forth);
        }

        public void HalfTick()
        {

        }

        public void Tick()
        {

        }

    }
}



