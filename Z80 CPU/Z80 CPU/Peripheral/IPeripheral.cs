﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z80_CPU.Peripheral
{
    interface IPeripheral
    {
        void DataIn(ZByte data);

        ZByte DataOut();
    }
}
