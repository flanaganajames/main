﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU
{
    internal class BitConversion
    {
        public static ZByte StringOfBitsIntoZByte(string bits)
        {
            if (bits.Length != 8) throw new ArgumentException("bits does not have a length of 8");

            ZByte output = new ZByte(CharacterToBit(bits[0])
                                     , CharacterToBit(bits[1])
                                     , CharacterToBit(bits[2])
                                     , CharacterToBit(bits[3])
                                     , CharacterToBit(bits[4])
                                     , CharacterToBit(bits[5])
                                     , CharacterToBit(bits[6])
                                     , CharacterToBit(bits[7]));

            return output;
        }

        private static Bit CharacterToBit(char c)
        {
            if (c == '1') return Bit.One;
            if (c == '0') return Bit.Zero;

            throw new ArgumentException("c is not a 0 or a 1");
        }

    }
}

