﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Register
{
    public class SixteenBitRegister : I16BitRegister
    {
        private ZWord _internalValue;

        public SixteenBitRegister()
        {
            HighOrderByte = new ZByte();
            LowOrderByte = new ZByte();
        }

        public ZByte HighOrderByte
        {
            get
            {
                return _internalValue.HighOrderByte;
            }
            set
            {
                _internalValue.HighOrderByte = value;
            }
        }

        public ZByte LowOrderByte
        {
            get
            {   
                return _internalValue.LowOrderByte;
            }
            set
            {
                _internalValue.LowOrderByte = value;
            }
        }

        public ZWord Value
        {
            get
            {
                return _internalValue;
            }

            set
            {
                _internalValue = value;
            }
        }

        public void Decrement()
        {
            _internalValue.Decrement();
        }

        public void IncreaseBy(int value)
        {
            _internalValue.IncreaseBy(value);
        }

        public void Increment()
        {
            _internalValue.Increment();
        }

        public override string ToString()
        {
            return _internalValue.ToString();
        }
    }
}

