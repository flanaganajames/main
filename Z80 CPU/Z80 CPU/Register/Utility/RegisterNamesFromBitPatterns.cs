﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Register.Utility
{
    class RegisterNamesFromBitPatterns
    {
        public static EightBitRegisterNames rrrPattern(Bit two, Bit one, Bit zero)
        {
            string bitList = "";

            bitList += two == Bit.One ? "1" : "0";
            bitList += one == Bit.One ? "1" : "0";
            bitList += zero == Bit.One ? "1" : "0";

            switch (bitList)
            {
                case "111": return EightBitRegisterNames.Accumulator;
                case "000": return EightBitRegisterNames.B;
                case "001": return EightBitRegisterNames.C;
                case "010": return EightBitRegisterNames.D;
                case "011": return EightBitRegisterNames.E;
                case "100": return EightBitRegisterNames.H;
                case "101": return EightBitRegisterNames.L;

                default:
                    {
                        throw new ArgumentException(String.Format("Invalid Bits supplied to rrrPattern: {0}", bitList));
                    }
            }
        }

        public static SixteenBitRegisterNames ddBitPattern(Bit one, Bit zero)
        {
            string bitList = "";

            bitList += one == Bit.One ? "1" : "0";
            bitList += zero == Bit.One ? "1" : "0";

            switch (bitList)
            {
                case "00": return SixteenBitRegisterNames.BC;
                case "01": return SixteenBitRegisterNames.DE;
                case "10": return SixteenBitRegisterNames.HL;
                case "11": return SixteenBitRegisterNames.StackPointer;

                default:
                    {
                        throw new ArgumentException(String.Format("Invalid Bits supplied to ddBitPattern: {0}", bitList));
                    }
            }
        }

        public static SixteenBitRegisterNames ssBitPattern(Bit one, Bit zero)
        {
            return ddBitPattern(one, zero);
        }

        public static SixteenBitRegisterNames qqBitPattern(Bit one, Bit zero)
        {
            string bitList = "";

            bitList += one == Bit.One ? "1" : "0";
            bitList += zero == Bit.One ? "1" : "0";

            switch (bitList)
            {
                case "00": return SixteenBitRegisterNames.BC;
                case "01": return SixteenBitRegisterNames.DE;
                case "10": return SixteenBitRegisterNames.HL;
                case "11": return SixteenBitRegisterNames.AF;

                default:
                    {
                        throw new ArgumentException(String.Format("Invalid Bits supplied to qqBitPattern: {0}", bitList));
                    }
            }
        }

        public static SixteenBitRegisterNames ppBitPattern(Bit one, Bit zero)
        {
            string bitList = "";

            bitList += one == Bit.One ? "1" : "0";
            bitList += zero == Bit.One ? "1" : "0";

            switch (bitList)
            {
                case "00": return SixteenBitRegisterNames.BC;
                case "01": return SixteenBitRegisterNames.DE;
                case "10": return SixteenBitRegisterNames.IX;
                case "11": return SixteenBitRegisterNames.StackPointer;

                default:
                    {
                        throw new ArgumentException(String.Format("Invalid Bits supplied to ppBitPattern: {0}", bitList));
                    }
            }
        }

        public static SixteenBitRegisterNames rrBitPattern(Bit one, Bit zero)
        {
            string bitList = "";

            bitList += one == Bit.One ? "1" : "0";
            bitList += zero == Bit.One ? "1" : "0";

            switch (bitList)
            {
                case "00": return SixteenBitRegisterNames.BC;
                case "01": return SixteenBitRegisterNames.DE;
                case "10": return SixteenBitRegisterNames.IY;
                case "11": return SixteenBitRegisterNames.StackPointer;

                default:
                    {
                        throw new ArgumentException(String.Format("Invalid Bits supplied to ppBitPattern: {0}", bitList));
                    }
            }
        }

    }
}
