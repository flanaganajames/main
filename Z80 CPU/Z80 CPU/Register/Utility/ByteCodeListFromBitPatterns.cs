﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Register.Utility
{
    static class ByteCodeListFromBitPatterns
    {
        public static List<string> BitAddressingCodes()
        {
            return new List<String>()
            {
                "000",
                "001",
                "010",
                "011",
                "100",
                "101",
                "110",
                "111",
            };
        }

        public static List<string> CalConditionalBits()
        {
            return new List<String>()
            {
                "000",
                "001",
                "010",
                "011",
                "100",
                "101",
                "110",
                "111",
            };
        }

        public static List<string> EightRegisterCodes()
        {
            return new List<string>()
                {
                    "111",
                    "000",
                    "001",
                    "010",
                    "011",
                    "100",
                    "101"
                };
        }

        public static List<string> SixteenBitRegisterCodes()
        {
            return new List<string>()
                {
                    "00",
                    "01",
                    "10",
                    "11"
                };
        }

        public static List<ZByte> ByteCodeListFrom_rrr_Pattern(string pattern)
        {
            return GeneratePatternList(EightRegisterCodes(), pattern, "rrr");
        }

        public static List<ZByte> ByteCodeListFrom_rrr_sss_Pattern(string pattern)
        {
            var registerValues = EightRegisterCodes();

            List<ZByte> output = new List<ZByte>();

            for (int source = 0; source < registerValues.Count; source++)
            {
                output.AddRange(ByteCodeListFrom_rrr_Pattern(pattern.Replace("sss", registerValues[source])));
            }

            return output;
        }

        public static List<ZByte> ByteCodeListFrom_bbb_rrr_Pattern(string pattern)
        {
            var bitValues = BitAddressingCodes();

            List<ZByte> output = new List<ZByte>();

            for (int source = 0; source < bitValues.Count; source++)
            {
                output.AddRange(ByteCodeListFrom_rrr_Pattern(pattern.Replace("bbb", bitValues[source])));
            }

            return output;
        }

        public static List<ZByte> ByteCodeListFrom_bbb_Pattern(string pattern)
        {
            return GeneratePatternList(BitAddressingCodes(), pattern, "bbb");
        }

        public static List<ZByte> ByteCodeListFrom_ccc_Pattern(string pattern)
        {
            return GeneratePatternList(CalConditionalBits(), pattern, "ccc");
        }

        public static List<ZByte> ByteCodeListFrom_dd_Pattern(string pattern)
        {
            return GeneratePatternList(SixteenBitRegisterCodes(), pattern, "dd");
        }

        public static List<ZByte> ByteCodeListFrom_ss_Pattern(string pattern)
        {
            return GeneratePatternList(SixteenBitRegisterCodes(), pattern, "ss");
        }

        public static List<ZByte> ByteCodeListFrom_qq_Pattern(string pattern)
        {
            return GeneratePatternList(SixteenBitRegisterCodes(), pattern, "qq");
        }

        public static List<ZByte> ByteCodeListFrom_rr_Pattern(string pattern)
        {
            return GeneratePatternList(SixteenBitRegisterCodes(), pattern, "rr");
        }

        public static List<ZByte> ByteCodeListFrom_pp_Pattern(string pattern)
        {
            return GeneratePatternList(SixteenBitRegisterCodes(), pattern, "pp");
        }

        public static List<ZByte> GeneratePatternList(List<string> registerValues, string pattern, string patternCode)
        {
            List<ZByte> output = new List<ZByte>();

            for (int source = 0; source < registerValues.Count; source++)
            {
                output.Add(BitConversion.StringOfBitsIntoZByte(pattern.Replace(patternCode, registerValues[source])));
            }

            return output;
        }

    }
}
