﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Register
{
    interface I16BitRegister
    {
        ZByte HighOrderByte { get; set; }

        ZByte LowOrderByte { get; set; }

        void Decrement();

        void Increment();

        ZWord Value { get; set; }

        void IncreaseBy (int value);      
    }
}
