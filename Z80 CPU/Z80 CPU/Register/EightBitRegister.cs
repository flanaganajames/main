﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Register
{
    internal class EightBitRegister
    {
        private ZByte _bits = new ZByte();

        public Bit this[EightBitLocation location]
        {
            get
            {
                return _bits[location];
            }
            set
            {
                _bits[location] = value;
            }
        }

        public ZByte ToByte()
        {
            return _bits;
        }

        public void FromByte(ZByte value)
        {
            _bits = value;            
        }

        public override string ToString()
        {
            return _bits.ToString();
        }

        public void Decrement()
        {
            _bits.Decrement();
        }
    }
}
