﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Register
{
    class Double8BitRegister : I16BitRegister
    {
        private EightBitRegister _firstRegister;
        private EightBitRegister _secondRegister;
        
        public Double8BitRegister(EightBitRegister first, EightBitRegister second)
        {
            _firstRegister = first;
            _secondRegister = second;
        }

        public ZByte HighOrderByte
        {
            get
            {
                return _firstRegister.ToByte();
            }

            set
            {
                _firstRegister.FromByte(value);
            }
        }

        public ZByte LowOrderByte
        {
            get
            {
                return _secondRegister.ToByte();
            }

            set
            {
                _secondRegister.FromByte(value);
            }
        }

        public ZWord Value
        {
            get
            {
                return new ZWord(HighOrderByte, LowOrderByte);
            }

            set
            {
                HighOrderByte = value.HighOrderByte;
                LowOrderByte = value.LowOrderByte;
            }
        }

        public void Decrement()
        {
            ZWord value = new ZWord(HighOrderByte, LowOrderByte);
            value.Decrement();

            HighOrderByte = value.HighOrderByte;
            LowOrderByte = value.LowOrderByte;            
        }

        public void IncreaseBy(int value)
        {   
            Value = Value.IncreaseBy(value);
        }

        public void Increment()
        {
            ZWord value = new ZWord(HighOrderByte, LowOrderByte);
            value.Increment();

            HighOrderByte = value.HighOrderByte;
            LowOrderByte = value.LowOrderByte;
        }

        public override string ToString()
        {
            return _firstRegister.ToString() + _secondRegister.ToString();
        }
    }
}
