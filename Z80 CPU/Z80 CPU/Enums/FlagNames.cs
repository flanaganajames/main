﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z80_CPU
{
    enum FlagNames : byte
    {
        Carry = 7,
        AddSubtract = 6,
        ParityOverflow = 5,
        HalfCarry = 3,
        Zero = 1,
        Sign = 0
    }
}
