﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z80_CPU
{
    public enum EightBitRegisterNames
    {
        Accumulator,
        Flags,
        B,
        C,
        D,
        E,
        H,
        L,
        Interrupt,
        Refresh,
        A_Dash,
        F_Dash,
        B_Dash,
        C_Dash,
        D_Dash,
        E_Dash,
        H_Dash,
        L_Dash
    }
}
