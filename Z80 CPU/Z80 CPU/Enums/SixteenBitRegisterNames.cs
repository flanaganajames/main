﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z80_CPU
{
    enum SixteenBitRegisterNames
    {
        BC,
        DE,
        HL,
        IX,
        IY,
        StackPointer,
        ProgramCounter,
        AF,
        AF_Dash,
        BC_Dash,
        DE_Dash,
        HL_Dash
    }
}
