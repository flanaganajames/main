﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Command.Addressing
{
    interface IDestination8
    {
        void SetValue(OperationData data, ZByte value);
    }
}
