﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;
using BaseClasses;

namespace Z80_CPU.Command.Addressing
{
    class SD8_DisplacedRegisterAddress : ISourceDestination8
    {
        private SixteenBitRegisterNames _name;
        private OperandNumber _displacementOperand;

        public SD8_DisplacedRegisterAddress(SixteenBitRegisterNames name, OperandNumber displacmentOperand)
        {
            _name = name;
            _displacementOperand = displacmentOperand;
        }
        
        public ZByte Value(OperationData data)
        {
            var address = new SixteenBitAddress(data.CPU.GetSixteenBitRegister(_name).Value);
            address.AddSignedByte(data[_displacementOperand]);

            return data.CPU.Memory.GetValue(address);
        }

        public void SetValue(OperationData data, ZByte value)
        {
            var address = new SixteenBitAddress(data.CPU.GetSixteenBitRegister(_name).Value);
            address.AddSignedByte(data[_displacementOperand]);

            data.CPU.Memory.SetValue(address, value);
        }
    }
}
