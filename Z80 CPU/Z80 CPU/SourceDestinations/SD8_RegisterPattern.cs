﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;
using Z80_CPU.Register.Utility;

namespace Z80_CPU.Command.Addressing
{
    class SD8_RegisterPattern : ISourceDestination8
    {
        private OperandNumber _operandWithPattern;
        private EightBitLocation _mostSignificantBitOfPattern;
        private EightBitLocation _secondMostSignificantBitOfPattern;
        private EightBitLocation _thirdMostSignificantBitOfPattern;

        public SD8_RegisterPattern(OperandNumber operandWithPattern, EightBitLocation mostSignificantBitOfPattern, EightBitLocation secondMostSignificantBitOfPattern, EightBitLocation thirdMostSignificantBitOfPattern)
        {
            _operandWithPattern = operandWithPattern;
            _mostSignificantBitOfPattern = mostSignificantBitOfPattern;
            _secondMostSignificantBitOfPattern = secondMostSignificantBitOfPattern;
            _thirdMostSignificantBitOfPattern = thirdMostSignificantBitOfPattern;
        }
        
        public ZByte Value(OperationData data)
        {
            var operand = data[_operandWithPattern];
            var registerName = RegisterNamesFromBitPatterns.rrrPattern(operand[_mostSignificantBitOfPattern], operand[_secondMostSignificantBitOfPattern], operand[_thirdMostSignificantBitOfPattern]);
            var register = data.CPU.GetEightBitRegister(registerName);

            return register.ToByte();
        }

        public void SetValue(OperationData data, ZByte value)
        {
            var operand = data[_operandWithPattern];
            var registerName = RegisterNamesFromBitPatterns.rrrPattern(operand[_mostSignificantBitOfPattern], operand[_secondMostSignificantBitOfPattern], operand[_thirdMostSignificantBitOfPattern]);
            var register = data.CPU.GetEightBitRegister(registerName);

            register.FromByte(value);
        }
    }
}
