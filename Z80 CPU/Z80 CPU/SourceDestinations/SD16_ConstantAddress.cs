﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using BitClasses;
using BaseClasses;

namespace Z80_CPU.Command.Addressing
{
    class SD16_ConstantAddress : ISourceDestination16
    {
        private OperandNumber _lowOrderByte;
        private OperandNumber _highOrderByte;

        public SD16_ConstantAddress(OperandNumber lowOrderByte, OperandNumber highOrderByte)
        {
            _lowOrderByte = lowOrderByte;
            _highOrderByte = highOrderByte;
        }        

        public ZByte HighOrderByte(OperationData data)
        {
            var address = new SixteenBitAddress(data[_highOrderByte], data[_lowOrderByte]);
            address.Increment();

            return data.CPU.Memory.GetValue(address);
        }

        public ZByte LowOrderByte(OperationData data)
        {
            var address = new SixteenBitAddress(data[_highOrderByte], data[_lowOrderByte]);
            return data.CPU.Memory.GetValue(address);
        }

        public void SetValue(OperationData data, ZByte highOrderByte, ZByte lowOrderByte)
        {
            var lowOrderAddress = new SixteenBitAddress(data[_highOrderByte], data[_lowOrderByte]);
            data.CPU.Memory.SetValue(lowOrderAddress, lowOrderByte);

            var highOrderAddress = new SixteenBitAddress(data[_highOrderByte], data[_lowOrderByte]);
            highOrderAddress.Increment();
            data.CPU.Memory.SetValue(highOrderAddress, highOrderByte);
        }

        public ZWord Value(OperationData data)
        {
            return new ZWord(HighOrderByte(data), LowOrderByte(data));
        }
    }
}
