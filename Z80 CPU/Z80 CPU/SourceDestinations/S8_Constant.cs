﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Command.Addressing
{
    class S8_Constant : ISource8
    {
        private OperandNumber _number;

        public S8_Constant(OperandNumber number)
        {
            _number = number;
        }

        public ZByte Value(OperationData data)
        {
            return data[_number];
        }
    }
}

