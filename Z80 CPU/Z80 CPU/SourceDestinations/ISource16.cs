﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Command.Addressing
{
    interface ISource16
    {
        ZByte HighOrderByte(OperationData data);

        ZByte LowOrderByte(OperationData data);

        ZWord Value(OperationData data);
    }
}
