﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using BitClasses;

namespace Z80_CPU.Command.Addressing
{
    class S16_Constant : ISource16
    {
        private OperandNumber _lowOrderByte;
        private OperandNumber _highOrderByte;

        public S16_Constant(OperandNumber lowOrderByte, OperandNumber highOrderByte)
        {
            _lowOrderByte = lowOrderByte;
            _highOrderByte = highOrderByte;                 
        }        

        public ZByte HighOrderByte(OperationData data)
        {
            return data[_highOrderByte];
        }

        public ZByte LowOrderByte(OperationData data)
        {
            return data[_lowOrderByte];
        }

        public ZWord Value(OperationData data)
        {
            return new ZWord(HighOrderByte(data), LowOrderByte(data));
        }
    }
}
