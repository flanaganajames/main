﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;
using Z80_CPU.Register.Utility;

namespace Z80_CPU.Command.Addressing
{
    class SD16_SSRegisterPattern : SD16_RegisterPattern
    {
        public SD16_SSRegisterPattern(OperandNumber operandWithPattern, EightBitLocation mostSignificantBitOfPattern, EightBitLocation secondMostSignificantBitOfPattern) : base(operandWithPattern, mostSignificantBitOfPattern,secondMostSignificantBitOfPattern)
        {

        }

        protected override SixteenBitRegisterNames GetRegisterName(Bit mostSignificantBitOfPattern, Bit secondMostSignificantBitOfPattern)
        {
            return RegisterNamesFromBitPatterns.ssBitPattern(mostSignificantBitOfPattern, secondMostSignificantBitOfPattern);
        }
    }
}
