﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command;

namespace Z80_CPU.SourceDestinations
{
    class BitPattern
    {
        private OperandNumber _operandWithPattern;
        private EightBitLocation _mostSignificantBitOfPattern;
        private EightBitLocation _secondMostSignificantBitOfPattern;
        private EightBitLocation _thirdMostSignificantBitOfPattern;

        public BitPattern(OperandNumber operandWithPattern, EightBitLocation mostSignificantBitOfPattern, EightBitLocation secondMostSignificantBitOfPattern, EightBitLocation thirdMostSignificantBitOfPattern)
        {
            _operandWithPattern = operandWithPattern;
            _mostSignificantBitOfPattern = mostSignificantBitOfPattern;
            _secondMostSignificantBitOfPattern = secondMostSignificantBitOfPattern;
            _thirdMostSignificantBitOfPattern = thirdMostSignificantBitOfPattern;
        }

        public EightBitLocation BitAddressed(OperationData data)
        {
            var value = data[_operandWithPattern];

            var bitList = ToBinaryString(value[_mostSignificantBitOfPattern]) + ToBinaryString(value[_secondMostSignificantBitOfPattern]) + ToBinaryString(value[_thirdMostSignificantBitOfPattern]);

            switch (bitList)
            {
                case "000" : return EightBitLocation.Zero;
                case "001": return EightBitLocation.One;
                case "010": return EightBitLocation.Two;
                case "011": return EightBitLocation.Three;
                case "100": return EightBitLocation.Four;
                case "101": return EightBitLocation.Five;
                case "110": return EightBitLocation.Six;
                case "111": return EightBitLocation.Seven;
                default: throw new ArgumentOutOfRangeException("Invalid arguments supplied");
            }
        }

        private string ToBinaryString(Bit value)
        {
            return value == Bit.One ? "1" : "0";
        }


    }
}
