﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using BitClasses;

namespace Z80_CPU.Command.Addressing
{
    class SD16_Register : ISourceDestination16
    {
        private SixteenBitRegisterNames _name;

        public SD16_Register(SixteenBitRegisterNames name)
        {
            _name = name;
        }

        public ZByte HighOrderByte(OperationData data)
        {
            return data.CPU.GetSixteenBitRegister(_name).HighOrderByte;
        }

        public ZByte LowOrderByte(OperationData data)
        {
            return data.CPU.GetSixteenBitRegister(_name).LowOrderByte;
        }

        public void SetValue(OperationData data, ZByte highOrderByte, ZByte lowOrderByte)
        {
            var register = data.CPU.GetSixteenBitRegister(_name);
            register.LowOrderByte = lowOrderByte;
            register.HighOrderByte = highOrderByte;
        }

        public ZWord Value(OperationData data)
        {
            return data.CPU.GetSixteenBitRegister(_name).Value;
        }
    }
}
