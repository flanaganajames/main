﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;
using BaseClasses;

namespace Z80_CPU.Command.Addressing
{
    class SD8_ConstantAddress : ISourceDestination8
    {
        private OperandNumber _lowOrderByte;
        private OperandNumber _highOrderByte;

        public SD8_ConstantAddress(OperandNumber lowOrderByte, OperandNumber highOrderByte)
        {
            _lowOrderByte = lowOrderByte;
            _highOrderByte = highOrderByte;
        }

        public ZByte Value(OperationData data)
        {
            var address = new SixteenBitAddress(data[_highOrderByte], data[_lowOrderByte]);

            return data.CPU.Memory.GetValue(address);
        }

        public void SetValue(OperationData data, ZByte value)
        {
            var address = new SixteenBitAddress(data[_highOrderByte], data[_lowOrderByte]);

            data.CPU.Memory.SetValue(address, value);
        }
    }
}


