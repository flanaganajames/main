﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;
using BaseClasses;

namespace Z80_CPU.Command.Addressing
{
    class SD8_RegisterAddress : ISourceDestination8
    {
        private SixteenBitRegisterNames _name;

        public SD8_RegisterAddress(SixteenBitRegisterNames name)
        {
            _name = name;
        }

        public ZByte Value(OperationData data)
        {
            return data.CPU.Memory.GetValue(new SixteenBitAddress(data.CPU.GetSixteenBitRegister(_name).Value));
        }

        public void SetValue(OperationData data, ZByte value)
        {
            data.CPU.Memory.SetValue(new SixteenBitAddress(data.CPU.GetSixteenBitRegister(_name).Value), value);
        }
    }
}
