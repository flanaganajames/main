﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Command.Addressing
{
    class SD8_Register : ISourceDestination8
    {
        private EightBitRegisterNames _name;

        public SD8_Register(EightBitRegisterNames name)
        {
            _name = name;
        }

        public ZByte Value(OperationData data)
        {
            return data.CPU.GetEightBitRegister(_name).ToByte();
        }

        public void SetValue(OperationData data, ZByte value)
        {
            data.CPU.GetEightBitRegister(_name).FromByte(value);
        }
    }
}
