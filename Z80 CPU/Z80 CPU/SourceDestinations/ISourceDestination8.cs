﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z80_CPU.Command.Addressing
{
    interface ISourceDestination8 : ISource8, IDestination8
    {
    }
}
