﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z80_CPU.Command.Addressing
{
    interface ISourceDestination16 : ISource16, IDestination16
    {
    }
}
