﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;
using Z80_CPU.Command;
using Z80_CPU.Command.Addressing;
using BaseClasses;

namespace Z80_CPU.SourceDestinations
{
    class SD16_RegisterAddress : ISourceDestination16
    {
        private SixteenBitRegisterNames _registerName;

        public SD16_RegisterAddress(SixteenBitRegisterNames registerName)
        {
            _registerName = registerName;
        }


        public ZByte HighOrderByte(OperationData data)
        {
            return Value(data).HighOrderByte;
        }

        public ZByte LowOrderByte(OperationData data)
        {
            return Value(data).LowOrderByte;
        }

        public void SetValue(OperationData data, ZByte highOrderByte, ZByte lowOrderByte)
        {
            var lowOrderAddress = new SixteenBitAddress(data.CPU.GetSixteenBitRegister(_registerName).Value);
            data.CPU.Memory.SetValue(lowOrderAddress, lowOrderByte);

            lowOrderAddress.Increment();
            data.CPU.Memory.SetValue(lowOrderAddress, highOrderByte);
        }

        public ZWord Value(OperationData data)
        {
            var lowOrderAddress = new SixteenBitAddress(data.CPU.GetSixteenBitRegister(_registerName).Value);

            var lowOrderByte = data.CPU.Memory.GetValue(lowOrderAddress);
            lowOrderAddress.Increment();

            var highOrderByte = data.CPU.Memory.GetValue(lowOrderAddress);

            return new ZWord(highOrderByte, lowOrderByte);

        }
    }
}
