﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Command.Addressing
{
    interface IDestination16
    {
        void SetValue(OperationData data, ZByte highOrderByte, ZByte lowOrderByte);
    }
}
