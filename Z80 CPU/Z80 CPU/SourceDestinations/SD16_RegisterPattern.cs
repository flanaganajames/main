﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitClasses;

namespace Z80_CPU.Command.Addressing
{
    abstract class SD16_RegisterPattern : ISourceDestination16
    {
        private OperandNumber _operandWithPattern;
        private EightBitLocation _mostSignificantBitOfPattern;
        private EightBitLocation _secondMostSignificantBitOfPattern;

        public SD16_RegisterPattern(OperandNumber operandWithPattern, EightBitLocation mostSignificantBitOfPattern, EightBitLocation secondMostSignificantBitOfPattern)
        {
            _operandWithPattern = operandWithPattern;
            _mostSignificantBitOfPattern = mostSignificantBitOfPattern;
            _secondMostSignificantBitOfPattern = secondMostSignificantBitOfPattern;
        }

        public ZByte HighOrderByte(OperationData data)
        {
            var operand = data[_operandWithPattern];
            var registerName = GetRegisterName(operand[_mostSignificantBitOfPattern], operand[_secondMostSignificantBitOfPattern]);
            var register = data.CPU.GetSixteenBitRegister(registerName);
            return register.HighOrderByte;
        }

        public ZByte LowOrderByte(OperationData data)
        {
            var operand = data[_operandWithPattern];
            var registerName = GetRegisterName(operand[_mostSignificantBitOfPattern], operand[_secondMostSignificantBitOfPattern]);
            var register = data.CPU.GetSixteenBitRegister(registerName);
            return register.LowOrderByte;
        }

        public void SetValue(OperationData data, ZByte highOrderByte, ZByte lowOrderByte)
        {
            var operand = data[_operandWithPattern];
            var registerName = GetRegisterName(operand[_mostSignificantBitOfPattern], operand[_secondMostSignificantBitOfPattern]);
            var register = data.CPU.GetSixteenBitRegister(registerName);

            register.LowOrderByte = lowOrderByte;
            register.HighOrderByte = highOrderByte;
        }

        protected abstract SixteenBitRegisterNames GetRegisterName(Bit mostSignificantBitOfPattern, Bit secondMostSignificantBitOfPattern);

        public ZWord Value(OperationData data)
        {
            var operand = data[_operandWithPattern];
            var registerName = GetRegisterName(operand[_mostSignificantBitOfPattern], operand[_secondMostSignificantBitOfPattern]);
            var register = data.CPU.GetSixteenBitRegister(registerName);
            return register.Value;
        }
    }
}
