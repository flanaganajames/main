﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Arithmetic;
using Z80_CPU.Command.Instruction;
using Z80_CPU.Command.Addressing;
using Z80_CPU.Command.Load;
using BitClasses;
using Z80_CPU.Instructions.Interfaces;

namespace Z80_CPU.Command
{
    class InstructionHandler
    {
        private Dictionary<ZByte, ICPUOperation_SingleByteOpcode> _singleByteOpCodeOperations = new Dictionary<ZByte, ICPUOperation_SingleByteOpcode>();
        private Dictionary<ZByte, Dictionary<ZByte, ICPUOperation_DoubleByteOpcode>> _doubleByteOpCodeOperations = new Dictionary<ZByte, Dictionary<ZByte, ICPUOperation_DoubleByteOpcode>>();
        private Dictionary<ZByte, Dictionary<ZByte, Dictionary<ZByte, ICPUOperation_TripleByteOpcode_IgnoreThird>>> _tripleByteOpCode_ignoreThirdOperations = new Dictionary<ZByte, Dictionary<ZByte, Dictionary<ZByte, ICPUOperation_TripleByteOpcode_IgnoreThird>>>();

        internal void AddCommand(ICPUOperation_SingleByteOpcode operation)
        {
            foreach (var item in operation.HandledOpCodes)
            {
                _singleByteOpCodeOperations.Add(item, operation);
            }
        }

        internal void AddCommand(ICPUOperation_DoubleByteOpcode operation)
        {
            Dictionary<ZByte, ICPUOperation_DoubleByteOpcode> internalDictionary;

            if (!_doubleByteOpCodeOperations.TryGetValue(operation.FirstOpCode, out internalDictionary))
            {
                internalDictionary = new Dictionary<ZByte, ICPUOperation_DoubleByteOpcode>();
            }

            foreach ( var item in operation.HandledSecondOpCodes)
            {
                internalDictionary.Add(item, operation);
            }

            _doubleByteOpCodeOperations.Remove(operation.FirstOpCode);
            _doubleByteOpCodeOperations.Add(operation.FirstOpCode, internalDictionary);
        }

        internal void AddCommand(ICPUOperation_TripleByteOpcode_IgnoreThird operation)
        {
            Dictionary<ZByte, Dictionary<ZByte, ICPUOperation_TripleByteOpcode_IgnoreThird>> internalDictionary_one;
            Dictionary<ZByte, ICPUOperation_TripleByteOpcode_IgnoreThird> internalDictionary_two;

            if (!_tripleByteOpCode_ignoreThirdOperations.TryGetValue(operation.FirstOpCode, out internalDictionary_one))
            {
                internalDictionary_one = new Dictionary<ZByte, Dictionary<ZByte, ICPUOperation_TripleByteOpcode_IgnoreThird>>();
            }

            if (!internalDictionary_one.TryGetValue(operation.SecondOpCode, out internalDictionary_two))
            {
                internalDictionary_two = new Dictionary<ZByte, ICPUOperation_TripleByteOpcode_IgnoreThird>();                
            }

            foreach (var item in operation.HandledForthOpCodes)
            {
                internalDictionary_two.Add(item, operation);
            }

            _tripleByteOpCode_ignoreThirdOperations.Remove(operation.FirstOpCode);
            _tripleByteOpCode_ignoreThirdOperations.Add(operation.FirstOpCode, internalDictionary_one);

            internalDictionary_one.Remove(operation.SecondOpCode);
            internalDictionary_one.Add(operation.SecondOpCode, internalDictionary_two);

        }

        public void HandleOperation(CPU cpu, ZByte opCode, ZByte second, ZByte third, ZByte forth)
        {            
            ICPUOperation selectedOperation = null;
            int programCounterAddition = 0;

            var data = new OperationData(cpu, opCode, second, third, forth);

            ICPUOperation_SingleByteOpcode singleOutput = null;
            Dictionary<ZByte, ICPUOperation_DoubleByteOpcode> doubleOutputList = null;
            ICPUOperation_DoubleByteOpcode doubleOutput = null;
            //Dictionary<ZByte, Dictionary<ZByte, ICPUOperation_TripleByteOpcode_IgnoreThird>> tripleOutputListOne = null;
            //Dictionary<ZByte, ICPUOperation_TripleByteOpcode_IgnoreThird> tripleOutputListTwo = null;
            ICPUOperation_TripleByteOpcode_IgnoreThird tripleOutput = null;


            if (_singleByteOpCodeOperations.TryGetValue(opCode, out singleOutput))
            {
                selectedOperation = singleOutput;
                programCounterAddition = singleOutput.NumberOfBytes;
            }
            else if (_doubleByteOpCodeOperations.TryGetValue(opCode, out doubleOutputList) && doubleOutputList.TryGetValue(second, out doubleOutput))
            {   
                selectedOperation = doubleOutput;
                programCounterAddition = doubleOutput.NumberOfBytes;
            }
            else 
            {
                tripleOutput = _tripleByteOpCode_ignoreThirdOperations[opCode][second][forth];
                selectedOperation = tripleOutput;
                programCounterAddition = tripleOutput.NumberOfBytes;
            }

            cpu.GetSixteenBitRegister(SixteenBitRegisterNames.ProgramCounter).IncreaseBy(programCounterAddition);
            selectedOperation.HandleOperation(data);
        }

        private InstructionHandler()
        {

        }


        public static class InstructionHandlerGenerator
        {
            public static InstructionHandler Generate()
            {
                var ch = new InstructionHandler();

                EightBitLoadCommands.Add(ch);
                SixteenBitLoadCommands.Add(ch);
                ExchangeBlockSearchCommands.Add(ch);
                EightBitArithmeticCommands.Add(ch);
                RotateAndShiftCommands.Add(ch);
                BitSetResetAndTestCommands.Add(ch);
                JumpCommands.Add(ch);
                CallAndReturnCommands.Add(ch);
                InputAndOutputGroup.Add(ch);
                GeneralArithmeticAndCPUControlCommands.Add(ch);
                SixteenBitArithmeticCommands.Add(ch);

                return ch;
            }
        }
    }




}



