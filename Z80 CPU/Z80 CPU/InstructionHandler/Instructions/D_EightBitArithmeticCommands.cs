﻿using Z80_CPU.Command.Arithmetic;
using Z80_CPU.Command.Addressing;
using BitClasses;
using Z80_CPU.Register.Utility;
using Z80_CPU.Instructions;

namespace Z80_CPU.Command.Instruction
{
    static class EightBitArithmeticCommands
    {
        public static void Add(InstructionHandler ch)
        {
            // ADD A, r
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("10000rrr"), new AddCommand8(new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 1));
            // ADD A, n
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11000110"), new AddCommand8(new S8_Constant(OperandNumber.Second)), 2));
            // ADD A, (HL)
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("10000110"), new AddCommand8(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 1));
            // ADD A, (IX + d)
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("10000110"), new AddCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third)), 3));
            // ADD A, (IY + d)
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("10000110"), new AddCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third)), 3));
            // ADC A, s
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("10001rrr"), new ADCCommand(new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 1));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001110"), new ADCCommand(new S8_Constant(OperandNumber.Second)), 2));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("10001110"), new ADCCommand(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 1));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011110"), BitConversion.StringOfBitsIntoZByte("10001110"), new ADCCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third)), 3));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("10001110"), new ADCCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third)), 3));
            // SUB s
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("10010rrr"), new SubCommand8(new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 1));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11010110"), new SubCommand8(new S8_Constant(OperandNumber.Second)), 2));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("10010110"), new SubCommand8(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 1));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("10010110"), new SubCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third)), 3));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("10010110"), new SubCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third)), 3));
            // SBC A, s
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("10011rrr"), new SBCCommand(new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 1));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011110"), new SBCCommand(new S8_Constant(OperandNumber.Second)), 2));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("10011110"), new SBCCommand(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 1));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("10011110"), new SBCCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third)), 3));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("10011110"), new SBCCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third)), 3));
            // AND s
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("10100rrr"), new AndCommand(new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 1));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11100110"), new AndCommand(new S8_Constant(OperandNumber.Second)), 2));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("10100110"), new AndCommand(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 1));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("10100110"), new AndCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third)), 3));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("10100110"), new AndCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third)), 3));
            // OR s
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("10110rrr"), new OrCommand(new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 1));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11110110"), new OrCommand(new S8_Constant(OperandNumber.Second)), 2));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("10110110"), new OrCommand(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 1));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("10110110"), new OrCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third)), 3));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("10110110"), new OrCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third)), 3));
            // XOR r
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("10101rrr"), new XorCommand8(new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 1));
            // XOR n
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101110"), new XorCommand8(new S8_Constant(OperandNumber.Second)), 2));
            // XOR (HL)
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("10101110"), new XorCommand8(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 1));
            // XOR (IX + d)
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("10101110"), new XorCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third)), 3));
            // XOR (IY + d)
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("10101110"), new XorCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third)), 3));
            // CP s
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("10111rrr"), new CPCommand(new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 1));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111110"), new CPCommand(new S8_Constant(OperandNumber.Second)), 2));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("10111110"), new CPCommand(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 1));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("10111110"), new CPCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third)), 3));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("10111110"), new CPCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third)), 3));
            /* INC r */
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("00rrr100"), new IncCommand8(new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three)), 1));
            /* INC (HL) */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00110100"), new IncCommand8(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 1));
            /* INC (IX + d) */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("00110100"), new IncCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third)), 3));
            /* INC (IY + d) */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("00110100"), new IncCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third)), 3));
            /* DEC r */
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("00rrr101"), new DecCommand8(new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three)), 1));
            /* DEC (HL) */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00110101"), new DecCommand8(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 1));
            // DEC (IX + d)
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("00110101"), new DecCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third)), 3));
            // DEC (IY + d)
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("00110101"), new DecCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third)), 3));
        }
    }
}
