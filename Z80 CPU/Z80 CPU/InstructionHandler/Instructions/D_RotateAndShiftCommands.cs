﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using Z80_CPU.Instructions;
using Z80_CPU.Register.Utility;

namespace Z80_CPU.Command.Instruction
{
    static class RotateAndShiftCommands
    {
        public static void Add(InstructionHandler ch)
        {
            /* RLCA */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00000111"), new RLCACommand(), 1));
            /* RLA */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00010111"), new RLACommand(), 1));
            /* RRCA */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00001111"), new RRCACommand(), 1));
            /* RRA */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00011111"), new RRACommand(), 1));
            /* RLC r */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("00000rrr"), new RLCCommand(new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 2));
            /* RLC (HL) */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00000110"), new RLCCommand(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 2));
            /* RLC (IX + d) */
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00000110"), new RLCCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third))));
            /* RLC (IY + d) */
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00000110"), new RLCCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third))));
            /* RL m*/                        
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("00010rrr"), new RLCommand(new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 2));            
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00010110"), new RLCommand(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 2));            
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00010110"), new RLCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third))));            
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00010110"), new RLCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third))));
            /* RRC m*/
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("00001rrr"), new RRCCommand(new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 2));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00001110"), new RRCCommand(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 2));
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00001110"), new RRCCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third))));
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00001110"), new RRCCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third))));
            /* RR m*/
            //Mistake in documentation for RRCommand r
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("00011rrr"), new RRCommand(new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 2));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00011110"), new RRCommand(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 2));
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00011110"), new RRCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third))));
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00011110"), new RRCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third))));
            /* SLA m*/
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("00100rrr"), new SLACommand(new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 2));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00100110"), new SLACommand(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 2));
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00100110"), new SLACommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third))));
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00100110"), new SLACommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third))));
            /* SRA m*/
            //This one is interesting
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("00101rrr"), new SRACommand(new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 2));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00101110"), new SRACommand(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 2));
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00101110"), new SRACommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third))));
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00101110"), new SRACommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third))));
            /* SRL m*/
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("00111rrr"), new SRLCommand(new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 2));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00111110"), new SRLCommand(new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 2));
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00111110"), new SRLCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third))));
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11001011"), BitConversion.StringOfBitsIntoZByte("00111110"), new SRLCommand(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third))));
            /* RLD */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("01101111"), new RLDCommand(), 2));
            /* RRD */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("01100111"), new RLDCommand(), 2));
        }
    }
}
