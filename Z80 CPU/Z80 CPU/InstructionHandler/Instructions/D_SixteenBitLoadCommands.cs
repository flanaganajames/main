﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using Z80_CPU.Command.Load;

using BitClasses;
using Z80_CPU.Register.Utility;

namespace Z80_CPU.Command.Instruction
{
    static class SixteenBitLoadCommands
    {

        public static void Add(InstructionHandler ch)
        {
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_dd_Pattern("00dd0001"), new LoadCommand16(new SD16_DDRegisterPattern(OperandNumber.First, EightBitLocation.Five, EightBitLocation.Four), new S16_Constant(OperandNumber.Second, OperandNumber.Third)), 3));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("00100001"), new LoadCommand16(new SD16_Register(SixteenBitRegisterNames.IX), new S16_Constant(OperandNumber.Third, OperandNumber.Forth)), 4));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("00100001"), new LoadCommand16(new SD16_Register(SixteenBitRegisterNames.IY), new S16_Constant(OperandNumber.Third, OperandNumber.Forth)), 4));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00101010"), new LoadCommand16(new SD16_Register(SixteenBitRegisterNames.HL), new SD16_ConstantAddress(OperandNumber.Second, OperandNumber.Third)), 3));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), ByteCodeListFromBitPatterns.ByteCodeListFrom_dd_Pattern("01dd1011"), new LoadCommand16(new SD16_DDRegisterPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four), new SD16_ConstantAddress(OperandNumber.Third, OperandNumber.Forth)), 4));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("00101010"), new LoadCommand16(new SD16_Register(SixteenBitRegisterNames.IX), new SD16_ConstantAddress(OperandNumber.Third, OperandNumber.Forth)), 4));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("00101010"), new LoadCommand16(new SD16_Register(SixteenBitRegisterNames.IY), new SD16_ConstantAddress(OperandNumber.Third, OperandNumber.Forth)), 4));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00100010"), new LoadCommand16(new SD16_ConstantAddress(OperandNumber.Second, OperandNumber.Third), new SD16_Register(SixteenBitRegisterNames.HL)), 3));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), ByteCodeListFromBitPatterns.ByteCodeListFrom_dd_Pattern("01dd0011"), new LoadCommand16(new SD16_ConstantAddress(OperandNumber.Third, OperandNumber.Forth), new SD16_DDRegisterPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four)), 4));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("00100010"), new LoadCommand16(new SD16_ConstantAddress(OperandNumber.Third, OperandNumber.Forth), new SD16_Register(SixteenBitRegisterNames.IX)), 4));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("00100010"), new LoadCommand16(new SD16_ConstantAddress(OperandNumber.Third, OperandNumber.Forth), new SD16_Register(SixteenBitRegisterNames.IY)), 4));
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111001"), new LoadCommand16(new SD16_Register(SixteenBitRegisterNames.StackPointer), new SD16_Register(SixteenBitRegisterNames.HL)), 1));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11111001"), new LoadCommand16(new SD16_Register(SixteenBitRegisterNames.StackPointer), new SD16_Register(SixteenBitRegisterNames.IX)), 2));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11111001"), new LoadCommand16(new SD16_Register(SixteenBitRegisterNames.StackPointer), new SD16_Register(SixteenBitRegisterNames.IY)), 2));
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_qq_Pattern("11qq0101"), new PushCommand16(new SD16_StackRegisterPattern(OperandNumber.First, EightBitLocation.Five, EightBitLocation.Four)), 1));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11100101"), new PushCommand16(new SD16_Register(SixteenBitRegisterNames.IX)), 2));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11100101"), new PushCommand16(new SD16_Register(SixteenBitRegisterNames.IY)), 2));
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_qq_Pattern("11qq0001"), new PopCommand16(new SD16_StackRegisterPattern(OperandNumber.First, EightBitLocation.Five, EightBitLocation.Four)), 1));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11100001"), new PopCommand16(new SD16_Register(SixteenBitRegisterNames.IX)), 2));
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11100001"), new PopCommand16(new SD16_Register(SixteenBitRegisterNames.IY)), 2));
        }
    }
}
