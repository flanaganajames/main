﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using Z80_CPU.Command.GeneralArithmeticAndCPUControl;
using Z80_CPU.Command.Jump;
using Z80_CPU.Instructions;
using Z80_CPU.Register.Utility;

namespace Z80_CPU.Command.Instruction
{
    static class JumpCommands
    {
        public static void Add(InstructionHandler ch)
        {
            /* JP nn */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11000011"), new JumpCommand16(new S16_Constant(OperandNumber.Second, OperandNumber.Third)), 3));
            /* JP cc, nn */
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_ccc_Pattern("11ccc010"), new ConditionalJumpCommand16(new S16_Constant(OperandNumber.Second, OperandNumber.Third)), 3));
            /* JR e */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00011000"), new JumpRelativeCommand8(new S8_Constant(OperandNumber.Second)), 2));
            /* JR C, e */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00111000"), new JumpCarryRelativeCommand8(new S8_Constant(OperandNumber.Second)), 2));
            /* JR NC, e */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00110000"), new JumpNotCarryRelativeCommand8(new S8_Constant(OperandNumber.Second)), 2));
            /* JR Z, e */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00101000"), new JumpZeroRelativeCommand8(new S8_Constant(OperandNumber.Second)), 2));
            /* JR NZ, e */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00100000"), new JumpNotZeroRelativeCommand8(new S8_Constant(OperandNumber.Second)), 2));
            /* JP (HL) */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101001"), new JumpCommand16(new SD16_Register(SixteenBitRegisterNames.HL)), 1)); //This matches the example but seems wrong on the syntax
            /* JP (IX) */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11101001"), new JumpCommand16(new SD16_Register(SixteenBitRegisterNames.IX)), 2)); //This matches the example but seems wrong on the syntax
            /* JP (IY) */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11101001"), new JumpCommand16(new SD16_Register(SixteenBitRegisterNames.IY)), 2)); //This matches the example but seems wrong on the syntax
            /* DJNZ, e */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00010000"), new DJNZCommand(new S8_Constant(OperandNumber.Second)), 2));
        }
    }
}

