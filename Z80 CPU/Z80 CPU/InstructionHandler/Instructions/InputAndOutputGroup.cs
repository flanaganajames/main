﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using Z80_CPU.Instructions;
using Z80_CPU.Register.Utility;
using Z80_CPU.SourceDestinations;

namespace Z80_CPU.Command.Instruction
{
    static class InputAndOutputGroup
    {
        public static void Add(InstructionHandler ch)
        {
            /* IN A, (n) */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011011"), new In_A_Command(), 2));
            /* IN r (C) */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("01rrr000"), new IN_R_Command(new SD8_RegisterPattern(OperandNumber.Second, BitClasses.EightBitLocation.Five, BitClasses.EightBitLocation.Four, BitClasses.EightBitLocation.Three)), 2));
            /* INI */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10100010"), new INICommand(), 2));
            /* INIR */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10110010"), new INIRCommand(), 2));
            /* IND */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10101010"), new INDCommand(), 2));
            /* INDR */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10111010"), new INDRCommand(), 2));
            /* OUT (n), A */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11010011"), new Out_N_Command(), 2));
            /* OUT (C), r */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("01rrr001"), new OUT_R_Command(new SD8_RegisterPattern(OperandNumber.Second, BitClasses.EightBitLocation.Five, BitClasses.EightBitLocation.Four, BitClasses.EightBitLocation.Three)), 2));
            /* OUTI */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10100011"), new OUTICommand(), 2));
            /* OTIR */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10110011"), new OTIRCommand(), 2));
            /* OUTD */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10101011"), new OUTDCommand(), 2));
            /* OTDR */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10111011"), new OTDRCommand(), 2));
        }
    }
}

