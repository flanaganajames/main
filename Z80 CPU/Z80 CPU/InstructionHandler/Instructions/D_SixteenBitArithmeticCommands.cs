﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Arithmetic;
using Z80_CPU.Command.Addressing;
using BitClasses;
using Z80_CPU.Register.Utility;
using Z80_CPU.Instructions;

namespace Z80_CPU.Command.Instruction
{
    static class SixteenBitArithmeticCommands
    {
        public static void Add(InstructionHandler ch)
        {
            /* ADD HL, ss */
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_ss_Pattern("00ss1001"), new AddCommand16(new SD16_Register(SixteenBitRegisterNames.HL), new SD16_SSRegisterPattern(OperandNumber.First, EightBitLocation.Five, EightBitLocation.Four)), 1));
            /* ADC HL, ss */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), ByteCodeListFromBitPatterns.ByteCodeListFrom_ss_Pattern("01ss1010"), new ADCCommand16(new SD16_SSRegisterPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four)), 2));
            /* SBC HL, ss */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), ByteCodeListFromBitPatterns.ByteCodeListFrom_ss_Pattern("01ss0010"), new SBCCommand16(new SD16_SSRegisterPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four)), 2));
            /* ADD IX, pp */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), ByteCodeListFromBitPatterns.ByteCodeListFrom_pp_Pattern("00pp1001"), new AddCommand16(new SD16_Register(SixteenBitRegisterNames.IX), new SD16_SSRegisterPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four)), 2));
            /* ADD IY, rr */            
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rr_Pattern("00rr1001"), new AddCommand16(new SD16_Register(SixteenBitRegisterNames.IY), new SD16_SSRegisterPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four)), 2));
            /* INC ss */
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_ss_Pattern("00ss0011"), new IncCommand16(new SD16_SSRegisterPattern(OperandNumber.First, EightBitLocation.Five, EightBitLocation.Four)), 1));
            /* INC IX */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("00100011"), new IncCommand16(new SD16_Register(SixteenBitRegisterNames.IX)), 2));
            /* INC IY */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("00100011"), new IncCommand16(new SD16_Register(SixteenBitRegisterNames.IY)), 2));
            /* DEC ss */
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_ss_Pattern("00ss1011"), new DecCommand16(new SD16_SSRegisterPattern(OperandNumber.First, EightBitLocation.Five, EightBitLocation.Four)), 1));
            /* DEC IX */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("00101011"), new DecCommand16(new SD16_Register(SixteenBitRegisterNames.IX)), 2));
            /* DEC IY */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("00101011"), new DecCommand16(new SD16_Register(SixteenBitRegisterNames.IY)), 2));
        }
    }
}


