﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using Z80_CPU.Command.Load;

using BitClasses;
using Z80_CPU.Register.Utility;

namespace Z80_CPU.Command.Instruction
{
    static class EightBitLoadCommands
    {
        public static void Add(InstructionHandler ch)
        {
            /*LD r, r' */
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_sss_Pattern("01rrrsss"), new LoadCommand8(new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 1));
            /*LD r,n */
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("00rrr110"), new LoadCommand8(new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new S8_Constant(OperandNumber.Second)), 2));
            /*LD r, (HL)*/
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("01rrr110"), new LoadCommand8(new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 1));
            /*LD r, (IX + d)*/
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("01rrr110"), new LoadCommand8(new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third)), 3));
            /*LD r, (IY + d)*/
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("01rrr110"), new LoadCommand8(new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third)), 3));
            /*LD (HL), r*/
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("01110rrr"), new LoadCommand8(new SD8_RegisterAddress(SixteenBitRegisterNames.HL), new SD8_RegisterPattern(OperandNumber.First, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 1));
            /*LD (IX + d), r*/
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("01110rrr"), new LoadCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third), new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 3));
            /*LD (IY + d), r*/
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), ByteCodeListFromBitPatterns.ByteCodeListFrom_rrr_Pattern("01110rrr"), new LoadCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third), new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 3));
            /*LD (HL), n*/
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00110110"), new LoadCommand8(new SD8_RegisterAddress(SixteenBitRegisterNames.HL), new S8_Constant(OperandNumber.Second)), 2));
            /* LD (IX + d), n*/
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("00110110"), new LoadCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third), new S8_Constant(OperandNumber.Forth)), 4));
            /*LD (IY + d), n*/
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("00110110"), new LoadCommand8(new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third), new S8_Constant(OperandNumber.Forth)), 4));
            /*LD A, (BC)*/
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00001010"), new LoadCommand8(new SD8_Register(EightBitRegisterNames.Accumulator), new SD8_RegisterAddress(SixteenBitRegisterNames.BC)), 1));
            //LD A, (DE)
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00011010"), new LoadCommand8(new SD8_Register(EightBitRegisterNames.Accumulator), new SD8_RegisterAddress(SixteenBitRegisterNames.DE)), 1));
            /*LD A, (nn)*/
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00111010"), new LoadCommand8(new SD8_Register(EightBitRegisterNames.Accumulator), new SD8_ConstantAddress(OperandNumber.Second, OperandNumber.Third)), 3));
            /*LD (BC), A*/
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00000010"), new LoadCommand8(new SD8_RegisterAddress(SixteenBitRegisterNames.BC), new SD8_Register(EightBitRegisterNames.Accumulator)), 1));
            /*LD (DE), A*/
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00010010"), new LoadCommand8(new SD8_RegisterAddress(SixteenBitRegisterNames.DE), new SD8_Register(EightBitRegisterNames.Accumulator)), 1));
            /*LD (nn), A*/
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00110010"), new LoadCommand8(new SD8_ConstantAddress(OperandNumber.Second, OperandNumber.Third), new SD8_Register(EightBitRegisterNames.Accumulator)), 3));
            /* LD A, I */ //TODO If an Interrupt occurs during execution of this instruction, the Parity flag contains a 0 //TODO Flags
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("01010111"), new Load_A_IR_Command8(new SD8_Register(EightBitRegisterNames.Accumulator), new SD8_Register(EightBitRegisterNames.Interrupt)), 2));
            /* LD A, R */ //TODO If an Interrupt occurs during execution of this instruction, the Parity flag contains a 0 //TODO Flags
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("01011111"), new Load_A_IR_Command8(new SD8_Register(EightBitRegisterNames.Accumulator), new SD8_Register(EightBitRegisterNames.Refresh)), 2));
            /* LD I, A */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("01000111"), new LoadCommand8(new SD8_Register(EightBitRegisterNames.Interrupt), new SD8_Register(EightBitRegisterNames.Accumulator)), 2));
            /* LD R, A */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("01001111"), new LoadCommand8(new SD8_Register(EightBitRegisterNames.Refresh), new SD8_Register(EightBitRegisterNames.Accumulator)), 2));

        }
    }
}
