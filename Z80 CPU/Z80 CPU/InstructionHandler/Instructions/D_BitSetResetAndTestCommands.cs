﻿using BitClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using Z80_CPU.Instructions;
using Z80_CPU.Register.Utility;
using Z80_CPU.SourceDestinations;

namespace Z80_CPU.Command.Instruction
{
    static class BitSetResetAndTestCommands
    {
        public static void Add(InstructionHandler ch)
        {
            /* BIT b, r */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_bbb_rrr_Pattern("01bbbrrr"), new BitCommand(new BitPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero)), 2));
            /* BIT b, (HL) */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_bbb_Pattern("01bbb110"), new BitCommand(new BitPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_RegisterAddress(SixteenBitRegisterNames.HL)), 2));
            /* BIT b, (IX + d) */
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_bbb_Pattern("01bbb110"), new BitCommand(new BitPattern(OperandNumber.Forth, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third))));
            /* BIT b, (IY + d) */
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_bbb_Pattern("01bbb110"), new BitCommand(new BitPattern(OperandNumber.Forth, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third))));
            /* SET b, r */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_bbb_rrr_Pattern("11bbbrrr"), new SetResetCommand(new BitPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero), Bit.One), 2));
            /* SET b, (HL) */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_bbb_Pattern("11bbb110"), new SetResetCommand(new BitPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_RegisterAddress(SixteenBitRegisterNames.HL), Bit.One), 2));
            /* SET b, (IX + d) */
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_bbb_Pattern("11bbb110"), new SetResetCommand(new BitPattern(OperandNumber.Forth, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third), Bit.One)));
            /* SET b, (IY + d) */
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_bbb_Pattern("11bbb110"), new SetResetCommand(new BitPattern(OperandNumber.Forth, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third), Bit.One)));
            /* RES b, r */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_bbb_rrr_Pattern("10bbbrrr"), new SetResetCommand(new BitPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_RegisterPattern(OperandNumber.Second, EightBitLocation.Two, EightBitLocation.One, EightBitLocation.Zero), Bit.Zero), 2));
            /* RES b, (HL) */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_bbb_Pattern("10bbb110"), new SetResetCommand(new BitPattern(OperandNumber.Second, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_RegisterAddress(SixteenBitRegisterNames.HL), Bit.Zero), 2));
            /* RES b, (IX + d) */
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_bbb_Pattern("10bbb110"), new SetResetCommand(new BitPattern(OperandNumber.Forth, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IX, OperandNumber.Third), Bit.One)));
            /* RES b, (IY + d) */
            ch.AddCommand(new TripleByteOpcode_IgnoreThirdCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11001011"), ByteCodeListFromBitPatterns.ByteCodeListFrom_bbb_Pattern("10bbb110"), new SetResetCommand(new BitPattern(OperandNumber.Forth, EightBitLocation.Five, EightBitLocation.Four, EightBitLocation.Three), new SD8_DisplacedRegisterAddress(SixteenBitRegisterNames.IY, OperandNumber.Third), Bit.One)));
        }
    }
}
