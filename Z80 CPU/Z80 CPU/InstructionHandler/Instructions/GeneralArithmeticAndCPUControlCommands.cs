﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.GeneralArithmeticAndCPUControl;
using BitClasses;
using Z80_CPU.Command.Addressing;
using Z80_CPU.Instructions;

namespace Z80_CPU.Command.Instruction
{
    static class GeneralArithmeticAndCPUControlCommands
    {
        public static void Add(InstructionHandler ch)
        {
            /* DAA */
            /* CPL */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00101111"), new CPLCommand(), 1));
            /* NEG */
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("01000100"), new NEGCommand(), 2));
            /* CCF */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00111111"), new CCFCommand(), 1));
            /* SCF */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00110111"), new SCFCommand(), 1));
            /* NOP */
            ch.AddCommand(new SingleOpCodeCommand(new ZByte(), new NOPCommand(), 1));
            /* HALT */
            /* DI */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11110011"), new DICommand(), 1));
            /* EI */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111011"), new EICommand(), 1));
            /* IM 0 */
            /* IM 1 */
            /* IM 2 */
        }
    }
}
