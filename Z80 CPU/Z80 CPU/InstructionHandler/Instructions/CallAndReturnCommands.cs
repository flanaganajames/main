﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Instructions;
using Z80_CPU.Register.Utility;

namespace Z80_CPU.Command.Instruction
{
    static class CallAndReturnCommands
    {
        public static void Add(InstructionHandler ch)
        {
            /* CALL nn */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001101"), new CallCommand(), 3));
            /* CALL cc, nn */
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_ccc_Pattern("11ccc100"), new ConditionalCallCommand(), 3));
            /* RET */
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11001001"), new ReturnCommand(), 1));
            /* RET cc */
            ch.AddCommand(new SingleOpCodeCommand(ByteCodeListFromBitPatterns.ByteCodeListFrom_ccc_Pattern("11ccc000"), new ConditionalReturnCommand(), 1));
            /* RETI */
            /* RETN */
            /* RST p */
        }
    }
}

