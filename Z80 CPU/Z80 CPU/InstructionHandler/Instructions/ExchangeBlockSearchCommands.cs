﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z80_CPU.Command.Addressing;
using Z80_CPU.Instructions;
using Z80_CPU.SourceDestinations;

namespace Z80_CPU.Command.Instruction
{
    static class ExchangeBlockSearchCommands
    {
        public static void Add(InstructionHandler ch)
        {
            //EX DE, HL
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101011"), new EXCommand(new SD16_Register(SixteenBitRegisterNames.DE), new SD16_Register(SixteenBitRegisterNames.HL)), 1));
            //EX AF, AF'
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("00001000"), new EXCommand(new SD16_Register(SixteenBitRegisterNames.AF), new SD16_Register(SixteenBitRegisterNames.AF_Dash)), 1));
            //EXX
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011001"), new EXXCommand(), 1));
            //EX (SP), HL
            ch.AddCommand(new SingleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11100011"), new EXCommand(new SD16_RegisterAddress(SixteenBitRegisterNames.StackPointer), new SD16_Register(SixteenBitRegisterNames.HL)), 1));
            //EX (SP), IX
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11011101"), BitConversion.StringOfBitsIntoZByte("11100011"), new EXCommand(new SD16_RegisterAddress(SixteenBitRegisterNames.StackPointer), new SD16_Register(SixteenBitRegisterNames.IX)), 2));
            //EX (SP), IY
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11111101"), BitConversion.StringOfBitsIntoZByte("11100011"), new EXCommand(new SD16_RegisterAddress(SixteenBitRegisterNames.StackPointer), new SD16_Register(SixteenBitRegisterNames.IY)), 2));
            //LDI
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10100000"), new LDICommand(), 2));
            //LDIR
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10110000"), new LDIRCommand(), 2));
            //LDD
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10101000"), new LDDCommand(), 2));
            //LDDR  
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10111000"), new LDDRCommand(), 2));
            //CPI
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10100001"), new CPICommand(), 2));
            //CPIR
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10110001"), new CPICommand(), 2));
            //CPD
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10101001"), new CPDCommand(), 2));
            //CPDR
            ch.AddCommand(new DoubleOpCodeCommand(BitConversion.StringOfBitsIntoZByte("11101101"), BitConversion.StringOfBitsIntoZByte("10111001"), new CPDRCommand(), 2));
        }
    }
}
